<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Sketch::class, function (Faker $faker) {

	$sen = $faker->paragraph($nbSentences = 2, $variableNbSentences = true);

	return [
		'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
		'slug' => str_slug($sen),
		'description' => $faker->randomHtml(2,3),
		'co_jumbel' => [1,2,6],
		'question' => $sen,
		'ea_jumbel' => ["velit","consectetur","aut"],
		'path' => 'http://lorempixel.com/'.mt_rand(3, 6).'00/'.mt_rand(3, 6).'00/',
		'category_id' => mt_rand(1, 11),
		'is_active'=> '1',
		'image' => 'testname',
		'created_by' => '1',
		'urls' => ["https://timesofindia.indiatimes.com/india/trump-describes-pulwama-terrorist-attack-as-horrible-situation/articleshow/68071703.cms"],
	];
});
