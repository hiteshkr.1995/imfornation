<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSketchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sketches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image');
            $table->longText('path');
            $table->string('title')->unique();
            $table->string('slug')->unique();
            $table->text('question');
            $table->longText('description');
            $table->text('co_jumbel');
            $table->text('ea_jumbel');
            $table->bigInteger('category_id');
            $table->longText('urls');
            $table->enum('is_active', ['0', '1'])->default('0');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sketches');
    }
}
