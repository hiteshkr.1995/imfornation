<?php

use Illuminate\Database\Seeder;

class SketchsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(App\Models\Sketch::class, 5000)->create();
	}
}
