<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class FilterArrEmVal implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $request;

    public function __construct($request)
    {

        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value = is_num_array($value)) {

            return $this->request[$attribute] = $value;

        } else {

            return FALSE;

        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please enter one :attribute.';
    }
}
