<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ReqBase64Img implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    private $request;

    private $message;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // $this->request

        if ($this->request[$attribute] = base64_decode_image($this->request->sketch)) {

            return $this->request[$attribute];

        } else {

            $this->message = 'Please Select Valid Image.';
            return FALSE;

        }

        // $this->message
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
