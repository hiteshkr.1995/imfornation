<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckForJumbels implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $faker;

    private $request;

    public function __construct($faker, $request)
    {
        $this->faker = $faker;
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value = is_num_array($value)) {

            if (count($value) >= 3 && count($value) <= 8) {

                return $this->request[$attribute] = $value;

            } else {

                return FALSE;

            }

        } else {

            $value = array_unique(explode(' ', $this->faker->realText()));
            $value = array_rand(array_flip($value), 4);
            return $this->request[$attribute] = $value;

        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please Pass beetwen 3 to 8 words.';
    }
}
