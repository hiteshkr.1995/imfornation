<?php

namespace App\Mail;

use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUs extends Mailable implements ShouldQueue
{
	use Queueable, SerializesModels;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	protected $send;

	public function __construct($send)
	{
		$this->send = $send;
	}

	private function setMailConfig()
	{
		$existing = config('mail');

		$new =array_merge(
			$existing, [

			// 'driver' => ,

			'host' => 'mail.imfornation.com',

			'port' => 465,

			// 'from' => [
			// 	'address' => ,
			// 	'name' => ,
			// ],

			'encryption' => 'ssl',

			'username' => 'contact_us@imfornation.com',

			'password' => 'Imfornation@2019',

			// 'sendmail' => '/usr/sbin/sendmail -bs',

			// 'markdown' => [
			// 	'theme' => 'default',

			// 	'paths' => [
			// 		resource_path('views/vendor/mail'),
			// 	],
			// ],

			// 'log_channel' => env('MAIL_LOG_CHANNEL'),

		]);

		config([
			'mail' => $new
		]);

		// Config::set('mail', $new);

		// print_array(config('mail'));
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{

		// $this->setMailConfig();

		return $this->from('contact_us@imfornation.com', 'Contact Imfornation')
		->markdown('emails.contact_us')
		->with([
			'name' => $this->send['name'],
			'email' => $this->send['email'],
			'subject' => $this->send['subject'],
			'message' => $this->send['message'],
		]);
	}
}
