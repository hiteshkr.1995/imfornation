<?php

namespace App\Repositories;

use Route;
use App\Models\Sketch;
use App\Models\Website;
use App\Models\Category;

class SketchRepository
{

	public $model;

	public function __construct(Sketch $model)
	{
		$this->model = $model;
	}

	public function index($slug = NULL, $skeSlug = NULL)
	{
		$websites = Website::all();

		if ( Route::is('cate.page.share') || Route::is('cate.page') ) {

			$category = Category::where('slug', $slug)->firstOrFail();

		}

		if ( Route::is('cate.page.share') ) {

			$this->model = $this->model->where('category_id', $category->id)
			->where('slug', $skeSlug)
			->firstOrFail();

		} elseif ( Route::is('cate.page') ) {

			$this->model = $this->model::orderBy('id', 'desc')
			->where('category_id', $category->id)
			->simplePaginate(10);

		} else {

			$this->model = $this->model::orderBy('id', 'desc')
			->simplePaginate(10);

		}

		$this->setUrlsIcons($websites);

		return $this->model;
	}

	protected function setUrlsIcons($websites)
	{
		if (Route::is('cate.page.share')) {

			$custArray = array();

			foreach ($this->model->urls as $key => $value) {

				$parse_url = parse_url($value, PHP_URL_HOST);

				$ar_se_key = array_search($parse_url, array_column($websites->toArray(), 'url'));

				if (false !== $ar_se_key) {

					$custArray[] = [
						'url' => $value,
						'icon' => $websites[$ar_se_key]->icon->path,
					];

				} else {

					$custArray[] = [
						'url' => $value,
						'icon' => app_logo('tran_50', 'png'),
					];

				}

			}

			$this->model->post = route('posts.page.share', [
				"catSlug" => $this->model->category->slug,
				"skeSlug" => $this->model->slug,
			]);

			$defLink = [
				'url' => $this->model->post,
				'icon' => app_logo('main', 'png'),
			];

			array_unshift($custArray, $defLink);

			$this->model->urls = $custArray;
			$this->model->created = $this->model->created_at->diffForHumans();
			$this->model->urlShare = route('cate.page.share', [
				"slug" => $this->model->category->slug,
				"skeSlug" => $this->model->slug,
			]);

		} else {

			foreach ($this->model as $key => $sketch) {

				$custArray = array();

				foreach ($sketch->urls as $uKey => $uValue) {

					$parse_url = parse_url($uValue, PHP_URL_HOST);

					$ar_se_key = array_search($parse_url, array_column($websites->toArray(), 'url'));

					if (false !== $ar_se_key) {

						$custArray[] = [
							'url' => $uValue,
							'icon' => $websites[$ar_se_key]->icon->path,
						];

					} else {

						$custArray[] = [
							'url' => $uValue,
							'icon' => app_logo('tran_50', 'png'),
						];

					}

				}

				$sketch->post = route('posts.page.share', [
					"catSlug" => $sketch->category->slug,
					"skeSlug" => $sketch->slug,
				]);

				$defLink = [
					'url' => $sketch->post,
					'icon' => app_logo('main', 'png'),
				];

				array_unshift($custArray, $defLink);

				$sketch->urls = $custArray;
				$sketch->created = $sketch->created_at->diffForHumans();
				$sketch->urlShare = route('cate.page.share', [
					"slug" => $sketch->category->slug,
					"skeSlug" => $sketch->slug,
				]);

			}

		}

	}


}