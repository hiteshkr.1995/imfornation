<?php

namespace App\Http\Controllers\Oversight;

use App\Rules\CheckForJumbels;
use App\Rules\FilterArrEmVal;
use App\Rules\ReqBase64Img;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\Sketch;
use App\Models\Category;
use InterventionImage;
use Faker\Generator as Faker;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SketchController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('oversight.sketchs.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$categories = Category::all();
		return view('oversight.sketchs.create', compact('categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request, Faker $faker)
	{
		$request['slug'] = Str::slug($request->question);

		$request->validate(
			[
				'ea_jumbel' => [new CheckForJumbels($faker, $request)],
				'sketch' => ['required', new ReqBase64Img($request)],
				'category' => 'required|numeric|exists:categories,id',
				'title' => 'required|string|max:191',
				'slug' => 'required|string|max:191|unique:sketches,slug',
				'question' => 'required|string|unique:sketches,question',
				'description' => 'required|string',
				'co_jumbel' => ['required', 'array', 'min:1', new FilterArrEmVal($request), 'max:8'],
				'urls' => ['required', 'array', 'min:1', new FilterArrEmVal($request), 'max:8'],
			],
			[
				'slug.required' => 'The Question field is required.',
				'slug.string' => 'The Question must be string.',
				'slug.max' => 'The Question may not be greater than 191 characters.',
				'slug.unique' => 'The Question has already been taken.',
			]
		);

		$sketch = $request->sketch;
		$category = Category::findOrFail($request->category);
		$title = Str::title($request->title);
		$slug = $request->slug;
		$question = $request->question;
		$co_jumbel = $request->co_jumbel;
		$description = $request->description;
		$urls = $request->urls;
		$ea_jumbel = $request->ea_jumbel;

		$path = 'sketchs/'.date("Y").'/'.$category->slug.'/';
		$image = public_storage_path().$path.$sketch['name'];

		Storage::disk('public')->put($path.$sketch['name'], decrypt($sketch['decodedImage']));

		if ( ($sketch['width'] >= 640) && ($sketch['height'] >= 640) ) {

			$imgObj = InterventionImage::make($image);

			$resizeImg = $imgObj->resize(640, 640, function ($constraint) {
				$constraint->aspectRatio();
			});

			$resizeImg->save($image);

		}

		$data = [
			'title' => $title,
			'slug' => $slug,
			'description' => $description,
			'co_jumbel' => $co_jumbel,
			'question' => $question,
			'ea_jumbel' => $ea_jumbel,
			'path' => $path,
			'category_id' => $category->id,
			'image' => $sketch['name'],
			'created_by' => auth('oversight')->user()->id,
			'urls' => $urls,
		];

		$sketch = Sketch::create($data);

		$request->session()->flash('success', 'Add Sketch Successfully!');

		return route('oversight.sketchs.show', $sketch->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$sketch = Sketch::findOrFail($id);
		return view('oversight.sketchs.show', compact('sketch'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$sketch = Sketch::findOrFail($id);
		$categories = Category::all();

		return view('oversight.sketchs.edit', compact('sketch', 'categories'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Faker $faker, $id)
	{
		$request['slug'] = Str::slug($request->question);

		$request->validate(
			[
				'ea_jumbel' => [new CheckForJumbels($faker, $request)],
				// 'sketch' => ['required', new ReqBase64Img($request)],
				'category' => 'required|numeric|exists:categories,id',
				'title' => 'required|string|max:191',
				'slug' => 'required|string|max:191|unique:sketches,slug,'.$id,
				'question' => 'required|string|unique:sketches,title,'.$id,
				'description' => 'required|string',
				'co_jumbel' => ['required', 'array', 'min:1', new FilterArrEmVal($request), 'max:8'],
				'urls' => ['required', 'array', 'min:1', new FilterArrEmVal($request), 'max:8'],
			],
			[
				'slug.required' => 'The Question field is required.',
				'slug.string' => 'The Question must be string.',
				'slug.max' => 'The Question may not be greater than 191 characters.',
				'slug.unique' => 'The Question has already been taken.',
			]
		);

		// $sketch = $request->sketch;
		$category = Category::findOrFail($request->category);
		$title = Str::title($request->title);
		$slug = $request->slug;
		$question = $request->question;
		$co_jumbel = $request->co_jumbel;
		$description = $request->description;
		$urls = $request->urls;
		$ea_jumbel = $request->ea_jumbel;

		$data = [
			'title' => $title,
			'slug' => $slug,
			'description' => $description,
			'co_jumbel' => $co_jumbel,
			'question' => $question,
			'ea_jumbel' => $ea_jumbel,
			// 'path' => $path,
			'category_id' => $category->id,
			// 'image' => $sketch['name'],
			'created_by' => auth('oversight')->user()->id,
			'urls' => $urls,
		];

		$sketch = Sketch::findOrFail($id)->update($data);

		$request->session()->flash('success', 'Update Sketch Successfully!');

		return route('oversight.sketchs.show', $id);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		// Sketch Successfully Delete!
		return Sketch::destroy($id);
	}

	public function datatable()
	{
		$sketchs = Sketch::query();

		return Datatables::of($sketchs)
			->addColumn('action', function ($sketchs) {
				$button = '<div class="row text-center">
					<a href="'.route('oversight.sketchs.edit', $sketchs->id).'" class="col-sm-6">
						<i class="material-icons">border_color</i>
					</a>
					<span onclick="openDeleteModal('.$sketchs->id.')" class="col-sm-6">
						<img style="width: 30px;height: 30px;" src="'.asset("svg/delete.svg").'">
					</span>
				</div>';
				return $button;
			})
			->editColumn('image', function($sketchs) {
				return '<img src='.$sketchs->path.' class="img-fluid" alt="Responsive image">';
			})
			->editColumn('id', function($sketchs) {
				return '<a href="'.route('oversight.sketchs.show', $sketchs->id).'" target="_blank">'.$sketchs->id.'</a>';
			})
			->editColumn('title', function($sketchs) {
				return str_limit($sketchs->title, 50);
			})
			->editColumn('created_at', function($sketchs) {
				return $sketchs->created_at->format('M d, Y (h:i:s A)');
			})
			->editColumn('is_active', function($sketchs) {
				$button = '<label class="switch">
					<input type="checkbox" '. (($sketchs->is_active == '1') ? 'checked' : '') .' onchange="isActive('.$sketchs->id.', event)">
					<span class="slider"></span>
				</label>';
				return $button;
			})
			->setRowId(function ($sketchs) {
				return 'sketch_'.$sketchs->id;
			})
			->setRowClass(function ($sketchs){
				return $sketchs->is_active == '0' ? 'table-danger' : '' ;
			})
			->rawColumns(['id', 'image', 'action', 'is_active'])
			->make(true);
	}

	public function isActive(Request $request, $id)
	{
		$check = ($request->check === 'true') ? '1' : '0';

		$data = [
			'is_active' => $check,
		];

		Sketch::findOrFail($id)->update($data);
	}
}