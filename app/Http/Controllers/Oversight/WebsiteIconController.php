<?php

namespace App\Http\Controllers\Oversight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WebsiteIcon;
use App\Models\Website;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use InterventionImage;

class WebsiteIconController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('oversight.website_icon.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate(
			[
				'image' => 'required|image|max:2000',
				'title' => 'required|string|unique:website_icons,title',
			]
		);

		// $request->image->path();
		// $request->image->extension();
		// $request->image->clientExtension();
		// $request->image->hashName();

		$image = $request->image;
		$title = $request->title;

		$data = [
			'name' => $image->hashName(),
			'title' => $title,
		];

		$this->resizeIcon($image);

		Storage::disk('public')->put('website_icons', $image);

		return WebsiteIcon::create($data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate(
			[
				'image' => 'required|image|max:2000',
			]
		);

		$image = $request->image;

		$websiteIcon = WebsiteIcon::findOrFail($id);

		$this->resizeIcon($image);

		return Storage::disk('public')->putFileAs('website_icons', new File($image), $websiteIcon->name);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function websites()
	{
		return Website::orderBy('id', 'desc')->get();
	}

	public function store_website_url(Request $request)
	{
		$request->validate(
			[
				'url' => 'required|string|unique:websites,url',
				'icon' => 'required|numeric|exists:website_icons,id',
			]
		);

		$url = parse_url($request->url, PHP_URL_HOST);
		$icon_id = $request->icon;
		
		$data = [
			'url' => $url,
			'icon_id' => $icon_id,
		];

		return Website::create($data);
	}

	public function website_icons()
	{
		return WebsiteIcon::orderBy('id', 'desc')->get();
	}

	public function resizeIcon($icon)
	{
		$imageSize = getimagesize($icon);
		$height = $imageSize[1];

		$iconObj = InterventionImage::make($icon);

		if ($height >= 50) {

			// resize the icon to a height of 200 and constrain aspect ratio (auto width)
			$resizeIcon = $iconObj->resize(null, 50, function ($constraint) {
				$constraint->aspectRatio();
			});

			$resizeIcon->save($icon);

		}

		return true;

	}
}
