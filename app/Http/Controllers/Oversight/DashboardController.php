<?php

namespace App\Http\Controllers\Oversight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sketch;
use Illuminate\Support\Facades\Storage;
use InterventionImage;

class DashboardController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$sketch = new Sketch;

		$sketch->total = $sketch->count();
		$sketch->active = $sketch->where('is_active', '1')->count();
		$sketch->inActive = $sketch->total - $sketch->active;

		return view('oversight.dashboard', compact('sketch'));
	}

	public function upload(Request $request)
	{
		$request->validate(
			[
				'upload' => ['required', 'image', 'max:2000'],
			],
			[
				'upload.max' => 'Maximum Image size to upload is 2MB.',
			],
			[
				'upload' => 'Image',
			]

		);

		// Get Image
		$image = $request->upload;

		// Set Path
		$path = 'content/'.date('Y').'/';

		$fullPath = '/storage/'.$path.$image->hashName();

		$this->resizeImage($image);

		Storage::disk('public')->put($path, $image);

		$data = [
			'url' => $fullPath,
		];

		// Make response
		return response()->json($data);
	}

	public function storage(Request $request)
	{
		return view('oversight.storage');
	}

	public function resizeImage($image)
	{
		$imageSize = getimagesize($image);
		$height = $imageSize[1];

		$imageObj = InterventionImage::make($image);

		if ($height > 600) {

			// resize the image to a height of 200 and constrain aspect ratio (auto width)
			$resizeImage = $imageObj->resize(null, 600, function ($constraint) {
				$constraint->aspectRatio();
			});

			$resizeImage->save($image);

		}

		return true;

	}
}
