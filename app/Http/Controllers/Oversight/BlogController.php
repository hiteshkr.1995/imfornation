<?php

namespace App\Http\Controllers\Oversight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;

class BlogController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('oversight.blogs.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('oversight.blogs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate(
			[
				'title' => 'required|string|unique:blogs,title',
				'image' => 'required|image|max:2000',
				'content' => 'required|min:200',
			]
		);

		$blank = "<p><br></p>";
		
		$content = $request->content;
		$title = $request->title;
		$image = $request->image;
		$slug = str_slug($request->title);

		if ($content == $blank) {

			return redirect()->back();

		} else {

			$created_by = auth('oversight')->user()->id;
			$path = 'blogs/'.date("Y").'/';

			$data = [
				'title' => $title,
				'slug' => $slug,
				'path' => $path,
				'image' => $image->hashName(),
				'content' => $content,
				'created_by' => $created_by,
			];

			$blog = Blog::create($data);

			Storage::disk('public')->put($path, $image);

			$request->session()->flash('success', 'Submit Blog Successfully!');
			return route('oversight.blogs.show', $blog->id);

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$blog = Blog::findOrFail($id);

		return view('oversight.blogs.show', compact('blog'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$blog = Blog::findOrFail($id);

		return view('oversight.blogs.edit', compact('blog'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request->validate(
			[
				'title' => 'required|string|unique:blogs,title,'.$id,
				'image' => 'image|max:2000',
				'content' => 'required|min:200',
			]
		);

		$blank = "<p><br></p>";
		
		$content = $request->content;
		$title = $request->title;
		$image = $request->image;
		$slug = str_slug($request->title);


		if ($content == $blank) {

			return redirect()->back();

		} else {

			$blog = Blog::findOrFail($id);

			$data = [
				'title' => $title,
				'slug' => $slug,
				'content' => $content,
			];

			if ($image) {

				if(!$blog->image) {
					$path = 'blogs/'.date("Y").'/';
					$data = $data + [
						'path' => $path,
					];
				}

				$data = $data + [
					'image' => $image->hashName(),
				];

			}

			$blog->update($data);

			if ($image) {

				Storage::disk('public')->putFileAs($blog->path, new File($image), $blog->image);

			}

			$request->session()->flash('success', 'Update Blog Successfully!');
			return route('oversight.blogs.show', $id);

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

		Blog::destroy($id);

		if (request()->ajax()) {

			return 'TRUE';

		} else {

			return redirect()->route('oversight.blogs.index')->with('success', 'Blog Deleted!');

		}

	}

	public function datatable()
	{
		$blogs = Blog::query();

		return Datatables::of($blogs)
		->addColumn('action', function ($blogs) {
			$button = '<div class="row text-center">
				<a href="'.route('oversight.blogs.edit', $blogs->id).'" class="col-sm-6">
					<i class="material-icons">border_color</i>
				</a>
				<span onclick="openDeleteModal('.$blogs->id.')" class="col-sm-6">
					<img style="width: 30px;height: 30px;" src="'.asset("svg/delete.svg").'">
				</span>
			</div>';
			return $button;
		})
		->editColumn('id', function($blogs) {
			return '<a href="'.route('oversight.blogs.show', $blogs->id).'" target="_blank">'.$blogs->id.'</a>';
		})
		->editColumn('image', function($blogs) {
			return '<img src="'.$blogs->image_path.'" class="img-fluid">';
		})
		->editColumn('title', function($blogs) {
			return str_limit($blogs->title, 50);
		})
		->editColumn('is_active', function($blogs) {
			$button = '<label class="switch">
				<input type="checkbox" '. (($blogs->is_active == '1') ? 'checked' : '') .' onchange="isActive('.$blogs->id.', event)">
				<span class="slider"></span>
			</label>';
			return $button;
		})
		->editColumn('created_at', function($blogs) {
			return $blogs->created_at->format('M d, Y (h:i:s A)');
		})
		->setRowClass(function ($blogs){
			return $blogs->is_active == '0' ? 'table-danger' : '' ;
		})
		->setRowId(function ($blogs) {
			return 'sketch_'.$blogs->id;
		})
		->rawColumns(['id', 'image', 'action', 'is_active'])
		->make(true);
	}

	public function isActive(Request $request, $id)
	{
		$check = ($request->check === 'true') ? '1' : '0';

		$data = [
			'is_active' => $check,
		];

		Blog::findOrFail($id)->update($data);
	}

}
