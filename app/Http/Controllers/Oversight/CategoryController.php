<?php

namespace App\Http\Controllers\Oversight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$categories = Category::all();
		return view('oversight.categories.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate(
			[
				'name' => 'required|string|unique:categories,name',
			]
		);


		$name = ucwords($request->name);
		$slug = str_slug($name, '_');

		$data = [
			'name' => $name,
			'slug' => $slug,
		];

		return Category::firstOrCreate($data,['name' => $name]);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{

		// $request->merge([
		// 	'title' => "google",
		// ]);
		$request->validate(
			[
				'name' => [
					'required',
					'string',
					Rule::unique('categories', 'name')->ignore($id),
					function ($attribute, $value, $fail) use ($id) {
						if(!Category::find($id)) {
							$fail('No record find.');
						}
					},
				]
			]
		);

		$name = ucwords($request->name);
		$slug = str_slug($name, '_');

		$data = [
			'name' => $name,
			'slug' => $slug,
		];

		Category::find($id)->update($data);

		return Category::find($id);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
