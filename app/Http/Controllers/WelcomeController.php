<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sketch;
use App\Models\Category;
use App\Models\Website;
use App\Models\WebsiteIcon;
use Carbon\Carbon;
use App\Repositories\SketchRepository;
use File;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUs;
use App\Http\Resources\SketchCollection;


class WelcomeController extends Controller
{
	/**
	* The user repository instance.
	*/
	protected $sketchs;

	/**
	* Create a new controller instance.
	*
	* @param  UserRepository  $users
	* @return void
	*/
	public function __construct(SketchRepository $sketchs)
	{
		$this->sketchs = $sketchs;
	}

	/**
	 * Show the content.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index(Request $request)
	{

		$response = $this->sketchs->index()->toArray();

		$result = $response['data'];

		$pagination = array_diff_key( $response, array_flip(["data"]) );

		if ($request->ajax()) {

			return response()->json([
				'result' => $result,
				'pagination' => $pagination,
			]);

		} else {

			return view('index', compact( 'result', 'pagination' ));

		}

	}

}

// diffForHumans();

// foreach ($data['links'] as $linkKey => $link) {
// 	$parse_domain = parse_url($link, PHP_URL_HOST);

// 	$web_ic_k = array_search($parse_domain, array_column($website_icons, 'name'));

// 	if ($web_ic_k != false) {
// 		if (array_key_exists($web_ic_k, $website_icons)) {
// 			$linkArray[] = [
// 				'link' => $link,
// 				'icon' => $website_icons[$web_ic_k]['icon'],
// 			];
// 		} else {
// 			$linkArray[] = [
// 				'link' => $link,
// 				'icon' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHkAAAB5CAMAAAAqJH57AAAAS1BMVEU9s57///8wsJoorpg4spxbvKrn9PH5/PwhrZbh8e30+vmV0cXt9/W439eq2c/W7OhtwrF6x7hQuKXM6OLA49ui1syLzcAAp46DybxdwGB9AAAFaUlEQVRogc2bC5OjIAyAFRDwUWvFuv3/v/R4qQhI3aM2m5mbubXoBwGSEKAoM6TNebnIAfMcdAZZ4AILCHJTKGkAyAOSYDR8n3zjus389m0yNWCJpl8mj8Ui4zXk1xR/PuOVjOd4EfHKIhd4vEWG77SBJTpSu+Y2YpJDZgWR7GfnPabFXvyu7p4jlm+yDHJnGlXM9e6xnlCbeFOrngujEr/CvyHXBkEQfty3p0+8b3KBn9uP9wdGxFSoDj94mizWxhFeLd1Z88IXvkCmipNVFWnTmiZPrlox6XXPoQAsKeoH1hNXG+hgXpwi916HopmWjyj5UdIZeaX7DHLrUxAZYmA1ykhQNu1D0+RI+0jw5Oi51MP/k6sjzhkhVQY5rtmzgjLI4fz5jfD/Jze+yfid4GTAkiTXmeSkEUuSRSY5acSS5FsmORkpJcl95thOGrEkOTBhvyQnjViS/MgxJNKUJOOhOLmpxa1vH1lcJY+2v4k6PrlccleLSfIqxLEUhPJarFuNkPoUR5WswyTqLkZ+mYLoE8BoFeyXXwFZ4CuIQQ22Ob5pe3z/3gdkWxds5EyzcU4c47KRm280emwi5DCY/by44bFD7g4jnU8JcYN/dz7PecbyvSB38eeS6dXqxvSAnGun3wnZBaM78j0v7non/H5ILqtLyfsoeE+eruxpb4XveckrO9rLIXjkzPgnJX5s5JHZhWSWJOeGXgmwH5T5ZD/58jnx0zhBHDZcM8hIkCD1ydGMwCckWE3vyTW6cIR5uaIdOeKhSfwvYiRWJv6mEtc778h0CMGoGJ0MZ1UgW/3lCdFlKluGoGqJXNFSdoceaIw8RYqSoWvWnBifWPPS6fSZdkZqOR7JQJktM9asNjEVEoxGxioqppA8x0y29i42h64NvnLhxNlCkN6NqzhWBxXaMOuVL5nLA8+3ZYYtmY5RX2FyeyZW0fOiU/+tNmskuMnq3/AS6+hUqQ4xp6jPxSN1ybcDx2yziiNZyHQlCyU3idMhjkrx2ymrlKzfOzKHfFrJzXAUEVhyEyFzzOXyi6x/rl5QeVqVm2GHKS0+MEMWkaG1J5d3HJC3d1BnW2rUSN2OiQsqhCK3iWhgzeH2OGwzNk02Q6xFq30ciPYOIhFayRKsaFOxlybrBckLe+ReycNMMqVjhJX/1f+w1nva6/H2RJtfT91raE82oqNVM910DuqHqdGmd5KSrge3Z/q5/bnrT8fIT6RmkwIh9VT8CFVJrrv7mGz7+e3YbhFWY0hUIdmsAnVHD6pvZ6wW5m11ZEfsV4fm3HyWg0eveqcduRilmNf0gHoqxcg/5G91m+xmu6F4wobJb6zO1ZlVjjJ1GxtjS2UFmKrE0Wz2bFh5ZLcXW4T7gFw4XtIuEVUdrV3uDhoc2O0y7qs2K2g3Yhxta6b1muZHppYolXEnIqrsqK+K+mfX/tI9mSqpjTNBxuXr7SRrfKJpgAP/rIr7A80hm70B31eZ3LvdN3iirRaxbj6MScowDtM5JLu2RWrC6G1nspGZHd2qo5kJCka26MbTdCoOK4PYc7zTft3jnmmtm0KqSRiZ1sWn6ERlO70SVIQNfhN7loHRw44W1vygyejt8oUIrZk8gnEsrvJBf2eNAbaugltLwq2fwXIGcHkSsNwQXD4MLAcIl/cEy/XC5bfBcvpw+xhwezdg+1Vwe3TOvuQFVUjtS5Zge7E7+fb+cyC5e+7Jg0N/9JwB3NkKuPMkcGdo4M4NwZ2VgjsfBngmDu4cINzZR7jznnBnXOHO9cKdZYY7vw13Zp0RqHP6JYG6m1C+DkbJmfsYaUPyd++gHArcvRu4u0aA96sA75TB3aMDvDtYsqz7kv8AytJFQgiewfkAAAAASUVORK5CYII=',
// 			];
// 		}
// 	} else {
// 		$linkArray[] = [
// 			'link' => $link,
// 			'icon' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHkAAAB5CAMAAAAqJH57AAAAS1BMVEU9s57///8wsJoorpg4spxbvKrn9PH5/PwhrZbh8e30+vmV0cXt9/W439eq2c/W7OhtwrF6x7hQuKXM6OLA49ui1syLzcAAp46DybxdwGB9AAAFaUlEQVRogc2bC5OjIAyAFRDwUWvFuv3/v/R4qQhI3aM2m5mbubXoBwGSEKAoM6TNebnIAfMcdAZZ4AILCHJTKGkAyAOSYDR8n3zjus389m0yNWCJpl8mj8Ui4zXk1xR/PuOVjOd4EfHKIhd4vEWG77SBJTpSu+Y2YpJDZgWR7GfnPabFXvyu7p4jlm+yDHJnGlXM9e6xnlCbeFOrngujEr/CvyHXBkEQfty3p0+8b3KBn9uP9wdGxFSoDj94mizWxhFeLd1Z88IXvkCmipNVFWnTmiZPrlox6XXPoQAsKeoH1hNXG+hgXpwi916HopmWjyj5UdIZeaX7DHLrUxAZYmA1ykhQNu1D0+RI+0jw5Oi51MP/k6sjzhkhVQY5rtmzgjLI4fz5jfD/Jze+yfid4GTAkiTXmeSkEUuSRSY5acSS5FsmORkpJcl95thOGrEkOTBhvyQnjViS/MgxJNKUJOOhOLmpxa1vH1lcJY+2v4k6PrlccleLSfIqxLEUhPJarFuNkPoUR5WswyTqLkZ+mYLoE8BoFeyXXwFZ4CuIQQ22Ob5pe3z/3gdkWxds5EyzcU4c47KRm280emwi5DCY/by44bFD7g4jnU8JcYN/dz7PecbyvSB38eeS6dXqxvSAnGun3wnZBaM78j0v7non/H5ILqtLyfsoeE+eruxpb4XveckrO9rLIXjkzPgnJX5s5JHZhWSWJOeGXgmwH5T5ZD/58jnx0zhBHDZcM8hIkCD1ydGMwCckWE3vyTW6cIR5uaIdOeKhSfwvYiRWJv6mEtc778h0CMGoGJ0MZ1UgW/3lCdFlKluGoGqJXNFSdoceaIw8RYqSoWvWnBifWPPS6fSZdkZqOR7JQJktM9asNjEVEoxGxioqppA8x0y29i42h64NvnLhxNlCkN6NqzhWBxXaMOuVL5nLA8+3ZYYtmY5RX2FyeyZW0fOiU/+tNmskuMnq3/AS6+hUqQ4xp6jPxSN1ybcDx2yziiNZyHQlCyU3idMhjkrx2ymrlKzfOzKHfFrJzXAUEVhyEyFzzOXyi6x/rl5QeVqVm2GHKS0+MEMWkaG1J5d3HJC3d1BnW2rUSN2OiQsqhCK3iWhgzeH2OGwzNk02Q6xFq30ciPYOIhFayRKsaFOxlybrBckLe+ReycNMMqVjhJX/1f+w1nva6/H2RJtfT91raE82oqNVM910DuqHqdGmd5KSrge3Z/q5/bnrT8fIT6RmkwIh9VT8CFVJrrv7mGz7+e3YbhFWY0hUIdmsAnVHD6pvZ6wW5m11ZEfsV4fm3HyWg0eveqcduRilmNf0gHoqxcg/5G91m+xmu6F4wobJb6zO1ZlVjjJ1GxtjS2UFmKrE0Wz2bFh5ZLcXW4T7gFw4XtIuEVUdrV3uDhoc2O0y7qs2K2g3Yhxta6b1muZHppYolXEnIqrsqK+K+mfX/tI9mSqpjTNBxuXr7SRrfKJpgAP/rIr7A80hm70B31eZ3LvdN3iirRaxbj6MScowDtM5JLu2RWrC6G1nspGZHd2qo5kJCka26MbTdCoOK4PYc7zTft3jnmmtm0KqSRiZ1sWn6ERlO70SVIQNfhN7loHRw44W1vygyejt8oUIrZk8gnEsrvJBf2eNAbaugltLwq2fwXIGcHkSsNwQXD4MLAcIl/cEy/XC5bfBcvpw+xhwezdg+1Vwe3TOvuQFVUjtS5Zge7E7+fb+cyC5e+7Jg0N/9JwB3NkKuPMkcGdo4M4NwZ2VgjsfBngmDu4cINzZR7jznnBnXOHO9cKdZYY7vw13Zp0RqHP6JYG6m1C+DkbJmfsYaUPyd++gHArcvRu4u0aA96sA75TB3aMDvDtYsqz7kv8AytJFQgiewfkAAAAASUVORK5CYII=',
// 		];
// 	}

// }