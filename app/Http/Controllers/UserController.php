<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Sketch;
use App\Models\Category;
use ImageOptimizer;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	    // $this->middleware('auth');
	}

	/**
	 * Show the user profile.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
	    return view('user.index');
	}

	/**
	 * Create the user sketch.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function sketchsCreate()
	{
		$categories = Category::all();
	    return view('user.sketchs.create', compact('categories'));
	}

	/**
	 * Store the user sketch.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function sketchsStore(Request $request)
	{
		$request->validate(
			[
				'sketch' => 'required',
				'category' => 'required|string',
				'title' => 'required|string',
				'question' => 'required|string',
				'description' => 'required|string',
				'words' => [
					'required',
					'array',
					'min:1',
					function ($attribute, $value, $fail) use ($request) {
						if (is_array($value)) {
							$array = array();

							foreach ($value as $key => $val) {
								if (!empty($val)) {
									$array[] = $val;
								}
							}

							if (empty($array)) {
								$fail('Please select one word.');
							} else {
								$request->words = $array;
							}
						}
					}
				],
				'links' => [
					'required',
					'array',
					'min:1',
					function ($attribute, $value, $fail) use ($request) {
						if (is_array($value)) {
							$array = array();

							foreach ($value as $key => $val) {
								if (!empty($val)) {
									$array[] = $val;
								}
							}

							if (empty($array)) {
								$fail('Please enter one link.');
							} else {
								$request->links = $array;
							}
						}
					}
				],
			]
		);

		$sketch = base64_decode_image($request->sketch);
		$category = $request->category;
		$title = $request->title;
		$question = $request->question;
		$words = $request->words;
		$description = $request->description;
		$image = sketch_path().$sketch['name'];
		$links = $request->links;

		Storage::disk('public')->put($image, decrypt($sketch['decodedImage']));

		ImageOptimizer::optimize($image);

		$imgObj = Image::make($image);

		if ( ($sketch['width'] <= 640) && ($sketch['height'] <= 640) ) {

		} else {


			// $resizeImg = $imgObj->resize(640, 640, function ($constraint) {
			// 	$constraint->aspectRatio();
			// });

			// $resizeImg->save($img);

			if ($sketch['height'] > $sketch['width']) {

				$resizeImg = $imgObj->resize(640, 640, function ($constraint) {
					$constraint->aspectRatio();
				});

				$resizeImg->save($image);

			} else {
				// $resizeImg = $imgObj->resize(640, null, function ($constraint) {
				// 	$constraint->aspectRatio();
				// });

				// $resizeImg->save($img);

			}

		}

		$randomData = [
			str_random(mt_rand(10,20)),
			str_random(mt_rand(10,20)),
			str_random(mt_rand(10,20)),
		];

		$jumbel = array_merge($randomData, $words);
		shuffle($jumbel);

		$data = [
			'title' => $title,
			'description' => $description,
			'correct_jumbel' => json_encode($words),
			'question' => $question,
			'jumbel' => json_encode($jumbel),
			'category' => $category,
			'sketch' => $sketch['name'],
			'created_by' => auth('web')->user()->id,
			'links' => json_encode($links),
		];


		$sketch = Sketch::create($data);

		$request->session()->flash('success', 'Add Sketch Successfully!');

		return route('user.index');
	}

	public function sketchsView(Request $request)
	{
		$created_by = auth('web')->user()->id;

		$sketch = Sketch::where('created_by', $created_by)->orderBy('id', 'desc')->get();
		foreach ($sketch as $key => $value) {
			$value->description = str_limit($value->description, 80);
		}
		return view('user.sketchs.view', compact('sketch'));
	}

	public function sketchsById($id)
	{
		$sketch = Sketch::find($id);
		return view('user.sketchs.byId', compact('sketch'));
	}
}
