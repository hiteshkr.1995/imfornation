<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sketch;
use App\Models\Category;

class PostsController extends Controller
{
	public function index()
	{
		$sketchs = Sketch::orderBy('views', 'desc')
		->orderBy('id', 'desc')
		->take(13)
		->get();

		return view('posts.index', compact('sketchs'));
	}

	public function catePage()
	{
	}

	/**
	 * Show the application Post by slug.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function pageShare($catSlug, $skeSlug)
	{
		$category = Category::where('slug', $catSlug)->firstOrFail();

		$sketch = Sketch::where('category_id', $category->id)
		->where('slug', $skeSlug)
		->firstOrFail();

		// increment Page View
		if (!request()->cookie('imfo_views') == "SuperAdmin") {
			$sketch->increment('views');
		}

		$latest = Sketch::whereNotIn('id', [$sketch->id])
		->orderBy('id','desc')
		->select('id', 'question', 'slug', 'category_id','created_at')
		->take(5)
		->get();

		foreach ($latest as $key => $value) {

			$value->blogsPageShare = route('posts.page.share', [
				'catSlug' => $value->category->slug,
				'skeSlug' => $value->slug,
			]);

		}

		$sketch = $sketch->toArray();
		$latest = $latest->toArray();

		$urlShare = request()->fullUrl();

		return view('posts.show', compact('sketch', 'latest', 'urlShare'));
	}
}
