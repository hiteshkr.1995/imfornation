<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use InterventionImage;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUs;
use Carbon\Carbon;
use Exception;

class BasicController extends Controller
{

	public function about()
	{
		$aboutPath = storage_path('app/public/json/about.json');

		$content = json_decode(File::get($aboutPath));

		return view('about', compact('content'));
	}

	public function aboutEdit()
	{
		$aboutPath = storage_path('app/public/json/about.json');

		$content = json_decode(File::get($aboutPath));

		return view('about_edit', compact('content'));
	}

	public function aboutUpdate(Request $request)
	{
		$request->validate(
			[
				'content' => 'required|string|min:100',
			]
		);

		$aboutPath = storage_path('app/public/json/about.json');
		$content = json_encode($request->content);

		File::put($aboutPath, $content);

		return redirect()->route('about')->with('success', 'Update Content Successfully!');
	}


	public function disclaimer()
	{
		return view('disclaimer');
	}

	public function disclaimerEdit()
	{
		$disclaimerPath = storage_path('app/public/json/disclaimer.json');

		// $content = json_decode(File::get($aboutPath));

		// return view('about_edit', compact('content'));
	}

	public function disclaimerUpdate(Request $request)
	{
		// $request->validate(
		// 	[
		// 		'content' => 'required|string|min:100',
		// 	]
		// );

		// $aboutPath = storage_path('app/public/json/about.json');
		// $content = json_encode($request->content);

		// File::put($aboutPath, $content);

		// return redirect()->route('about')->with('success', 'Update Content Successfully!');
	}

	public function contact()
	{
		return view('contact');
	}

	public function contactSend(Request $request)
	{
		$request->validate(
			[
				'name' => 'required|string',
				'email' => 'required|email',
				'subject' => 'required|string|min:5',
				'message' => 'required|string|min:25',
			]
		);

		try {

			$name = $request->name;
			$email = $request->email;
			$subject = $request->subject;
			$message = $request->message;

			$mailData = [
				'name' => $name,
				'email' => $email,
				'subject' => $subject,
				'message' => $message,
			];

			Mail::to('imfornation15@gmail.com')->send(new ContactUs($mailData));

			return redirect()->back()->with('success', 'Successfully Send Email!');

		} catch (Exception $event) {

			return redirect()->back()->with('error', 'Something Went Wrong (Try Again)!')->withInput();

		}
	}

	public function country()
	{
		$var = '{"data":[["af","Afrikaans",false],["az","Azərbaycan",false],["id","Bahasa Indonesia",false],["ms","Bahasa Malaysia",false],["bs","Bosanski",false],["ca","Català",false],["cs","Čeština",false],["da","Dansk",false],["de","Deutsch",false],["et","Eesti",false],["en-GB","English (UK)",false],["en","English (US)",true],["es","Español (España)",false],["es-419","Español (Latinoamérica)",false],["es-US","Español (US)",false],["eu","Euskara",false],["fil","Filipino",false],["fr","Français",false],["fr-CA","Français (Canada)",false],["gl","Galego",false],["hr","Hrvatski",false],["zu","IsiZulu",false],["is","Íslenska",false],["it","Italiano",false],["sw","Kiswahili",false],["lv","Latviešu valoda",false],["lt","Lietuvių",false],["hu","Magyar",false],["nl","Nederlands",false],["no","Norsk",false],["uz","O‘zbek",false],["pl","Polski",false],["pt-PT","Português",false],["pt","Português (Brasil)",false],["ro","Română",false],["sq","Shqip",false],["sk","Slovenčina",false],["sl","Slovenščina",false],["sr-Latn","Srpski",false],["fi","Suomi",false],["sv","Svenska",false],["vi","Tiếng Việt",false],["tr","Türkçe",false],["be","Беларуская",false],["bg","Български",false],["ky","Кыргызча",false],["kk","Қазақ Тілі",false],["mk","Македонски",false],["mn","Монгол",false],["ru","Русский",false],["sr","Српски",false],["uk","Українська",false],["el","Ελληνικά",false],["hy","Հայերեն",false],["iw","עברית",false],["ur","اردو",false],["ar","العربية",false],["fa","فارسی",false],["ne","नेपाली",false],["mr","मराठी",false],["hi","हिन्दी",false],["bn","বাংলা",false],["pa","ਪੰਜਾਬੀ",false],["gu","ગુજરાતી",false],["ta","தமிழ்",false],["te","తెలుగు",false],["kn","ಕನ್ನಡ",false],["ml","മലയാളം",false],["si","සිංහල",false],["th","ภาษาไทย",false],["lo","ລາວ",false],["my","ဗမာ",false],["ka","ქართული",false],["am","አማርኛ",false],["km","ខ្មែរ",false],["zh-CN","中文 (简体)",false],["zh-TW","中文 (繁體)",false],["zh-HK","中文 (香港)",false],["ja","日本語",false],["ko","한국어",false]]}';
		$var = json_decode($var);

		echo '<!DOCTYPE html>
			<html>
			<head>
			<style>
			table {
			  font-family: arial, sans-serif;
			  border-collapse: collapse;
			  width: 100%;
			}

			td, th {
			  border: 1px solid #dddddd;
			  text-align: left;
			  padding: 8px;
			}

			tr:nth-child(even) {
			  background-color: #dddddd;
			}
			</style>
			</head>
			<body>

			<h2>HTML Table Total data '.sizeof($var->data).'</h2>

			<table>
			  <tr>
			    <th>#</th>
			    <th>Sort</th>
			    <th>Country</th>
			  </tr>';

			  foreach ($var->data as $key => $value) {
			  	echo '
			  	<tr>
			  	  <td>'.++$key.'</td>
			  	  <td>'.$value[0].'</td>
			  	  <td>'.$value[1].'</td>
			  	</tr>
			  	';
			  }


			echo '</table>

			</body>
			</html>';
	}

	public function client()
	{
		try {
			$ipaddress = '';
			if (isset($_SERVER['HTTP_CLIENT_IP']))
				$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_X_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
			else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_FORWARDED'];
			else if(isset($_SERVER['REMOTE_ADDR']))
				$ipaddress = $_SERVER['REMOTE_ADDR'];
			else
				$ipaddress = 'UNKNOWN';

			throw_if(
			    $ipaddress == '127.0.0.1',
			    new Exception("Sorry not getting you ipaddress")
			);

			$api_url = "https://rdap.apnic.net/ip/$ipaddress";

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $api_url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

			$result = curl_exec($curl);
			$result = json_decode($result);

			curl_close($curl);

			// echo "<pre>";
			// print_r($ipaddress);
			// exit();

			return $template = "<!DOCTYPE html>
				<html lang='en'>
				<head>
					<meta charset='utf-8'>
					<meta name='viewport' content='width=device-width, initial-scale=1'>
					<style>
						table {
							font-family: arial, sans-serif;
							border-collapse: collapse;
							width: 100%;
						}

						td, th {
							border: 1px solid #c3a50f;
							text-align: left;
							padding: 8px;
						}

						tr:nth-child(even) {
							background-color: #dddddd;
						}

						th {
							font-size: 28px;
							padding: 20px 50px;
							text-align:center;
						}

					</style>
				</head>
					<table>
						<tr>
							<th colspan='2'>Client Ipaddress Details</th>
						</th>
						<tr>
							<td>My ipaddress</td>
							<td>".$ipaddress."</td>
						</tr>
						<tr>
							<td>Name</td>
							<td>".$result->name."</td>
						</tr>
						<tr>
							<td>IpVersion</td>
							<td>".$result->ipVersion."</td>
						</tr>
						<tr>
							<td>Address</td>
							<td>".$result->entities[0]->vcardArray[1][3][1]->label."</td>
						</tr>
						<tr>
							<td>Company Name</td>
							<td>".$result->remarks[0]->description[0]."</td>
						</tr>
						<tr>
							<td>Country Name</td>
							<td>".code_to_country($result->country)."</td>
						</tr>
					</table>
				</body>
			</html>";
		} catch (Exception $exception) {
			return $exception->getMessage();
		}
	}

	public function images(Request $request)
	{

		$width = $request->width;
		$height = $request->height;

		$path = public_path().'/storage/website_icons/livehindustan.png';

		// dd(public_storage_path().$check);

		if(!File::exists($path)) {
			return response()->json(['message' => 'Image not found.'], 404);
		}

		$type = File::mimeType($path);

		return InterventionImage::make($path)->resize($width, $height)->response($type);

		// $file = File::get($path);

		// $type = File::mimeType($path);

		// $response = response()->make($file, 200);
		// $response->header("Content-Type", $type);

		// return $response;
	}

}
