<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogsController extends Controller
{

	public function index()
	{

		$blogs = Blog::orderBy('id', 'desc')->take(10)->get();

		return view('blogs.index', compact('blogs'));

	}

	public function view($slug)
	{

		$blog = Blog::where('slug', $slug)->firstOrFail();

		// increment Page View
		if (!request()->cookie('imfo_views') == "SuperAdmin") {
			$blog->increment('views');
		}

		return view('blogs.show', compact('blog'));

	}

}