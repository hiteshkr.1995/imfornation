<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SketchRepository;

class CateController extends Controller
{
	/**
	* The user repository instance.
	*/
	protected $sketchs;

	/**
	* Create a new controller instance.
	*
	* @param  UserRepository  $users
	* @return void
	*/
	public function __construct(SketchRepository $sketchs)
	{
		$this->sketchs = $sketchs;
	}
	/**
	 * Show the application news by slug.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		// return view('cate.index', compact(''));
	}

	public function page(Request $request, $slug)
	{
		$response = $this->sketchs->index($slug)->toArray();

		$result = $response['data'];

		$pagination = array_diff_key( $response, array_flip(["data"]) );

		if ($request->ajax()) {

			return response()->json([
				'result' => $result,
				'pagination' => $pagination,
			]);

		} else {

			return view('index', compact( 'result', 'pagination' ));

		}
	}

	/**
	 * Show the application Blog by slug.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function pageShare(Request $request, $slug, $skeSlug)
	{
		$response[] = $this->sketchs->index($slug, $skeSlug)->toArray();

		$result = $response;
		$pagination = array();

		return view('index', compact( 'result', 'pagination' ));
	}
}
