<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'url', 'icon_id',
	];

	protected $with = ['icon'];

	public function icon()
	{
		return $this->belongsTo(WebsiteIcon::class);
	}
}
