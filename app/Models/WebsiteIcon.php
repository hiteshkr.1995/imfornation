<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebsiteIcon extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'title',
	];

	protected $appends = [
		'path'
	];

	public function getPathAttribute()
	{
		return asset_storage("website_icons/{$this->name}");
	}
}
