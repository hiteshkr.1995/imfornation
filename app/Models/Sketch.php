<?php

namespace App\Models;

use App\Scopes\IsActiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sketch extends Model
{
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'image', 'path', 'title', 'slug', 'question', 'description', 'co_jumbel', 'ea_jumbel', 'category_id', 'urls', 'views', 'is_active', 'created_by'
	];

	protected $attributes = [
		'views' => 0,
	];

	protected $casts = [
		'co_jumbel' => 'array',
		'ea_jumbel' => 'array',
		'urls' => 'array',
	];

	protected $appends = [
		'jumbels'
	];

	protected $with = [
		'category'
	];

	/**
	* The "booting" method of the model.
	*
	* @return void
	*/
	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope(new IsActiveScope);
	}

	public function getPathAttribute($value)
	{
		return asset_storage("{$value}{$this->image}");
	}

	public function getJumbelsAttribute()
	{
		if (!empty($this->question)) {

			if(is_array($this->co_jumbel) && is_array($this->ea_jumbel)) {

				$co_jumbel = [];
				$qa_jumbel = [];
				$question = explode(" ", $this->question);

				foreach ($this->co_jumbel as $key => $value) {
					if (@$question[$value]) {
						$co_jumbel['co'.$value] = $question[$value];
					}
				}

				foreach ($this->ea_jumbel as $key => $value) {
					$qa_jumbel['qaz'.$key] = $value;
				}

				$array = array_unique(array_replace($co_jumbel, $qa_jumbel));

				$shuffled_array = array();

				$keys = array_keys($array);
				shuffle($keys);

				foreach ($keys as $key)
				{
					$shuffled_array[$key] = $array[$key];
				}

				return $shuffled_array;

			}

		}
	}

	public function category()
	{
		return $this->belongsTo(Category::class)->withDefault([
			'name' => 'Error',
		]);
	}

}
