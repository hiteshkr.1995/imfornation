<?php

namespace App\Models;

use App\Scopes\IsActiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [

		'title', 'slug', 'path', 'image', 'content', 'views', 'is_active', 'created_by',

	];

	/**
	* The "booting" method of the model.
	*
	* @return void
	*/
	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope(new IsActiveScope);
	}

	protected $appends = [
		'image_path'
	];

	protected $attributes = [
		'views' => 0,
	];

	public function getImagePathAttribute()
	{
		if( $this->path && $this->image ) {

			return asset_storage("{$this->path}{$this->image}");

		}
	}

}
