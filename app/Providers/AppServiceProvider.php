<?php

namespace App\Providers;

use App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		if (App::environment('production')) {
			// URL::forceScheme('https');
			Schema::defaultStringLength(191);
		}
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		if (App::environment('production')) {

			$this->app->bind('path.public', function() {
				return realpath(base_path().'/../public_html/');
			});

		}
	}
}
