<script type="text/javascript">
	let app = new Vue({

		el: "#app",

		data: {
			id: "",
			result: @json($result),
			pagination: @json($pagination),
			ajax_result: [],
			ac_result: [],
			blog: "",
			views: [],
			addCard: [],
			urlShare: null,
		},

		mounted() {

			/*localStorage.clear();*/

			if (!localStorage.getItem('views')) {

				localStorage.setItem('views', JSON.stringify([]));

			} else {

				this.views = JSON.parse(localStorage.getItem('views'));

			}

			this.$nextTick(function () {
				this.result.forEach( (value, key) => {
					this.openModal(value.id);
				} );
			})

		},
		methods: {
			setId(id) {
				this.id = id;
			},
			openModal(id) {
				this.ac_result[id] = this.result.find( (element) => {
					if(element.id == id) {
						return element;
					}
				});

				if (this.views.includes(id)) {

					Vue.set(this.addCard, id, {raw_que: this.ac_result[id].question});

				} else {

					let que_array = this.ac_result[id].question.split(" ");
					let raw_que = [];
					let total_fields = 0;

					que_array.forEach( (value, key) => {

						let str = '';
						let fillWidth = 0;

						if(this.ac_result[id].co_jumbel.includes(key)) {

							fillWidth = this.ac_result[id].jumbels['co'+key].length * 18;

							++total_fields;

							str = "<input id='"+id+"word"+key+"' disabled class='text-center text-success fill-blank' type='text' style='width: "+fillWidth+"px!important'>";
							raw_que.push(str);

						} else {

							str = "<span class='mx-1 mt-3'>"+value+"</span>";
							raw_que.push(str);

						}

					});

					Vue.set(this.addCard, id, {
						raw_que: raw_que.join(" "),
						total_fields: total_fields,
						true_event: [],
					});

				}

			},
			getAjaxResult() {

				$("#ac_load").hide();
				$("#in_load").show();

				axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

				axios.get(this.pagination.next_page_url)
				.then((response) => {

					$("#ac_load").show();
					$("#in_load").hide();

					this.result = this.result.concat(response.data.result);
					this.ajax_result = this.ajax_result.concat(response.data.result);
					this.ajax_result.forEach( (value, key) => {
						this.openModal(value.id);
					} );
					this.pagination = response.data.pagination;

				}).catch(({ response }) => {

					$("#ac_load").show();
					$("#in_load").hide();

				});

			},
			userAnswer(value, key, event) {

				let sel_val = event.target.innerText || event.target.textContent || event.target.innerHTML;

				if(this.ac_result[this.id].co_jumbel.includes(parseInt(key.substr(2)))) {

					/*Set value to fill in the blanks*/
					$('#'+this.id+'word'+key.substr(2)).val( () => {
						return sel_val;
					});

					/*Count True Events*/
					if (!this.addCard[this.id].true_event.includes(value)) {
						this.addCard[this.id].true_event.push(value);
					}

					$("#op"+key+this.id).addClass("text-success");

					if (this.addCard[this.id].true_event.length == this.addCard[this.id].total_fields) {

						setTimeout( () => {

							this.addCard[this.id].raw_que = this.ac_result[this.id].question;

							this.views.push(this.ac_result[this.id].id);
							localStorage.setItem('views', JSON.stringify(this.views));

						}, 800);

					}

				} else {

					$("#op"+key+this.id).addClass("text-danger");

				}

			},
			openShareModal(value) {
				this.urlShare = value;
				$('#shareModal').modal('show');

				return true;
			},
		}
	})
</script>