<script type="text/javascript">
	let app = new Vue({

		el: "#app",

		data: {
			result: @json($result),
			pagination: @json($pagination),
			ajax_result: [],
			ac_result: [],
			blog: "",
			que_array: [],
			raw_que: [],
			total_fields: 0,
			true_event: [],
			views: [],
			urlShare: null,
		},

		mounted() {

			/*localStorage.clear();*/

			if (!localStorage.getItem('views')) {

				localStorage.setItem('views', JSON.stringify([]));

			} else {

				this.views = JSON.parse(localStorage.getItem('views'));

			}

			$('#modal').on('hidden.bs.modal', (event) => {

				this.clearModalValues();

			});

		},
		methods: {
			clearModalValues() {
				$(".col-6").removeClass('text-danger');
				$(".col-6").removeClass('text-success');
				$("input").val("");

				this.raw_que = [];
				this.total_fields = 0;
				this.true_event = [];
			},
			setSwiper() {
				new Swiper('.swiper-container', {
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					mousewheel: true,
					speed: 1000,
					spaceBetween: 100,
				});
			},
			openModal(id) {

				this.ac_result = this.result.find( (element) => {
					if(element.id == id) {
						return element;
					}
				});

				this.clearModalValues();

				if (this.views.includes(id)) {

					this.raw_que = this.ac_result.question

				} else {

					this.que_array = this.ac_result.question.split(" ");

					this.que_array.forEach( (value, key) => {

						let str = '';

						if(this.ac_result.co_jumbel.includes(key)) {

							++this.total_fields;

							str = "<input id='word"+key+"' disabled class='col-4 text-center text-success fill-blank' type='text'>";
							this.raw_que.push(str);

						} else {

							str = "<span class='mx-1 mt-3'>"+value+"</span>";
							this.raw_que.push(str);

						}

					});

					this.raw_que = this.raw_que.join(" ");

				}

				$('#modal').modal('show');

			},
			getAjaxResult() {

				$("#ac_load").hide();
				$("#in_load").show();

				axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

				axios.get(this.pagination.next_page_url)
				.then((response) => {

					$("#ac_load").show();
					$("#in_load").hide();

					this.result = this.result.concat(response.data.result);
					this.ajax_result = this.ajax_result.concat(response.data.result);
					this.pagination = response.data.pagination;

				}).catch(({ response }) => {

					$("#ac_load").show();
					$("#in_load").hide();

				});

			},
			userAnswer(value, key, event) {

				let sel_val = event.target.innerText || event.target.textContent || event.target.innerHTML;

				if(this.ac_result.co_jumbel.includes(parseInt(key.substr(2)))) {

					/*Set value to fill in the blanks*/
					$('#word'+key.substr(2)).val( () => {
						return sel_val;
					});

					/*Count True Events*/
					if (!this.true_event.includes(value)) {
						this.true_event.push(value);
					}

					$("#op"+key).addClass("text-success");

					if (this.true_event.length == this.total_fields) {

						setTimeout( () => {

							this.raw_que = this.ac_result.question;
							$(".col-6").removeClass('text-danger');
							$(".col-6").removeClass('text-success');

							this.views.push(this.ac_result.id);
							localStorage.setItem('views', JSON.stringify(this.views));

						}, 800);

					}

				} else {

					$("#op"+key).addClass("text-danger");

				}

			},
			openShareModal(value) {
				this.urlShare = value;
				$('#shareModal').modal('show');

				return true;
			},
		}
	})
</script>