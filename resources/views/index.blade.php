@extends('layouts.app')

@if(Route::is('cate.page.share'))

	@section('meta')

		<meta name="keywords" content="{{ $result[0]['title'] }}">
		<meta name="description" content="{{ str_limit( strip_tags($result[0]['description']), 150 ) }}">

		<meta property="og:url"           content="{{ $result[0]['urlShare'] }}" />
		<meta property="og:type"          content="{{ $result[0]['category']['name'] }}" />
		<meta property="og:title"         content="{{ $result[0]['title'] }}" />
		<meta property="og:description"   content="{{ str_limit( strip_tags($result[0]['description']), 150 ) }}" />
		<meta property="og:image"         content="{{ $result[0]['path'] }}" />

	@endsection

@endif

@if( Route::is('cate.page.share') )

	@section('title', $result[0]['title'])

@elseif( Route::is('cate.page') )

	@section('title', 'Category')

@else

	@section('title', 'Welcome')

@endif

@push('plugins')
<style type="text/css">
	.fill-blank {
		display: block;
		height: calc(2.25rem + 2px);
		padding: 0px;
		font-weight: bolder;
		margin: 5px;
		font-size: 1.5rem;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border-bottom: 2px solid #ced4da;
		border-top: unset; 
		border-right: unset;
		border-left: unset;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
	.fill-blank:focus {
		color: #495057;
		background-color: #fff;
		border-color: #80bdff;
		outline: 0;
		box-shadow: unset;
	}
	@media (max-width: 576px) {
		.card {
			border-right: 0px;
			border-left: 0px;
		}
		.container {
			padding-right: 0px;
			padding-left: 0px;
		}
	}
</style>

@endpush

@section('content')

	<!-- Generate card -->
	<div class="col-md-8 m-auto px-0">
		@if(!empty($result))
			@foreach($result as $key => $value)

				<div class="card shadow-lg rounded border-0 w-100 mb-5" @mouseover="setId({{ $value['id'] }})">
					<div class="card-header border-0 bg-transparent">
						<h5 class="card-title" style="margin-bottom: 0px;">{{ $value['title'] }}</h5>
					</div>

					<div class="card-body p-0 mx-auto w-100 text-center">
						<div style="cursor: pointer;">
							<img src="{{ $value['path'] }}" class="img-fluid rounded" style="min-height: 100%;max-height: 640px;">
						</div>
					</div>

					<hr class="mb-0">

					<div v-cloak class="card-body p-0 mx-auto w-100 text-center" v-if="addCard.length">

						<strong>
							<div class="row mx-2" v-html="addCard[<?php echo $value['id']; ?>].raw_que"></div>
						</strong>

						<hr>

						<div class="container-fluid">

							<div class="row" v-if="!views.includes(ac_result[{{ $value['id'] }}].id)">
								<div
									v-for="(value, key, index) in ac_result[{{ $value['id'] }}].jumbels"
									:key="key"
									@click="userAnswer(value, key, $event)"
									:style="{'font-weight': 'bold', 'cursor': 'pointer', 'font-size': '23px'}"
									class="col-6"
									:id="'op'+key+{{ $value['id'] }}"
								>
									<span>@{{value}}</span>
								</div>
							</div>

							<div v-if="views.includes(ac_result[{{ $value['id'] }}].id)">
								<span class="text-info">Read full article on:</span>
								<div class="row">
									<div
										v-for="(value, key, index) in ac_result[{{ $value['id'] }}].urls"
										:key="key"
										class="col-6 my-3"
									>
										<a :href="value.url" target="_blank">
											<img :src="value.icon" class="img-fluid">
										</a>
									</div>
								</div>
							</div>

						</div>

					</div>

					<hr>

					<div class="card-footer border-0 bg-transparent">
						<small>{{ $value['created'] }}</small>
						<span class="float-right" v-if="addCard.length">
							<a :href="ac_result[{{ $value['id'] }}].post" target="_blank" class="mr-2">Post info</a>
							<strong class="text-info mr-3 small">
								<i class="fas fa-tag"></i>
								{{ $value['category']['name'] }}
							</strong>
							<strong @click="openShareModal('{{ $value['urlShare'] }}')" style="cursor: pointer;">
								<i class="fas fa-share-alt text-info"></i>
							</strong>
						</span>
					</div>
				</div>
			@endforeach

			<div class="card w-100 shadow-lg rounded border-0 mb-5" v-for="(value, key, index) in ajax_result" @mouseover="setId(value.id)" v-cloak>
				<div class="card-header border-0 bg-transparent">
					<h5 class="card-title" style="margin-bottom: 0px;">@{{ value.title }}</h5>
				</div>

				<div class="card-body p-0 mx-auto w-100 text-center">
					<div style="cursor: pointer;">
						<img :src="value.path" class="img-fluid rounded" style="min-height: 100%;max-height: 640px;">
					</div>
				</div>

				<hr>

				<div v-cloak class="card-body p-0 mx-auto w-100 text-center" v-if="addCard.length">

					<strong>
						<div class="row mx-2" v-html="addCard[value.id].raw_que"></div>
					</strong>

					<hr>

						<div class="container-fluid">

							<div class="row" v-if="!views.includes(ac_result[value.id].id)">
								<div
									v-for="(valueO, keyO, index) in ac_result[value.id].jumbels"
									:key="keyO"
									@click="userAnswer(valueO, keyO, $event)"
									:style="{'font-weight': 'bold', 'cursor': 'pointer', 'font-size': '23px'}"
									class="col-6"
									:id="'op'+keyO+value.id"
								>
									<span>@{{valueO}}</span>
								</div>
							</div>

							<div v-if="views.includes(ac_result[value.id].id)">
								<span class="text-info">Read full article on:</span>
								<div class="row">
									<div
										v-for="(valueA, keyA, index) in ac_result[value.id].urls"
										:key="keyA"
										class="col-6 my-3"
									>
										<a :href="valueA.url" target="_blank">
											<img :src="valueA.icon" class="img-fluid">
										</a>
									</div>
								</div>
							</div>

						</div>

				</div>

				<hr>

				<div class="card-footer border-0 bg-transparent">
					<small>@{{ value.created }}</small>
					<span class="float-right">
						<a :href="ac_result[value.id].post" target="_blank" class="mr-2">Post info</a>
						<strong class="text-info mr-3 small">
							<i class="fas fa-tag"></i>
							@{{ value.category.name }}
						</strong>
						<strong @click="openShareModal(value.urlShare)" style="cursor: pointer;">
							<i class="fas fa-share-alt text-info"></i>
						</strong>
					</span>
				</div>
			</div>

		@else
			<div class="card shadow rounded">
				<div class="card-body text-center">
					<h1 class="m-5">
						NEWS available soon...
					</h1>
				</div>
			</div>
		@endif
	</div>

	<!-- Load More Button -->
	<div v-cloak>
		<div id="ac_load" class="my-4 text-center" v-if="pagination.next_page_url">
			<button class="btn btn-outline-primary w-50" @click="getAjaxResult">Load More</button>
		</div>
		<div id="in_load" class="my-4 text-center" style="display: none;">
			<button class="btn btn-outline-primary w-50" disabled>Loading...</button>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header border-0">
					<h5 class="modal-title" id="shareModal">Share with any social media platform</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body row m-auto">
					<span class="mx-3">
						<iframe class="fab" :src="'https://www.facebook.com/plugins/share_button.php?href='+urlShare+'&layout=button&size=large&mobile_iframe=true&width=73&height=28&appId'" width="73" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
					</span>
					<span class="mx-3 fab">
						<a class="twitter-share-button"
							href="https://twitter.com/intent/tweet"
							data-size="large"
							:data-url="urlShare"
						>Tweet</a>
					</span>
					<span class="mx-3">
						@if (detect_mobile())
							<a :href="'whatsapp://send?text='+urlShare">
								{!! get_svg('whatsapp') !!}
							</a>
						@else
							<a target="_blank" class="fab" :href="'https://web.whatsapp.com/send?text='+urlShare">
								{!! get_svg('whatsapp') !!}
							</a>
						@endif
					</span>
				</div>
				<div class="modal-footer border-0">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

@endsection

@push('scripts')
<script>window.twttr = (function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0],
		t = window.twttr || {};
	if (d.getElementById(id)) return t;
	js = d.createElement(s);
	js.id = id;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);

	t._e = [];
	t.ready = function(f) {
		t._e.push(f);
	};

	return t;
}(document, "script", "twitter-wjs"));</script>
	@include('vue.index')
@endpush