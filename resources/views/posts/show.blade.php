@extends('layouts.app')

@section('meta')

<meta name="keywords" content="{{ $sketch['question'] }}">
<meta name="description" content="{{ str_limit( strip_tags($sketch['description']), 150 ) }}">

<meta property="og:url"           content="{{ $urlShare }}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="{{ $sketch['question'] }}" />
<meta property="og:description"   content="{{ str_limit( strip_tags($sketch['description']), 150 ) }}" />
<meta property="og:image"         content="{{ $sketch['path'] }}" />

@endsection

@section('title', $sketch['question'])

@push('plugins')
<script async charset="utf-8" src="//cdn.embedly.com/widgets/platform.js"></script>

<link rel="stylesheet" type="text/css" href="{{ asset('css/ckeditor.css') }}">
@endpush

@section('content')
<div class="row">
	<div class="col-lg-8">

		<article class="post" itemscope="" itemtype="https://schema.org/BlogPosting">
			<header>
				<h3 itemprop="name headline">{{ $sketch['question']}}</h3>
				<div>
					<i class="fas fa-tag text-info"></i> {{ $sketch['category']['name'] }}
				</div>
				<hr class="custom">
				<div class="mb-3">
					Posted <time itemprop="datePublished">{{ date('M d, Y', strtotime($sketch['created_at'])) }}</time> by <span itemprop="author" itemscope="" itemtype="https://schema.org/Person"><span itemprop="name">Admin</span></span>
				</div>
			</header>
			<div itemprop="articleBody">
				<div class="text-center my-4">
					<img src="{{ $sketch['path'] }}" class="img-fluid">
				</div>

				<hr>

				<div class="ck-content">
					{!! $sketch['description'] !!}
				</div>

				<hr>

				<div class="card border-0 shadow">
					<div class="card-body">
						<strong class="text-info">
							<i class="fas fa-share-alt"></i> Share with
							@include('shared.socialmedia')
						</strong>
					</div>
				</div>
			</div>

			<hr>
		</article>

		<div class="card border-0 shadow mb-5">
			<div class="card-body d-flex align-items-center">
				{{--<div class="d-none d-md-block mr-4">
					<img class="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQF7h66NtAP3wUP_Pbqy83e5rE6yyjxkoRbp3iHenPVMhV4ZH8thg" alt="Credites">
				</div>--}}
				<div>

					<p>
						Read full article in depth in the links below.
					</p>

					<div class="container">

						<div class="row">
							@foreach ($sketch['urls'] as $url)
								<div class="mb-3 mr-3 small">
									<a href="{{ $url }}" target="_blank">{{ parse_url($url, PHP_URL_HOST) }}</a>
								</div>
							@endforeach
						</div>

					</div>

					<h4>Credits</h4>

				</div>
			</div>
		</div>

	</div>

	<div class="col-lg-4">

		<div class="card border-0 shadow mb-5 latest-posts">
			<div class="card-header border-0 bg-primary text-white py-3">
				<span class="font-weight-bold">Latest Posts</span>
			</div>
			<div class="list-group list-group-flush">
				@if(count($latest))
					@foreach ($latest as $key => $value)
						<a class="list-group-item list-group-item-action" href="{{ $value['blogsPageShare'] }}">
							<h6 class="latest-posts-title mb-1">{{ $value['question'] }}</h6>
							<div class="latest-posts-date">{{ date('M d, Y', strtotime($value['created_at'])) }}</div>
						</a>
					@endforeach
				{{--<div class="card-body">
					<a class="btn-all btn btn-block btn-primary" href="">Load More Post</a>
				</div>--}}
				@else
					<div class="card-body">
						<button class="btn-all btn btn-block btn-info"><strong>We don't have any records! Now</strong></button>
					</div>
				@endif
			</div>
		</div>

	</div>
</div>
@endsection

@push('scripts')
<script>
	document.querySelectorAll( 'oembed[url]' ).forEach( element => {
		const anchor = document.createElement( 'a' );

		anchor.setAttribute( 'href', element.getAttribute( 'url' ) );
		anchor.className = 'embedly-card';

		element.appendChild( anchor );
	} );
</script>
@endpush