@extends('layouts.app')

@section('title', 'Posts')

@push('plugins')

<link rel="stylesheet" type="text/css" href="{{ asset('css/blog.css') }}">
<style>
	.bd-placeholder-img {
		font-size: 1.125rem;
		text-anchor: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	.jumbotron {
		background-repeat: no-repeat;
		background-position: center;
	}

	.first-text {
		background-color: rgba(0, 0, 0, 0.4);
	}

	@media (min-width: 768px) {
		.bd-placeholder-img-lg {
			font-size: 3.5rem;
		}
	}
</style>
@endpush

@section('content')

<div class="jumbotron p-4 p-md-5 text-white rounded bg-dark" style="background-image: url({{ $sketchs[0]['path'] }});">
	<div class="col-md-6 px-0">
		<h1 class="display-4 font-italic first-text">{{ $sketchs[0]['title'] }}</h1>
		<p class="lead my-3 first-text">{{ str_limit( $sketchs[0]['question'], 150 ) }}</p>
		<p class="lead mb-0 first-text"><a href="{{ route('posts.page.share', [$sketchs[0]->category->slug, $sketchs[0]->slug]) }}" class="text-white font-weight-bold">Continue reading...</a></p>
	</div>
</div>

<div class="row mb-2">
	<div class="col-md-6">
		<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
			<div class="col p-4 d-flex flex-column position-static">
				<strong class="d-inline-block mb-2 text-primary">{{ $sketchs[1]['category']['name'] }}</strong>
				<h3 class="mb-0">{{ str_limit($sketchs[1]['title'], 18) }}</h3>
				<div class="mb-1 text-muted">{{ $sketchs[1]['created_at']->format('M d') }}</div>
				<p class="card-text mb-auto">{{ str_limit( $sketchs[1]['question'], 100 ) }}</p>
				<a href="{{ route('posts.page.share', [$sketchs[1]->category->slug, $sketchs[1]->slug]) }}" class="stretched-link">Continue reading</a>
			</div>
			<div class="col-auto d-none d-lg-block my-auto">
				<img class="bd-placeholder-img img-fluid" style="max-height: 249px;max-width: 200px;" src="{{ $sketchs[1]['path'] }}">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
			<div class="col p-4 d-flex flex-column position-static">
				<strong class="d-inline-block mb-2 text-success">{{ $sketchs[2]['category']['name'] }}</strong>
				<h3 class="mb-0">{{ str_limit($sketchs[2]['title'], 18) }}</h3>
				<div class="mb-1 text-muted">{{ $sketchs[2]['created_at']->format('M d') }}</div>
				<p class="mb-auto">{{ str_limit( $sketchs[2]['question'], 100 ) }}</p>
				<a href="{{ route('posts.page.share', [$sketchs[0]->category->slug, $sketchs[0]->slug]) }}" class="stretched-link">Continue reading</a>
			</div>
			<div class="col-auto d-none d-lg-block my-auto">
				<img class="bd-placeholder-img img-fluid" style="max-height: 249px;max-width: 200px;" src="{{ $sketchs[2]['path'] }}">
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-8 blog-main">
		<h3 class="pb-4 mb-4 font-italic border-bottom">
			From the Firehose
		</h3>

		@for ($key = 3; $key < count($sketchs); $key++)
			<div class="card shadow border-0 mb-5">
				<div class="card-body">
					<div class="blog-post">
						<h2 class="blog-post-title">{{ $sketchs[$key]['question'] }}</h2>
						<p class="blog-post-meta">{{ $sketchs[$key]['created_at']->format('M d, Y') }} <a href="">Admin</a></p>
						<div>
							{{ str_limit( strip_tags($sketchs[$key]['description']), 250 ) }}
						</div>
					</div>
					<a href="{{ route('posts.page.share', [$sketchs[$key]->category->slug, $sketchs[$key]->slug]) }}" class="stretched-link">Continue reading</a>
				</div>
			</div>
		@endfor

		{{--<nav class="blog-pagination">
			<a class="btn btn-outline-primary" href="">Older</a>
			<a class="btn btn-outline-secondary disabled" href="" tabindex="-1" aria-disabled="true">Newer</a>
		</nav>--}}

	</div>

	{{--<aside class="col-md-4 blog-sidebar">
		<div class="p-4 mb-3 bg-light rounded">
			<h4 class="font-italic">About</h4>
			<p class="mb-0">Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
		</div>

		<div class="p-4">
			<h4 class="font-italic">Archives</h4>
			<ol class="list-unstyled mb-0">
				<li><a href="">March 2014</a></li>
				<li><a href="">February 2014</a></li>
				<li><a href="">January 2014</a></li>
				<li><a href="">December 2013</a></li>
				<li><a href="">November 2013</a></li>
				<li><a href="">October 2013</a></li>
				<li><a href="">September 2013</a></li>
				<li><a href="">August 2013</a></li>
				<li><a href="">July 2013</a></li>
				<li><a href="">June 2013</a></li>
				<li><a href="">May 2013</a></li>
				<li><a href="">April 2013</a></li>
			</ol>
		</div>

		<div class="p-4">
			<h4 class="font-italic">Elsewhere</h4>
			<ol class="list-unstyled">
				<li><a href="">GitHub</a></li>
				<li><a href="">Twitter</a></li>
				<li><a href="">Facebook</a></li>
			</ol>
		</div>
	</aside>--}}

</div>
@endsection

@push('scripts')
<script type="text/javascript">
	
</script>
@endpush