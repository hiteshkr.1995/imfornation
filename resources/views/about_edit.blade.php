@extends('layouts.app')

@section('title', 'About Edit')

@push('plugins')
@endpush

@section('content')
<form action="{{ route('about.update') }}" method="POST">
	@csrf
	<div class="form-group">
		<label for="content">Content</label>
		<textarea id="content" name="content" class="form-control" rows="5">{{ $content }}</textarea>
		@if ($errors->has('content'))
		    <span class="invalid-feedback d-flex" role="alert">
		        <strong>{{ $errors->first('content') }}</strong>
		    </span>
		@endif
	</div>
	<button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection

@push('scripts')
@endpush