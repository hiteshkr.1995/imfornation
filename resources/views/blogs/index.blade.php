@extends('layouts.app')

@section('meta')

<meta name="keywords" content="Blogs | blogspot | blogs to read | blogs meaning | blogspot sign in | blogs on technology | blogs on life | blogspot search | blogspot blogs | blogs in hindi | blogs meaning in hindi | blogspot template | blogs on travel | blogs examples | blogs on digital marketing | blogs on love | blogs websites | blogs for students | blogs on marketing | blogs in india | blogs about life | blogs about love | blogs about travel | blogs about books | blogs and articles | blogs about technology | blogs app | blogs about digital marketing | blogs about lifestyle | blogs and forums | blogs about food | blogs and vlogs | blogs advantages and disadvantages | blogs about fashion | blogs about india | blogs and wikis | blogs and microblogs | blogs about friends | blogs about depression | blogs and online communities | blogs by writers | blogs best | blogs by students | blogs by ias officers | blogs by kids | blogs by sadhguru | blogs by ias toppers | blogs banner images | blogs business | blogs by amitabh bachchan | blogs by doctors | blogs by google | blogs by teachers | blogs based on technology | blogs by architects | blogs beauty | blogs bootstrap | blogs by bill gates | blogs by shashi tharoor | blogs by famous writers">
<meta name="description" content="Read your blogs and article over her and explore you knowledge, We are working on collection information with accuracy, Get latest and accurate info of your favorite Topic, Stay connected.">

<meta property="og:url"           content="{{ Request::fullUrl() }}" />
<meta property="og:type"          content="Blogs" />
<meta property="og:title"         content="Blogs | blogspot | blogs to read | blogs meaning | blogspot sign in | blogs on technology | blogs on life | blogspot search | blogspot blogs | blogs in hindi | blogs meaning in hindi | blogspot template | blogs on travel | blogs examples | blogs on digital marketing | blogs on love | blogs websites | blogs for students | blogs on marketing | blogs in india | blogs about life | blogs about love | blogs about travel | blogs about books | blogs and articles | blogs about technology | blogs app | blogs about digital marketing | blogs about lifestyle | blogs and forums | blogs about food | blogs and vlogs | blogs advantages and disadvantages | blogs about fashion | blogs about india | blogs and wikis | blogs and microblogs | blogs about friends | blogs about depression | blogs and online communities | blogs by writers | blogs best | blogs by students | blogs by ias officers | blogs by kids | blogs by sadhguru | blogs by ias toppers | blogs banner images | blogs business | blogs by amitabh bachchan | blogs by doctors | blogs by google | blogs by teachers | blogs based on technology | blogs by architects | blogs beauty | blogs bootstrap | blogs by bill gates | blogs by shashi tharoor | blogs by famous writers" />
<meta property="og:description"   content="Read your blogs and article over her and explore you knowledge, We are working on collection information with accuracy, Get latest and accurate info of your favorite Topic, Stay connected." />
<meta property="og:image"         content="{{ app_logo('50', 'png') }}" />

@endsection

@section('title', 'Blogs')


@push('plugins')
<style type="text/css">
	.image { 
	   position: relative;
	   width: 100%; /* for IE 6 */
	}

	h2 { 
	   position: absolute; 
	   top: 0px; 
	   left: 0; 
	   width: 100%; 
	}
	h2 span { 
	   color: white; 
	   font: bold 24px/45px Helvetica, Sans-Serif; 
	   letter-spacing: -1px;  
	   background: rgb(0, 0, 0); /* fallback color */
	   background: rgba(0, 0, 0, 0.3);
	   padding: 10px; 
	}
</style>
@endpush

@section('content')

	<div class="row">
		@foreach($blogs as $key => $value)

			<div class="col-md-4">
				<div class="card mb-4 shadow-lg">
					<img src="{{ $value->image_path }}" class="card-img-top img-fluid" alt="...">
					<div class="card-body">
						<p class="card-text">{{ $value->title }}</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<a href="{{ route('blogs.view', $value->slug) }}" class="btn btn-sm btn-outline-primary">View Blog</a>
							</div>
							<small class="text-muted">
								{{ date("M d Y", strtotime($value->created_at)) }}
							</small>
						</div>
					</div>
				</div>
			</div>

		@endforeach
	</div>

@endsection

@push('scripts')
@endpush