@extends('layouts.app')

@section('meta')

<meta name="keywords" content="{{ $blog['title'] }}">
<meta name="description" content="{{ str_limit( strip_tags($blog['content']), 150 ) }}">

<meta property="og:url"           content="{{ Request::fullUrl() }}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="{{ $blog['title'] }}" />
<meta property="og:description"   content="{{ str_limit( strip_tags($blog['content']), 150 ) }}" />
<meta property="og:image"         content="{{ $blog['image_path'] }}" />

@endsection

@section('title', $blog->title)

@push('plugins')
<script async charset="utf-8" src="//cdn.embedly.com/widgets/platform.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/ckeditor.css') }}">
@endpush

@section('content')

<div class="m-auto col-md-8">

	<header>
		<h3 itemprop="name headline">{{ $blog->title }}</h3>
		<!-- <div>
			<i class="fal fa-tag"></i> News
		</div> -->
		<hr>
		<div class="row">
			<div class="col-6">
				Posted <time datetime="2019-02-21T00:00:00+00:00" itemprop="datePublished">{{ $blog->created_at->format('M d, Y h:i A') }}</time>
				by <span itemprop="author" itemscope="" itemtype="https://schema.org/Person">
					<span itemprop="name">Admin</span>
				</span>
			</div>
			<div class="col-6">
				@include('shared.socialmedia', ['urlShare'=> Request::fullUrl()])
			</div>
		</div>
		<hr>
	</header>

	<div class="text-center">
		<img src="{{ $blog->image_path }}" class="img-fluid">
	</div>

	<div class="mt-5">

		<div class="ck-content">

			{!! $blog->content !!}

		</div>

	</div>

</div>

@endsection

@push('scripts')
<script>
	document.querySelectorAll( 'oembed[url]' ).forEach( element => {
		const anchor = document.createElement( 'a' );

		anchor.setAttribute( 'href', element.getAttribute( 'url' ) );
		anchor.className = 'embedly-card';

		element.appendChild( anchor );
	} );
</script>
@endpush