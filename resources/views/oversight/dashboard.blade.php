@extends('oversight.layouts.app')

@section('title', 'Dashboard')

@push('plugins')
@endpush

@section('content')
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Dashboard</h1>
			</div>
			<div class="col-sm-6">
			</div>
		</div>
	</div>
</section>
<!-- /.content-header -->
<!-- Main content -->
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6">

				<div class="card">
					<div class="card-header no-border">
						<div class="d-flex justify-content-between">
							<h3 class="card-title">Sketch Details</h3>
							<a href="javascript:void(0);">View Report</a>
						</div>
					</div>
					<div class="card-body">

						<div class="d-flex">
							<p class="d-flex flex-column">
								<span class="rounded-circle bg-success text-white" style="width: 20px; height: 20px;"></span>
							</p>
							<p class="ml-auto d-flex flex-column text-right">
								<span class="text-success">Active <span class="text-bold text-lg ml-5">{{ $sketch->active }}</span></span>
							</p>
						</div>

						<div class="d-flex">
							<p class="d-flex flex-column">
								<span class="rounded-circle bg-danger text-white" style="width: 20px; height: 20px;"></span>
							</p>
							<p class="ml-auto d-flex flex-column text-right">
								<span class="text-danger">Inactive <span class="text-bold text-lg ml-5">{{ $sketch->inActive }}</span></span>
							</p>
						</div>

						<div class="d-flex">
							<p class="d-flex flex-column">
								<span class="rounded-circle bg-primary text-white" style="width: 20px; height: 20px;"></span>
							</p>
							<p class="ml-auto d-flex flex-column text-right">
								<span class="text-primary">Total <span class="text-bold text-lg ml-5">{{ $sketch->total }}</span></span>
							</p>
						</div>

					</div>
				</div>
				<!-- /.card -->

				<!-- <div class="card">
					<div class="card-header no-border">
						<h3 class="card-title">Products</h3>
						<div class="card-tools">
							<a href="#" class="btn btn-tool btn-sm">
								<i class="fa fa-download"></i>
							</a>
							<a href="#" class="btn btn-tool btn-sm">
								<i class="fa fa-bars"></i>
							</a>
						</div>
					</div>
					<div class="card-body p-0">
						<table class="table table-striped table-valign-middle">
							<thead>
							<tr>
								<th>Product</th>
								<th>Price</th>
								<th>Sales</th>
								<th>More</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									<img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
									Some Product
								</td>
								<td>$13 USD</td>
								<td>
									<small class="text-success mr-1">
										<i class="fa fa-arrow-up"></i>
										12%
									</small>
									12,000 Sold
								</td>
								<td>
									<a href="#" class="text-muted">
										<i class="fa fa-search"></i>
									</a>
								</td>
							</tr>
							<tr>
								<td>
									<img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
									Another Product
								</td>
								<td>$29 USD</td>
								<td>
									<small class="text-warning mr-1">
										<i class="fa fa-arrow-down"></i>
										0.5%
									</small>
									123,234 Sold
								</td>
								<td>
									<a href="#" class="text-muted">
										<i class="fa fa-search"></i>
									</a>
								</td>
							</tr>
							<tr>
								<td>
									<img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
									Amazing Product
								</td>
								<td>$1,230 USD</td>
								<td>
									<small class="text-danger mr-1">
										<i class="fa fa-arrow-down"></i>
										3%
									</small>
									198 Sold
								</td>
								<td>
									<a href="#" class="text-muted">
										<i class="fa fa-search"></i>
									</a>
								</td>
							</tr>
							<tr>
								<td>
									<img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
									Perfect Item
									<span class="badge bg-danger">NEW</span>
								</td>
								<td>$199 USD</td>
								<td>
									<small class="text-success mr-1">
										<i class="fa fa-arrow-up"></i>
										63%
									</small>
									87 Sold
								</td>
								<td>
									<a href="#" class="text-muted">
										<i class="fa fa-search"></i>
									</a>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div> -->
				<!-- /.card -->
			</div>
			<!-- /.col-md-6 -->
			<!-- <div class="col-lg-6">
				<div class="card">
					<div class="card-header no-border">
						<div class="d-flex justify-content-between">
							<h3 class="card-title">Sales</h3>
							<a href="javascript:void(0);">View Report</a>
						</div>
					</div>
					<div class="card-body">
						<div class="d-flex">
							<p class="d-flex flex-column">
								<span class="text-bold text-lg">$18,230.00</span>
								<span>Sales Over Time</span>
							</p>
							<p class="ml-auto d-flex flex-column text-right">
								<span class="text-success">
									<i class="fa fa-arrow-up"></i> 33.1%
								</span>
								<span class="text-muted">Since last month</span>
							</p>
						</div>

						<div class="position-relative mb-4">
							<canvas id="sales-chart" height="200"></canvas>
						</div>

						<div class="d-flex flex-row justify-content-end">
							<span class="mr-2">
								<i class="fa fa-square text-primary"></i> This year
							</span>

							<span>
								<i class="fa fa-square text-gray"></i> Last year
							</span>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-header no-border">
						<h3 class="card-title">Online Store Overview</h3>
						<div class="card-tools">
							<a href="#" class="btn btn-sm btn-tool">
								<i class="fa fa-download"></i>
							</a>
							<a href="#" class="btn btn-sm btn-tool">
								<i class="fa fa-bars"></i>
							</a>
						</div>
					</div>
					<div class="card-body">
						<div class="d-flex justify-content-between align-items-center border-bottom mb-3">
							<p class="text-success text-xl">
								<i class="ion ion-ios-refresh-empty"></i>
							</p>
							<p class="d-flex flex-column text-right">
								<span class="font-weight-bold">
									<i class="ion ion-android-arrow-up text-success"></i> 12%
								</span>
								<span class="text-muted">CONVERSION RATE</span>
							</p>
						</div>
						<div class="d-flex justify-content-between align-items-center border-bottom mb-3">
							<p class="text-warning text-xl">
								<i class="ion ion-ios-cart-outline"></i>
							</p>
							<p class="d-flex flex-column text-right">
								<span class="font-weight-bold">
									<i class="ion ion-android-arrow-up text-warning"></i> 0.8%
								</span>
								<span class="text-muted">SALES RATE</span>
							</p>
						</div>
						<div class="d-flex justify-content-between align-items-center mb-0">
							<p class="text-danger text-xl">
								<i class="ion ion-ios-people-outline"></i>
							</p>
							<p class="d-flex flex-column text-right">
								<span class="font-weight-bold">
									<i class="ion ion-android-arrow-down text-danger"></i> 1%
								</span>
								<span class="text-muted">REGISTRATION RATE</span>
							</p>
						</div>

					</div>
				</div>
			</div>-->

		</div>

	</div>

</div>
@endsection

@push('scripts')
<!-- <script src="https://adminlte.io/themes/dev/AdminLTE/plugins/chart.js/Chart.min.js"></script> -->
<!-- <script src="https://adminlte.io/themes/dev/AdminLTE/dist/js/pages/dashboard3.js"></script> -->
@endpush