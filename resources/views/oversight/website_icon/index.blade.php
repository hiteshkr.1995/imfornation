@extends('oversight.layouts.app')

@section('title', 'Website Icons')

@push('plugins')
<link rel="stylesheet" type="text/css" href="https://adminlte.io/themes/dev/AdminLTE/plugins/select2/select2.min.css">
<style type="text/css">
	input[type=file] {
		cursor: pointer;
		border: 0.25em dashed silver;
		padding: 15px;
	}
</style>
@endpush

@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Website Icons</h1>
			</div>
			<div class="col-sm-6">
				<div class="float-right">
					<button @click="openAddUrl" class="btn btn-success">Add Url</button>
					<button @click="openAdd" class="btn btn-success">Add Website Icon</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content" v-cloak>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info card-outline">
					<div class="card-header">
						<div class="row">
							<div class="col-md-6">
								<h3 class="card-title">Handel<small> Website Icons as admin</small></h3>
							</div>
							<div class="col-md-6">
								<div class="card-tools float-right">
									<button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="card-body">

						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Websites</a>
								<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Website Icons</a>
							</div>
						</nav>
						<div class="tab-content" id="nav-tabContent">
							<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

								<div class="table-responsive-xl mt-3">
									<table class="table table-bordered text-center">
										<thead>
											<tr>
											<th scope="col">#</th>
											<th scope="col">Title</th>
											<th scope="col">Url</th>
											<th scope="col">Icon</th>
											<th scope="col">Select Icon</th>
											</tr>
										</thead>
										<tbody>
											@verbatim
												<tr v-for="(value, key, index) in websitesData" :key="key">
													<th scope="row">{{ value.id }}</th>
													<td>{{ value.icon.title }}</td>
													<td>{{ value.url }}</td>
													<td>
														<img :src="value.icon.path" class="img-fluid" alt="Web Icon">
													</td>
													<td>
														<div class="form-group">
															<select class="form-control select2" @change="onSelectIcon(value)" style="width: 100%;">
																<option value="">Select Icon to change</option>
																<option
																	v-for="(value1, key1, index1) in website_icons_data"
																	:key="key1"
																	:selected="value1.id == value.icon.id"
																>{{ value1.title }}</option>
															</select>
														</div>
													</td>
												</tr>
												<tr v-if="websitesData.length == 0">
													<td v-if="imgLoading" colspan="6" class="text-center">Loading...</td>
													<td v-if="!imgLoading" colspan="6" class="text-center">No record found</td>
												</tr>
											@endverbatim
										</tbody>
									</table>
								</div>

							</div>
							<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

								<div class="table-responsive-xl mt-3">
									<table class="table table-bordered text-center">
										<thead>
											<tr>
												<th scope="col">#</th>
												<th scope="col">Icon</th>
												<th scope="col">Name</th>
												<th scope="col">Update Icon</th>
											</tr>
										</thead>
										<tbody>
											@verbatim
												<tr v-for="(value, key, index) in website_icons_data" :key="key">
													<th scope="row">{{ value.id }}</th>
													<td>
														<img :src="value.path" class="img-fluid" alt="Web Icon">
													</td>
													<th scope="row">{{ value.title }}</th>
													<td>
														<button class="btn btn-primary" @click="openUpdateIcon(value.id)">Update Icon</button>
													</td>
												</tr>
												<tr v-if="website_icons_data.length == 0">
													<td v-if="imgLoading" colspan="6" class="text-center">Loading...</td>
													<td v-if="!imgLoading" colspan="6" class="text-center">No record found</td>
												</tr>
											@endverbatim
										</tbody>
									</table>
								</div>

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="websiteIcons" tabindex="-1" role="dialog" aria-labelledby="websiteIconsTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="websiteIconsTitle">Add Website Icon</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form @submit="websiteIconForm" method="POST">
				<div class="modal-body">
					<div class="form-group">
						<input type="file" accept="image/*" @change="onChangeIcon" name="image" class="form-control-file" required>
						<span v-if="errors.image" v-for="(value, key) in errors.image" :key="value.id" class="invalid-feedback d-flex" role="alert">
							<strong>@{{ value }}</strong>
						</span>
						<span v-if="message" class="invalid-feedback d-flex" role="alert">
							<strong>@{{ message }}</strong>
						</span>
					</div>
					<div class="form-group">
						<label for="title">Enter Title *</label>
						<input type="text" class="form-control" id="title" name="title" v-model="title" placeholder="Enter name *" required>
						<span v-if="errors.title" v-for="(value, key) in errors.title" :key="value.id" class="invalid-feedback d-flex" role="alert">
							<strong>@{{ value }}</strong>
						</span>
					</div>
					<div v-if="progress">
						<label>Progress Bar</label>
						<progress :value="progress" max="100" class="w-100"></progress>
					</div>
					<div v-if="base64Data" class="text-center">
						<img :src="base64Data" style="height: 50px;" class="img-fluid">
					</div>
					<div v-if="imgLoading">
						<span>Loading...</span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button v-if="!loading" type="submit" class="btn btn-primary">Add Icon</button>
					<button v-if="loading" type="button" class="btn btn-primary" disabled>Loading...</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="websiteUrls" tabindex="-1" role="dialog" aria-labelledby="websiteUrlsTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="websiteUrlsTitle">Add Website Url</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form @submit.prevent="websiteUrlForm" method="POST">
				<div class="modal-body">
					<div class="form-group">
						<label>Enter or Paste Url *</label>
						<input type="url" name="url" v-model="url" class="form-control" placeholder="Enter or Paste Url *" required>
						<span v-if="errors.url" v-for="(value, key) in errors.url" :key="key" class="invalid-feedback d-flex" role="alert">
							<strong>@{{ value }}</strong>
						</span>
						<span v-if="message" class="invalid-feedback d-flex" role="alert">
							<strong>@{{ message }}</strong>
						</span>
					</div>
					<div class="form-group" style="margin-bottom: unset;">
						<select id="icon" class="form-control select2" name="icon" style="width: 100%;" required>
							<option value="">Select Icon</option>
							<option
								v-for="(value, key, index) in website_icons_data"
								:key="key"
								:value="value.id"
							>@{{ value.title }}</option>
						</select>
						<span v-if="errors.icon" v-for="(value, key) in errors.icon" :key="key" class="invalid-feedback d-flex" role="alert">
							<strong>@{{ value }}</strong>
						</span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button v-if="!loading" type="submit" class="btn btn-primary">Add Url</button>
					<button v-if="loading" type="button" class="btn btn-primary" disabled>Loading...</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="updateIcon" tabindex="-1" role="dialog" aria-labelledby="updateIconTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="updateIconTitle">Update Icon</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form @submit.prevent="updateWebsiteIconForm" method="POST">
				<div class="modal-body">
					<div class="form-group">
						<input type="file" accept="image/*" @change="onChangeIcon" name="image" class="form-control-file" required>
						<span v-if="errors.image" v-for="(value, key) in errors.image" :key="value.id" class="invalid-feedback d-flex" role="alert">
							<strong>@{{ value }}</strong>
						</span>
						<span v-if="message" class="invalid-feedback d-flex" role="alert">
							<strong>@{{ message }}</strong>
						</span>
					</div>
					<div v-if="base64Data" class="text-center">
						<img :src="base64Data" style="height: 50px;" class="img-fluid">
					</div>
					<div v-if="imgLoading">
						<span>Loading...</span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button v-if="!loading" type="submit" class="btn btn-primary">Update Icon</button>
					<button v-if="loading" type="button" class="btn btn-primary" disabled>Loading...</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script src="https://adminlte.io/themes/dev/AdminLTE/plugins/select2/select2.full.min.js"></script>

<script type="text/javascript">

	new Vue({
		el: "#app",
		data: {
			base64Data: '',
			image: '',
			title: '',
			url: '',
			icon_id: '',
			loading: false,
			imgLoading: false,
			errors: [],
			message: '',
			websitesData: [],
			website_icons_data: [],
			progress: 0,
		},
		mounted() {
			this.imgLoading = true;

			this.websites().then( () => {
				this.website_icons().then( () => {
					this.imgLoading = false;
					$('.select2').select2();
				});
			});

			$('#websiteIcons').on('shown.bs.modal', () => {
				$("input[name='name']").trigger('focus');
			});
			$('#websiteIcons').on('hidden.bs.modal', (event) => {
				this.base64Data = '';
				this.image = '';
				this.title = '';
				this.loading = false;
				this.imgLoading = false;
				this.errors = [];
				this.message = '';
				this.progress = 0;
				$('input[type=file]').val(null);
			});
			$('#updateIcon').on('hidden.bs.modal', (event) => {
				this.base64Data = '';
				this.image = '';
				this.loading = false;
				this.imgLoading = false;
				this.errors = [];
				this.message = '';
				this.progress = 0;
				this.icon_id = '';
				$('input[type=file]').val(null);
			});
			$('#websiteUrls').on('hidden.bs.modal', (event) => {
				this.url = '';
				this.errors = [];
				this.message = '';
				this.loading = false;
				$("#icon").select2("val", "");
			});

			$.fn.modal.Constructor.prototype._enforceFocus = function() {};
		},
		methods: {
			openAdd: function() {
				$("#websiteIcons").modal('show');
			},
			openAddUrl: function() {
				$("#websiteUrls").modal('show');
			},
			openUpdateIcon(id) {
				this.icon_id = id;
				$("#updateIcon").modal('show');
			},
			async websites() {
				await axios.get("{{ route('oversight.websites') }}")
				.then((response) => {
					this.websitesData = response.data;
				}).catch(({ response }) => {
				});
			},
			async website_icons() {
				await axios.get("{{ route('oversight.website_icons') }}")
				.then((response) => {
					this.website_icons_data = response.data;
				}).catch(({ response }) => {
				});
			},
			websiteIconForm: function(event) {
				event.preventDefault();
				this.loading = true;
				this.progress = 0;

				let formData = new FormData();
				formData.append('image', this.image);
				formData.append('title', this.title);

				let config = {
					headers: {
						'Content-Type': 'multipart/form-data'
					},
					onUploadProgress: (progressEvent) => {
						this.progress = parseInt(Math.round( (progressEvent.loaded * 100) / progressEvent.total) );
					}
				};

				axios.post("{{ route('oversight.website_icon.store') }}",
					formData, config
				).then((response) => {
					/*this.loading = false;*/
					this.errors = [];

					let data = response.data;

					$("#websiteIcons").modal('hide');
					location.reload();

				}).catch(({ response }) => {
					this.loading = false;
					this.message = '';
					this.progress = 0;

					if (response.status == 422) {
						this.errors = response.data.errors;
					} else {
						this.message = response.statusText;
					}

				});
			},
			onSelectIcon(value) {
				console.log(value);
			},
			updateWebsiteIconForm() {
				this.loading = true;
				let formData = new FormData();
				formData.append('image', this.image);
				formData.append('_method', 'put');

				let config = {
					headers: {
						'Content-Type': 'multipart/form-data'
					},
				};

				axios.post("{{ route('oversight.website_icon.index') }}/"+this.icon_id,
					formData, config
				).then((response) => {
					/*$("#websiteIcons").modal('hide');*/
					this.errors = [];
					this.message = '';

					let data = response.data;

					location.reload();

				}).catch(({ response }) => {
					this.loading = false;
					this.message = '';

					if (response.status == 422) {
						this.errors = response.data.errors;
					} else {
						this.message = response.statusText;
					}

				})
			},
			websiteUrlForm: function(event) {
				this.loading = true;
				axios.post("{{ route('oversight.store_website_url') }}", {
					url: this.url,
					icon: $("#icon").select2("val"),
				}).then((response) => {
					this.errors = [];

					let data = response.data;

					/*$("#websiteUrls").modal('hide');*/
					location.reload();

				}).catch(({ response }) => {
					this.loading = false;
					this.message = '';

					if (response.status == 422) {
						this.errors = response.data.errors;
					} else {
						this.message = response.statusText;
					}

				})
			},
			onChangeIcon: function(event) {
				this.base64Data = '';
				if (event.target.files[0].size < 2000*1024) {
					this.image = event.target.files[0];
					var reader = new FileReader();

					reader.onloadstart = (event) => {
						this.imgLoading = true;
					};
					reader.onload = (readerEvent) => {
						this.base64Data = readerEvent.target.result;
					};
					reader.onloadend = (event) => {
						this.imgLoading = false;
					};
					reader.readAsDataURL(this.image);
					this.message = "";
				} else {
					event.target.value = '';
					this.message = "Maximum file size is 2MB.";
				}
			},
			handle_function_call(function_name) {
				this[function_name]();
			},
		},
	})

</script>
@endpush