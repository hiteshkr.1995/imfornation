@extends('oversight.layouts.app')

@section('title', 'Categories')

@push('plugins')
@endpush

@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Categories</h1>
			</div>
			<div class="col-sm-6">
				<div class="float-right">
					<button @click="openAdd" class="btn btn-success">Add Category</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content" v-cloak>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info card-outline">

					<div class="card-header">
						<div class="row">
							<div class="col-md-6">
								<h3 class="card-title">Handel<small> categories as admin</small></h3>
							</div>
							<div class="col-md-6">
								<div class="card-tools float-right">
									<button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
						</div>
					</div>

					<div class="card-body">

						<div class="table-responsive-xl">
							<table class="table">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Name</th>
										<th scope="col">Updated At</th>
										<th scope="col">Action</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(value, key, index) in categories" :key="key">
										<th scope="row">@{{ value.id }}</th>
										<td>@{{ value.name }}</td>
										<td>@{{ date_time(value.updated_at) }}</td>
										<td>
											<button class="btn btn-sm btn-primary" @click="openEdit(key)">Edit</button>
										</td>
									</tr>
									<tr v-if="categories.length == 0">
										<td colspan="4" class="text-center">No record found</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">@{{ modalTitle }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form @submit="categoryForm" :action="action" method="POST">
					<div class="modal-body">
						<div class="form-group my-2">
							<input type="text" class="form-control" name="name" v-model="name" placeholder="Enter category name" autofocus required>
							<span v-if="errors.name" v-for="(value, key) in errors.name" :key="value.id" class="invalid-feedback" role="alert" style="display: flex;">
								<strong>@{{ value }}</strong>
							</span>
							<span v-if="message" class="invalid-feedback d-flex" role="alert">
								<strong>@{{ message }}</strong>
							</span>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button v-if="!loading" type="submit" class="btn btn-primary">@{{ submitButtonName }}</button>
						<button v-if="loading" type="button" class="btn btn-primary" disabled>Loading...</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">

	new Vue({
		el: "#app",
		data: {
			categories: @json($categories),
			name: '',
			errors: [],
			loading: false,
			message: '',
			cat_elem: null,
			cat_key: null,
			action: '',
			modalTitle: '',
			submitButtonName: '',
		},
		mounted() {
			$('#category').on('shown.bs.modal', () => {
				$("input[name='name']").trigger('focus');
			});
			$('#category').on('hidden.bs.modal', (event) => {
				this.name = '';
				this.errors = [];
				this.loading = false;
				this.message = '';
				this.cat_elem = {};
				this.cat_key = null;
				this.action = '';
				this.modalTitle = '';
				this.submitButtonName = '';
			});
		},
		methods: {
			openAdd: function() {
				this.submitButtonName = 'Add';
				this.modalTitle = 'Add Category';
				this.action = "{{ route('oversight.categories.store') }}";

				$("#category").modal('show');
			},
			openEdit: function(key) {
				this.cat_key = key;
				this.cat_elem = this.categories[key];
				this.name = this.cat_elem.name;
				this.action = "{{ route('oversight.categories.index') }}/"+this.cat_elem.id;
				this.submitButtonName = 'Edit';
				this.modalTitle = 'Edit Category';

				$("#category").modal('show');
			},
			categoryForm: function (event) {
				this.loading = true;
				event.preventDefault();

				if (this.submitButtonName == 'Edit') {
					axios.put(this.action, {
						name: this.name,
					}).then((response) => {

						this.loading = false;
						this.errors = [];

						let data = response.data;

						this.categories[this.cat_key] = data;
						$("#category").modal('hide');


					}).catch(({ response }) => {
						this.message = '';

						this.loading = false;
						if (response.status == 422) {
							this.errors = response.data.errors;
						} else {
							console.log(response);
							this.message = response.statusText;
						}

					});
				} else {
					axios.post(this.action, {
						name: this.name,
					}).then((response) => {

						this.loading = false;
						this.errors = [];

						let data = response.data;

						this.categories.push(data);
						$("#category").modal('hide');

					}).catch(({ response }) => {
						this.message = '';

						this.loading = false;
						if (response.status == 422) {
							this.errors = response.data.errors;
						} else {
							this.message = response.statusText;
						};

					});
				};

			},
			date_time: function(value) {
				let update_at = new Date(value);
				let date = update_at.toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric' });
				let time = update_at.toTimeString().substr(0,8);

				return date + ' ' +time;
			},
			handle_function_call(function_name) {
				this[function_name]();
			},
		},
	})

</script>
@endpush