@extends('oversight.layouts.app')

@section('title', 'Edit Blog')

@push('plugins')
<style type="text/css">
	#editor p {
		margin-bottom: 1.5rem;
		font-size: 1.25rem;
		line-height: 2.25rem;
	}
</style>
@endpush

@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Edit Blog</h1>
			</div>
			<div class="col-sm-6">
				<span class="float-right">
					<a href="{{ route('oversight.blogs.show', $blog->id) }}" class="btn btn-primary">Show</a>
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete">
						Delete
					</button>
				</span>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content" v-cloak>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info card-outline">
					<div class="overlay align-items-center d-flex justify-content-center" style="display: none!important;">
						<div class="spinner-border text-primary" role="status">
							<span class="sr-only">Loading...</span>
						</div>
					</div>
					<div class="card-header">
						<h3 class="card-title">Please <small>write your content</small></h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body">
						<div class="mb-3">

							<form @submit.prevent="upload" action="{{ route('oversight.blogs.update', $blog->id) }}" method="POST" enctype="multipart/form-data">
								@csrf
								{{ method_field('PUT') }}

								<div class="row">
									<div class="col-md-6">
										
										<div class="form-group">
											<label for="title">Update Title</label>
											<input type="text" name="title" v-model="title" class="form-control form-control-lg" placeholder="Update Title" required>

											<span v-if="errors.title" v-for="(value, key) in errors.title" :key="key" class="invalid-feedback d-flex" role="alert">
												<strong>@{{ value }}</strong>
											</span>
										</div>

									</div>

									<div class="col-md-6">

										<div class="form-group">
											<label for="image">Update Image <small class="text-info">select image or drop image here to update</small></label>
											<div id="add-image" class="text-center" style="height: 200px;">
												<input type="file" accept="image/*" @change="getComImg" name="image" class="form-control-file d-none">
												<img src="{{ ($blog->image_path) ? $blog->image_path : '' }}" class="img-fluid" style="display: unset!important;height: 100%" />
												<div class="align-items-center d-flex justify-content-center" style="display: none!important; height: 100%">
													<h2 class="m-0">Select or Drop Image Here</h2>
												</div>
											</div>

											<span v-if="errors.image" v-for="(value, key) in errors.image" :key="key" class="invalid-feedback d-flex" role="alert">
												<strong>@{{ value }}</strong>
											</span>
										</div>

									</div>
								</div>

								<div class="form-group">

									<label for="content">
										Update Content
									</label>

									<span v-if="errors.content" v-for="(value, key) in errors.content" :key="key" class="invalid-feedback d-flex" role="alert">
										<strong>@{{ value }}</strong>
									</span>

									<!-- This container will become the editable. -->
									<textarea id="editor" name="content" v-model="content" class="border editor"></textarea>

								</div>

								<div class="float-right">
									<button type="submit" class="btn btn-success">
										Update Blog
									</button>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@include('oversight.blogs.delete')

@endsection

@push('scripts')
<script type="text/javascript">
	const UploadUrl = '{{ route("oversight.upload") }}';
</script>
<script src="{{ asset('js/ckeditor/classic.js') }}"></script>
<script src="{{ asset('js/ckeditor/image-upload.js') }}"></script>
<script src="{{ asset('js/imagecompressor/index.js') }}"></script>

<script type="text/javascript">
	new Vue({
		el: "#app",
		data: {
			response: @json($blog),
			title: "",
			image: [],
			content: "",
			errors: [],
		},
		components: {
		},
		mounted() {
			ClassicEditor.create( document.querySelector( '#editor' ), {

				extraPlugins: [ MyCustomUploadAdapterPlugin ],

			} ).then(editor => {
				this.content = editor;
			})
			.catch( error => {

				console.error( error );

			} );

			function_default();

			this.title = this.response.title;
			this.content = this.response.content;
		},
		methods: {
			upload(event) {

				$(".overlay").removeAttr("style");

				let formData = new FormData();
				formData.append('_method', 'put');
				formData.append('title', this.title);
				if (this.image.name) {
					formData.append('image', this.image);
				}
				formData.append('content', this.content.getData());

				let config = {
					headers: {
						'Content-Type': 'multipart/form-data'
					},
				};

				axios.post(event.target.action, formData, config
				).then((response) => {

					window.location.href = response.data;

				}).catch(({ response }) => {

					if (response.status == 422) {
						this.errors = response.data.errors;
					} else {
						alert(response.data.message);
					}

					$(".overlay").attr("style", "display: none!important");

				});

			},
			getComImg: function(event) {
				compress(event.target.files[0]).then( (data) => {

					this.image = data;

					let reader = new FileReader();

					reader.onload = (readerEvent) => {

						let value = readerEvent.target.result;

						$("#add-image img").show();
						$("#add-image div").attr("style", "display: none!important");

						$("#add-image img").attr("src", value);

					};

					reader.readAsDataURL(data);

				});
			},
		},
	});
</script>
@endpush