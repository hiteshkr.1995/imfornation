@extends('oversight.layouts.app')

@section('title', 'Blog View')

@push('plugins')

<script async charset="utf-8" src="//cdn.embedly.com/widgets/platform.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/ckeditor.css') }}">

@endpush

@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Blog View</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<span class="float-right">
					<a href="{{ route('oversight.blogs.edit', $blog->id) }}" class="btn btn-primary">Edit</a>
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete">
						Delete
					</button>
				</span>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">

						<h3 class="mb-5">{{ $blog->title }}</h3>

						<div class="text-center mb-5">
							<img src="{{ $blog->image_path }}" class="img-fluid">
						</div>
						<div class="ck-content">
							{!! $blog->content !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('oversight.blogs.delete')
@endsection

@push('scripts')
<script>
	document.querySelectorAll( 'oembed[url]' ).forEach( element => {
		const anchor = document.createElement( 'a' );

		anchor.setAttribute( 'href', element.getAttribute( 'url' ) );
		anchor.className = 'embedly-card';

		element.appendChild( anchor );
	} );
</script>
@endpush