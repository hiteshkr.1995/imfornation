<!-- Modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="deleteCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="deleteCenterTitle">Delete Modal</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<span class="text-danger">Are you sure you want to delete?</span>
			</div>
			<div class="modal-footer">
				<form action="{{ route('oversight.blogs.destroy', $blog->id) }}" method="POST" class="mr-auto">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" class="btn btn-primary">Yes</button>
				</form>

				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>