@extends('oversight.layouts.app')

@section('title', 'Storage')

@push('plugins')
@endpush

@section('content')
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Storage</h1>
			</div>
			<div class="col-sm-6">
			</div>
		</div>
	</div>
</section>
<!-- /.content-header -->

<?php

	$directory = request()->directory ?? '';

	$directories = Storage::disk('public')->directories($directory);
	$files = Storage::disk('public')->files($directory);

?>

<!-- Main content -->
<section class="content">

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info card-outline">

					<div class="card-header">
						<div class="row">
							<div class="col-md-6">
								<h3 class="card-title">Get all files here</h3>
							</div>
							<div class="col-md-6">
								<div class="card-tools float-right">
									<button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
						</div>
					</div>

					<div class="card-body">

						<h4 class="text-primary">Folders</h4>
						<hr class="mt-2 mb-3">

						<div class="container">
							@foreach($directories as $key => $value)
								<div class="row">
									<i class="material-icons text-warning">folder</i>
									<a href="{{ route('oversight.storage', ['directory' => $value]) }}" class="text-muted">{{ dir_name($value) }}</a>
								</div>
							@endforeach
						</div>

					</div>

					<div class="card-body">

						<h4 class="font-weight-light text-center text-lg-left mt-4 mb-0 text-primary">Thumbnail Gallery</h4>
						<hr class="mt-2 mb-5">

						<div class="container">

							<div class="row text-center text-lg-left">

								@foreach($files as $key => $value)
									<?php
										$split = explode( '/', Storage::disk('public')->mimeType($value) );
									?>

									@if($split[0] == 'image')
										<div class="col-lg-3 col-md-4 col-6">
											<a href="{{ asset_storage($value) }}" class="d-block mb-4 h-100">
												<img class="img-fluid img-thumbnail" src="{{ asset_storage($value) }}" alt="">
											</a>
										</div>
									@endif
								@endforeach

							</div>

						</div>

					</div>

					<div class="card-footer">
						Footer
					</div>

				</div>
			</div>
		</div>
	</div>

</section>
@endsection

@push('scripts')
@endpush