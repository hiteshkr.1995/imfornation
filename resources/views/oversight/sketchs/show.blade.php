@extends('oversight.layouts.app')

@section('title', 'Show Sketch')

@push('plugins')
<script async charset="utf-8" src="//cdn.embedly.com/widgets/platform.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/ckeditor.css') }}">
@endpush

@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Show Sketch</h1>
			</div>
			<div class="col-sm-6">
				<span class="float-right">
					<a href="{{ route('oversight.sketchs.edit', $sketch->id) }}" class="btn btn-primary">Edit</a>
				</span>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info card-outline">
					<div class="card-header">
						<h3 class="card-title">View<small> full sketch as admin</small></h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body">

						<article>
							<header>
								<h3>{{ $sketch['question']}}</h3>
								<div>
									<i class="fas fa-tag text-info"></i> {{ $sketch['category']['name'] }}
								</div>
								<hr class="custom">
								<div class="mb-3">
									Posted <time>{{ date('M d, Y', strtotime($sketch['created_at'])) }}</time> by <span><span>Admin</span></span>
								</div>
							</header>
							<div>
								<div class="text-center my-4">
									<img src="{{ $sketch['path'] }}" class="img-fluid">
								</div>

								<hr>

								<div class="ck-content">
									{!! $sketch['description'] !!}
								</div>
							</div>
						</article>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
	document.querySelectorAll( 'oembed[url]' ).forEach( element => {
		const anchor = document.createElement( 'a' );

		anchor.setAttribute( 'href', element.getAttribute( 'url' ) );
		anchor.className = 'embedly-card';

		element.appendChild( anchor );
	} );
</script>
@endpush