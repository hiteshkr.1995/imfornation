@extends('oversight.layouts.app')

@section('title', 'Sketchs')

@push('plugins')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

<style type="text/css">
	.dataTables_processing {
		z-index: 1!important;
		top: unset!important;
		height: 101.3%!important;
	}
	.dataTables_length {
		margin-right: 20px!important;
	}
</style>
@endpush

@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Sketchs</h1>
			</div>
			<div class="col-sm-6">
				<div class="float-right">
					<a href=" {{ route('oversight.sketchs.create') }} ">
						<button class="btn btn-success">Add Sketch</button>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info card-outline">
					<div class="card-header">
						<div class="row">
							<div class="col-md-6">
								<h3 class="card-title"><small>Listing of sketchs</small></h3>
							</div>
							<div class="col-md-6">
								<div class="card-tools float-right">
									<button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="card-body">

						<div class="table-responsive">
							<table id="sketchs" class="table-bordered display" style="width:100%" cellspacing="0">
								<thead>
									<tr>
										<th>#</th>
										<th>Sketch</th>
										<th>Title</th>
										<th>Category</th>
										<th>Views</th>
										<th>Created At</th>
										<th>Active</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>#</th>
										<th>Sketch</th>
										<th>Title</th>
										<th>Category</th>
										<th>Views</th>
										<th>Created At</th>
										<th>Active</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="deleteSketch" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="deleteSketchTitle">Delete Sketch</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="POST">
				@csrf
				{{ method_field('DELETE') }}
				<div class="modal-body">
					<strong class="text-danger">Are you sure you want to delete?</strong>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary mr-auto">Yes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://adminlte.io/themes/dev/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>

<script type="text/javascript">
	let sketchId = '';
	var table = {};
	$(document).ready(function() {
		table = $("#sketchs").DataTable({
			aaSorting: [
				[ 0, "desc"],
			],
			select: true,
			serverSide: true,
			responsive: true,
			processing: true,
			language: {
				processing: "Loading...",
			},
			scrollX: true,
			bJQueryUI: true,
			stateSave: true,
			ajax: {
				url: "{{ route('oversight.sketchs.datatable') }}",
				type: "POST",
			},
			iDisplayLength: 10,
			lengthMenu: [5,10,50,100,500],
			dom: 'lBfrtip',
			buttons: [
				{
					text: 'Reload',
					action: function ( e, dt, node, config ) {
						dt.ajax.reload( null, false );
					},
				},
			],
			columns: [
				{ data: "id", name: "id", width: "4%" },
				{ data: "image", name: "image", width: "16%" },
				{ data: "title", name: "title", width: "20%" },
				{ data: "category.name", name: "category.name", width: "11%" },
				{ data: "views", name: "views", width: "7%" },
				{ data: "created_at", name: "created_at", width: "21%" },
				{ data: "is_active", name: "is_active", orderable: false, searchable: false, width: "7%" },
				{ data: "action", name: "action", orderable: false, searchable: false, width: "20%" },
			],
		});
		$.fn.dataTable.ext.errMode = "none";

		$('#deleteSketch').on('hidden.bs.modal', function (event) {
			sketchId = '';
		});
		$("form").on("submit", (event) => {
			$.ajax({
				url: "{{ route('oversight.sketchs.index').'/' }}"+sketchId,
				type: 'DELETE',
				success: (result) => {
					$('#deleteSketch').modal('hide');
					table.ajax.reload( null, false );
				},
				error: (error) => {
					table.ajax.reload( null, false );
					console.log(error.statusText);
				}
			});
			event.preventDefault();
		});
	});

	function openDeleteModal(id) {
		sketchId = id;
		$('#deleteSketch').modal('show');
	}

	function isActive(id, event) {
		let data = {
			check : event.target.checked,
		};

		$.ajax({
			url: "{{ route('oversight.sketchs.index').'/is_active/' }}"+id,
			type: "POST",
			data : data,
			success: (result) => {
				table.ajax.reload( null, false );
			},
			error: (error) => {
				table.ajax.reload( null, false );
				console.log(error.statusText);
			}
		});
	}

</script>
@endpush