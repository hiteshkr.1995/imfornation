@extends('oversight.layouts.app')

@section('title', 'Edit Sketch')

@push('plugins')
@endpush

@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Edit Sketch</h1>
			</div>
			<div class="col-sm-6">
				<span class="float-right">
					<a href="{{ route('oversight.sketchs.show', $sketch->id) }}" class="btn btn-primary">Show</a>
				</span>
			</div>
		</div>
	</div>
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content" v-cloak>
	<div class="container-fluid">

		<form @submit.prevent="upload" action="" method="" enctype="multipart/form-data">

			<div class="row">
				<div class="col-md-6">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Basic Info</h3>
						</div>
						<div class="card-body">

							<div class="form-group">
								<label for="title">Article Title</label>
								<input id="title" type="text" class="form-control form-control-lg" v-bind:class="{ 'is-invalid': errors.title }" name="title" v-model="title" placeholder="Enter Title" required>
								<div class="errors" v-if="errors.title">
									<small class="text-danger" :key="error" v-for="error in errors.title">@{{ error }}</small>
								</div>
							</div>

							<div class="form-group">
								<label for="category" for="title">Select Category</label>
								<select id="category" class="form-control"  v-bind:class="{ 'is-invalid': errors.category }" name="category" v-model="category" required>
									<option disabled value="">Slelect Category</option>
									@foreach ($categories as $key => $value)
										<option value="{{ $value->id }}">{{ $value->name }}</option>
									@endforeach
								</select>
								<div class="errors" v-if="errors.category">
									<small class="text-danger" :key="error" v-for="error in errors.category">@{{ error }}</small>
								</div>
							</div>

						</div>
					</div>

					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Write Question & Select Correct Jumbel</h3>
						</div>
						<div class="card-body">

							<div class="form-group">
								<label for="question">Write Question</label>
								<input id="question" type="text" class="form-control form-control-lg" v-bind:class="{ 'is-invalid': errors.question }" name="question" v-model="question" placeholder="Enter question" required>
								<div class="errors" v-if="errors.question || errors.co_jumbel">
									<small class="text-danger" :key="error" v-for="error in errors.question">@{{ error }}</small>
								</div>
							</div>
							@verbatim

								<small v-bind:class="[ errors.co_jumbel ? 'text-danger' : 'text-info' ]" class="border-bottom" v-if="question">Select words to set fill in the blank:</small>

								<!-- Check box for fill in the blank -->
								<div class="row">

									<div
										class="col-4 mb-2"
										v-for="(value,key) in que_check"
										:key="key"
									>
										<div class="form-check">
											<input class="form-check-input" type="checkbox" :value="key" v-model="co_jumbel" :id="'check'+key">
											<label class="form-check-label" :for="'check'+key">
												{{ value }}
											</label>
										</div>
									</div>

								</div>

								<small class="border-bottom text-info" v-if="question">Write Extra Jumble <span class="text-danger">(OPTIONAL)</span></small>
								<div class="errors" v-if="errors.ea_jumbel || errors.co_jumbel">
									<small class="text-danger" :key="error" v-for="error in errors.ea_jumbel">{{ error }}</small>
								</div>
								<div class="row" v-if="question">

									<div
										v-for="(value,key) in ea_jumbel"
										:key="key"
										class="col-4 mb-2"
									>
										<input type="text" class="form-control form-control-sm" v-model="ea_jumbel[key]" placeholder="Enter new jumbel">
									</div>

								</div>
							@endverbatim

						</div>
					</div>

				</div>

				<div class="col-md-6">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Update Image <span class="text-danger">(Currently unavilable)</span></h3>
						</div>
						<div class="card-body">
							<img :src="image" class="img-fluid">
						</div>
					</div>

					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Paste or Enter Article urls</h3>
						</div>
						<div class="card-body">
							<label>Article urls</label>
								<small class="text-danger d-block" :key="error" v-for="error in errors.urls">@{{ error }}</small>
							<div
								class="mb-4"
								v-for="(value,key) in urls"
								:key="key"
							>
								<input type="url" class="form-control" v-model="urls[key]" placeholder="Paste or Enter Link">
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title">Write Description for your Article</h3>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label for="editor">Write Description</label>
						<textarea id="editor" class="form-control editor" v-bind:class="{ 'is-invalid': errors.description }" rows="5" name="description" v-model="description" placeholder="Write Description"></textarea>
						<div class="errors" v-if="errors.description">
							<small class="text-danger" :key="error" v-for="error in errors.description">@{{ error }}</small>
						</div>
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-body">
					<button id="sub_btn" type="submit" class="btn btn-lg btn-block btn-success">Update Sketch</button>
					<button id="lo_btn" type="button" class="btn btn-lg btn-block btn-success loading" style="display: none;" disabled>Loading...</button>
				</div>
			</div>

		</form>

	</div>
</section>
@endsection

@push('scripts')
<script>
	let description;
	const UploadUrl = '{{ route("oversight.upload") }}';
</script>
<script src="{{ asset('js/vue-picture-input/vue-picture-input.js') }}"></script>
<script src="{{ asset('js/imagecompressor/index.js') }}"></script>
<script src="{{ asset('js/ckeditor/classic.js') }}"></script>
<script src="{{ asset('js/ckeditor/image-upload.js') }}"></script>
<script type="text/javascript">

	new Vue({
		el: "#app",
		data: {
			response: @json($sketch),
			sketch: '',
			category: '',
			description: '',
			question: '',
			title: '',
			errors: [],
			co_jumbel: [],
			ea_jumbel: [""],
			urls: [""],
			que_check: [],
			image: "",
		},
		components: {
			'picture-input': PictureInput
		},
		mounted() {

			ClassicEditor.create( document.querySelector( '#editor' ), {

				extraPlugins: [ MyCustomUploadAdapterPlugin ],

			} ).then(editor => {

				description = editor;

			})
			.catch( error => {

				console.error( error );

			} );

			this.image = this.response.path;
			this.title = this.response.title;
			this.category = this.response.category_id;
			this.question = this.response.question;
			this.description = this.response.description;
			this.co_jumbel = this.response.co_jumbel;
			this.ea_jumbel = this.response.ea_jumbel;
			this.urls = this.response.urls;
		},
		watch: {
			urls: function (val) {
				let lastKey = (val.length-1);
				if (lastKey < 10) {
					if (this.urls[lastKey] != "") {
						if (this.urls[lastKey].length >= 1) {
							this.urls.push("");
						}
					}
				}
			},
			ea_jumbel: function (val) {
				let lastKey = (val.length-1);
				if (lastKey < 10) {
					if (this.ea_jumbel[lastKey] != "") {
						if (this.ea_jumbel[lastKey].length >= 1) {
							this.ea_jumbel.push("");
						}
					}
				}
			},

			question: function (val) {
				this.que_check = val.split(" ");
			},

		},
		methods: {
			onChange(image) {

				let file = $('input[type="file"]')[0].files[0];

				this.compress(file).then( (data) => {
					let reader = new FileReader();
					reader.onload = (readerEvent) => {
						this.sketch = readerEvent.target.result;
					};
					reader.readAsDataURL(data);
				});

			},
			compress(file) {
				return new Promise((resolve, reject) => {
					new ImageCompressor(file, {
						quality: .6,
						success(result) {
							resolve(result);
						},
						error(e) {
							reject(e);
						},
					});
				});
			},
			upload: function(event) {
				event.preventDefault();

				this.description = description.getData();

				$("#sub_btn").hide();
				$("#lo_btn").show();

				if (this.urls.length > 1) {
					this.urls = this.urls.filter( (el) => {
						return el != "";
					});
				}

				if (this.urls.length == 0) {
					this.urls = [""];
				}

				if (this.ea_jumbel.length > 1) {
					this.ea_jumbel = this.ea_jumbel.filter( (el) => {
						return el != "";
					})
				}

				if (this.ea_jumbel.length == 0) {
					this.ea_jumbel = [""];
				}

				axios.put("{{ route('oversight.sketchs.index') }}/"+this.response.id, {
					sketch: this.sketch,
					category: this.category,
					question: this.question,
					title: this.title,
					co_jumbel: this.co_jumbel,
					ea_jumbel: this.ea_jumbel,
					description: this.description,
					urls: this.urls,
				}).then((response) => {

					window.location.href = response.data;

				}).catch(({ response }) => {

					$("#sub_btn").show();
					$("#lo_btn").hide();

					if (response.status == 422) {
						this.errors = response.data.errors;
						if (this.errors.slug) {
							this.errors.question = this.errors.slug;
						}
					}

				});

			},
			handle_function_call(function_name) {
				this[function_name]();
			},

		},
	})

</script>
@endpush