<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield('title') | {{ config('app.name') }}</title>

		<!-- Fonts -->
		<link href="{{ asset('css/fonts.css') }}" rel="stylesheet" type="text/css">

		<style type="text/css">
			.form-control:focus {
				color: unset!important;
			}
			.form-control {
				color: unset!important;
			}
			/* Rules for sizing the icon. */
			.material-icons.md-18 { font-size: 18px; }
			.material-icons.md-24 { font-size: 24px; }
			.material-icons.md-36 { font-size: 36px; }
			.material-icons.md-48 { font-size: 48px; }

			/* Rules for using icons as black on a light background. */
			.material-icons.md-dark { color: rgba(0, 0, 0, 0.54); }
			.material-icons.md-dark.md-inactive { color: rgba(0, 0, 0, 0.26); }

			/* Rules for using icons as white on a dark background. */
			.material-icons.md-light { color: rgba(255, 255, 255, 1); }
			.material-icons.md-light.md-inactive { color: rgba(255, 255, 255, 0.3); }

			#add-image {
				cursor: pointer;
			}
			#add-image div {
				border: 0.25em dashed silver;
				padding: 15px;
			}
			@-webkit-keyframes spinner-border {
				to {
					-webkit-transform: rotate(360deg);
					transform: rotate(360deg);
				}
			}

			@keyframes spinner-border {
			  to {
			    -webkit-transform: rotate(360deg);
			    transform: rotate(360deg);
			  }
			}

			.spinner-border {
			  display: inline-block;
			  width: 2rem;
			  height: 2rem;
			  vertical-align: text-bottom;
			  border: 0.25em solid currentColor;
			  border-right-color: transparent;
			  border-radius: 50%!important;
			  -webkit-animation: spinner-border .75s linear infinite;
			  animation: spinner-border .75s linear infinite;
			}

			.spinner-border-sm {
			  width: 1rem;
			  height: 1rem;
			  border-width: 0.2em;
			}

			@-webkit-keyframes spinner-grow {
			  0% {
			    -webkit-transform: scale(0);
			    transform: scale(0);
			  }
			  50% {
			    opacity: 1;
			  }
			}

			@keyframes spinner-grow {
			  0% {
			    -webkit-transform: scale(0);
			    transform: scale(0);
			  }
			  50% {
			    opacity: 1;
			  }
			}

			.spinner-grow {
			  display: inline-block;
			  width: 2rem;
			  height: 2rem;
			  vertical-align: text-bottom;
			  background-color: currentColor;
			  border-radius: 50%;
			  opacity: 0;
			  -webkit-animation: spinner-grow .75s linear infinite;
			  animation: spinner-grow .75s linear infinite;
			}

			.spinner-grow-sm {
			  width: 1rem;
			  height: 1rem;
			}
		</style>

		<!-- Material Icons -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<!-- Theme style -->
		<link rel="stylesheet" href="https://adminlte.io/themes/dev/AdminLTE/dist/css/adminlte.min.css">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

		<style type="text/css">
			/*[v-cloak] {
				display: none;
			}*/
			[v-cloak] > * { display:none; }
			[v-cloak]::before { content: "loading..."; }
		</style>

		@stack('plugins')

	</head>
	<body class="hold-transition sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<!-- Navbar -->
		@include('oversight.layouts.nav')
		<!-- /.navbar -->

		<!-- Main Sidebar Container -->
		@include('oversight.layouts.aside')

		<!-- Content Wrapper. Contains page content -->
		<div id="app" class="content-wrapper">
			@include('shared.messages')
			<!-- Content Header (Page header) -->
			@yield('content')
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<!-- Main Footer -->
		@include('oversight.layouts.footer')
	</div>
	<!-- ./wrapper -->

	<!-- Modal -->
	<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logoutCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="logoutCenterTitle">Logout Modal</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<span class="text-info">Are you sure you want to logout?</span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary mr-5" data-dismiss="modal">Close</button>
					<form action="{{ route('oversight.logout') }}" method="POST">
						@csrf
						<button type="submit" class="btn btn-primary">Logout</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery -->
	<script src="https://adminlte.io/themes/dev/AdminLTE/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="https://adminlte.io/themes/dev/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE -->
	<script src="https://adminlte.io/themes/dev/AdminLTE/dist/js/adminlte.js"></script>
	<script src="{{ asset('js/vue/app.js') }}"></script>
	<script src="{{ asset('js/axios/axios.min.js') }}"></script>
	<script src="{{ asset('js/main.js') }}"></script>
	<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		})
	</script>

	@stack('scripts')

	<script type="text/javascript">
		let url = window.location;
		url = url.origin+url.pathname;
		$('ul.nav-sidebar a').filter(function() {
		    return this.href == url;
		}).siblings().removeClass('active').end().addClass('active');
		$('ul.treeview-menu a').filter(function() {
		    return this.href == url;
		}).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active menu-open').end().addClass('active menu-open');
	</script>
	</body>
</html>