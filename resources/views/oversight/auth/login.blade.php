<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Log In | {{ config('app.name') }}</title>

		<!-- Fonts -->
		<link href="{{ asset('css/fonts.css') }}" rel="stylesheet" type="text/css">

		<!-- Tell the browser to be responsive to screen width -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://adminlte.io/themes/dev/AdminLTE/dist/css/adminlte.min.css">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	</head>
	<body class="hold-transition login-page">
	<div class="container col-md-6 my-5">
		<div class="login-logo">
			<a href="">
				{{ config('app.name') }}
			</a>
		</div>
		<!-- /.login-logo -->
				<div class="card py-5">

					<div class="card-body">
						<form method="POST" action="{{ route('oversight.login.post') }}">
							@csrf
							<div class="form-group mt-4 row">
								<div class="col-md-12">
									<input type="text" class="form-control{{ $errors->has('oversight') ? ' is-invalid' : '' }}" name="oversight" required="" value="{{ old('oversight') }}" autofocus="" placeholder="Enter your credential">

									@if ($errors->has('oversight'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('oversight') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-12">
									<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required="" placeholder="Password">

									@if ($errors->has('password'))
									    <span class="invalid-feedback" role="alert">
									        <strong>{{ $errors->first('password') }}</strong>
									    </span>
									@endif
								</div>
							</div>

							<div class="form-group row mt-5">
								<div class="col-md-6">
									<div class="form-check">
										<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

										<label class="form-check-label" for="remember">
											Remember Me
										</label>
									</div>
								</div>
								<div class="col-md-6">
									<button type="submit" class="btn btn-success w-100 float-right">
										Log In
									</button>
								</div>
							</div>

						</form>
					</div>
				</div>


	</div>
	<!-- /.login-box -->
	</body>
</html>
