@extends('layouts.app')

@if(Route::is('cate.page.share'))

	@section('meta')

		<meta name="keywords" content="{{ $result[0]['title'] }}">
		<meta name="description" content="{{ str_limit( strip_tags($result[0]['description']), 150 ) }}">

		<meta property="og:url"           content="{{ $result[0]['urlShare'] }}" />
		<meta property="og:type"          content="{{ $result[0]['category']['name'] }}" />
		<meta property="og:title"         content="{{ $result[0]['title'] }}" />
		<meta property="og:description"   content="{{ str_limit( strip_tags($result[0]['description']), 150 ) }}" />
		<meta property="og:image"         content="{{ $result[0]['path'] }}" />

	@endsection

@endif

@if( Route::is('cate.page.share') )

	@section('title', $result[0]['title'])

@elseif( Route::is('cate.page') )

	@section('title', 'Category')

@else

	@section('title', 'Welcome')

@endif

@push('plugins')
<style type="text/css">
	.fill-blank {
		display: block;
		width: 100%;
		height: calc(2.25rem + 2px);
		padding: 0px;
		font-weight: bolder;
		margin: 2px;
		font-size: 1.5rem;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border-bottom: 6px solid #ced4da;
		border-top: unset; 
		border-right: unset;
		border-left: unset;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
	.fill-blank:focus {
		color: #495057;
		background-color: #fff;
		border-color: #80bdff;
		outline: 0;
		box-shadow: unset;
	}
	@media (max-width: 576px) {
		.card {
			border-right: 0px;
			border-left: 0px;
		}
		.container {
			padding-right: 0px;
			padding-left: 0px;
		}
	}
</style>

@endpush

@section('content')

	<!-- Generate card -->
	<div class="col-md-8 m-auto px-0">
		@if(!empty($result))
			@foreach($result as $key => $value)
				<div class="card shadow-lg rounded border-0 w-100 mb-5">
					<div class="card-header border-0 bg-transparent">
						<h5 class="card-title" style="margin-bottom: 0px;">{{ $value['title'] }}</h5>
					</div>
					<div class="card-body p-0 mx-auto w-100 text-center">
						<div @click="openModal({{ $value['id'] }})" style="cursor: pointer;">
							<img src="{{ $value['path'] }}" class="img-fluid rounded" style="min-height: 100%;max-height: 640px;">
						</div>
					</div>
					<div class="card-footer border-0 bg-transparent">
						<small>{{ $value['created'] }}</small>
						<span class="float-right">
							<strong class="text-info mr-3 small">
								<i class="fas fa-tag"></i>
								{{ $value['category']['name'] }}
							</strong>
							<strong @click="openShareModal('{{ $value['urlShare'] }}')" style="cursor: pointer;">
								<i class="fas fa-share-alt text-info"></i>
							</strong>
						</span>
					</div>
				</div>
			@endforeach

			<div class="card w-100 shadow-lg rounded border-0 mb-5" v-for="(value, key, index) in ajax_result" v-cloak>
				<div class="card-header border-0 bg-transparent">
						<h5 class="card-title" style="margin-bottom: 0px;">@{{ value.title }}</h5>
				</div>
				<div class="card-body p-0 mx-auto w-100 text-center">
					<div @click="openModal(value.id)" style="cursor: pointer;">
						<img :src="value.path" class="img-fluid rounded" style="min-height: 100%;max-height: 640px;">
					</div>
				</div>
				<div class="card-footer border-0 bg-transparent">
					<small>@{{ value.created }}</small>
					<span class="float-right">
						<strong class="text-info mr-3 small">
							<i class="fas fa-tag"></i>
							@{{ value.category.name }}
						</strong>
						<strong @click="openShareModal(value.urlShare)" style="cursor: pointer;">
							<i class="fas fa-share-alt text-info"></i>
						</strong>
					</span>
				</div>
			</div>

		@else
			<div class="card shadow rounded">
				<div class="card-body text-center">
					<h1 class="m-5">
						NEWS available soon...
					</h1>
				</div>
			</div>
		@endif
	</div>

	<!-- Load More Button -->
	<div v-cloak>
		<div id="ac_load" class="my-4 text-center" v-if="pagination.next_page_url">
			<button class="btn btn-outline-primary w-50" @click="getAjaxResult">Load More</button>
		</div>
		<div id="in_load" class="my-4 text-center" style="display: none;">
			<button class="btn btn-outline-primary w-50" disabled>Loading...</button>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row m-2" v-html="raw_que">
						@{{ raw_que }}
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="text-center">
						<div class="row" v-if="!views.includes(ac_result.id)">
							<div
								v-for="(value, key, index) in ac_result.jumbels"
								:key="key"
								@click="userAnswer(value, key, $event)"
								:style="{'font-weight': 'bold', 'cursor': 'pointer', 'font-size': '25px'}"
								class="col-6"
								:id="'op'+key"
							>
								<h4>@{{value}}</h4>
							</div>
						</div>
						<div v-if="views.includes(ac_result.id)">
							<span class="text-info">Read full article on:</span>
							<div class="row">
								<div
									v-for="(value, key, index) in ac_result.urls"
									:key="key"
									class="col-6 my-3"
								>
									<a :href="value.url" target="_blank">
										<img :src="value.icon" class="img-fluid">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a :href="ac_result.post" target="_blank" class="mr-auto">Post info</a>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header border-0">
					<h5 class="modal-title" id="shareModal">Share with any social media platform</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body row m-auto">
					<span class="mx-3">
						<iframe class="fab" :src="'https://www.facebook.com/plugins/share_button.php?href='+urlShare+'&layout=button&size=large&mobile_iframe=true&width=73&height=28&appId'" width="73" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
					</span>
					<span class="mx-3 fab">
						<a class="twitter-share-button"
							href="https://twitter.com/intent/tweet"
							data-size="large"
							:data-url="urlShare"
						>Tweet</a>
					</span>
					<span class="mx-3">
						@if (detect_mobile())
							<a :href="'whatsapp://send?text='+urlShare">
								{!! get_svg('whatsapp') !!}
							</a>
						@else
							<a target="_blank" class="fab" :href="'https://web.whatsapp.com/send?text='+urlShare">
								{!! get_svg('whatsapp') !!}
							</a>
						@endif
					</span>
				</div>
				<div class="modal-footer border-0">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

@endsection

@push('scripts')
<script>window.twttr = (function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0],
		t = window.twttr || {};
	if (d.getElementById(id)) return t;
	js = d.createElement(s);
	js.id = id;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);

	t._e = [];
	t.ready = function(f) {
		t._e.push(f);
	};

	return t;
}(document, "script", "twitter-wjs"));</script>
	@include('vue.index')
@endpush