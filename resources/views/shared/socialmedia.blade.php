<div class="container">
	<div class="row">

		<div class="mx-2">
			<iframe src="https://www.facebook.com/plugins/share_button.php?href={{ $urlShare }}&layout=button&size=large&mobile_iframe=true&width=73&height=28&appId" width="73" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
		</div>

		<div class="mx-2">
			<a class="twitter-share-button"
				href="https://twitter.com/intent/tweet"
				data-size="large"
				data-url="{{ $urlShare }}"
			>
			Tweet</a>
			<script>window.twttr = (function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0],
					t = window.twttr || {};
				if (d.getElementById(id)) return t;
				js = d.createElement(s);
				js.id = id;
				js.src = "https://platform.twitter.com/widgets.js";
				fjs.parentNode.insertBefore(js, fjs);

				t._e = [];
				t.ready = function(f) {
					t._e.push(f);
				};

				return t;
			}(document, "script", "twitter-wjs"));</script>
		</div>

		<div class="mx-2">
			@if (detect_mobile())
				<a href="whatsapp://send?text={{ $urlShare }}">
					{!! get_svg('whatsapp') !!}
				</a>
			@else
				<a target="_blank" href="https://web.whatsapp.com/send?text={{ $urlShare }}">
					{!! get_svg('whatsapp') !!}
				</a>
			@endif
		</div>

	</div>
</div>