@extends('layouts.app')

@section('title', 'Contact')

@push('plugins')
@endpush

@section('content')
<div class="row mb-5">
	<div class="col-lg-12 text-center">
		<h2 class=>Contact Us</h2>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<form action="{{ route('contact.send') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<input name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" placeholder="Your Name *" value="{{ old('name') }}" required autofocus>
						@if ($errors->has('name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<input name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" placeholder="Your Email *" value="{{ old('email') }}" required>
						@if ($errors->has('email'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<input name="subject" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" type="text" placeholder="Subject *" value="{{ old('subject') }}" required>
						@if ($errors->has('subject'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('subject') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<textarea name="message" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" style="height: 145px;" placeholder="Your Message *" required>{{ old('message') }}</textarea>
						@if ($errors->has('message'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('message') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="col-lg-12 text-center">
					<button class="btn btn-primary text-uppercase" type="submit">Send Message</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection

@push('scripts')
@endpush