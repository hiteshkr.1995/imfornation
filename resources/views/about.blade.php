@extends('layouts.app')

@section('title', 'About us')

@push('plugins')
<style type="text/css">
	.video-container {
		position: relative;
		padding-bottom: 56.25%;
		padding-top: 30px; height: 0; overflow: hidden;
	}

	.video-container iframe,
	.video-container object,
	.video-container embed {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
</style>
@endpush

@section('content')

@if(Auth::guard('oversight')->check())
	<a href="{{ route('about.edit') }}" class="btn btn-primary">Edit Content</a>
@endif

	<div class="row text-center">

		<div class="col-md-6">

			<div class="card rounded shadow">
				<div class="card-body">
					<h1 class="card-title"><strong>About us</strong></h1>
					<p class="card-text">{{ $content }}</p>
					<strong>{{ config('app.name') }}</strong>
					<a href="{{ route('disclaimer') }}">
						<small class="text-info">disclaimer</small>
					</a>
				</div>
			</div>

		</div>

		<div class="col-md-6 mt-md-0 mt-5">

			<div class="card rounded shadow" style="height: 100%;">
				<div class="card-body align-items-center d-flex justify-content-center">
					<div>
						<h2 class="card-title mb-5"><strong>Connect with us</strong></h2>
						<span class="mx-2">
							<a href="https://www.facebook.com/imfornation" target="_blank">
								<i class="fab fa-facebook fa-3x"></i>
							</a>
						</span>
						<span class="mx-2">
							<a href="https://twitter.com/imfornation1" target="_blank">
								<i class="fab fa-twitter fa-3x"></i>
							</a>
						</span>
						<span class="mx-2">
							<a href="https://www.instagram.com/imfornation" target="_blank">
								<i class="fab fa-instagram fa-3x"></i>
							</a>
						</span>
						<span class="mx-2">
							<a href="https://www.pinterest.com/imfornation" target="_blank">
								<i class="fab fa-pinterest fa-3x"></i>
							</a>
						</span>
						<span class="mx-2">
							<a href="https://www.youtube.com/channel/UCT6KEmRDGj2bVZUtG1TtIDA" target="_blank">
								<i class="fab fa-youtube fa-3x"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

		</div>

	</div>

	<div class="col-md-8 mx-auto mt-5 text-center">
		<div class="card rounded shadow">
			<div class="card-body">

				<h2 class="card-title">
					<strong>Watch on </strong><i class="fab fa-youtube"></i><strong style="color: #3d3d3d;">YouTube</strong>
				</h2>
				<div class="video-container">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/iGNtKQSvfRE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>

			</div>
		</div>
		
	</div>

@endsection

@push('scripts')
@endpush