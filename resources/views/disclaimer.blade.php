@extends('layouts.app')

@section('title', 'Disclaimer')

@push('plugins')
@endpush

@section('content')
<h1>Legal Disclaimer</h1> 
{{ config('app.name') }} uses reasonable care to make sure that the information appearing on this Website is accurate and up-to-date. However, errors and omissions do occur and the user should not take the accuracy of the information for granted but should check directly with Blue Seas Adjusters Limited. None of the material contained in this Website is to be relied upon as a statement or representation of fact.


<div class="my-5">
	<strong>Links to Third Party Sites</strong>
	<div>
		{{ config('app.name') }} cannot and has not reviewed all pages of the Websites linked to this Site and therefore cannot be liable for their content. Users link to other Websites at their own risk and use such sites according to the terms and conditions of use of such sites. {{ config('app.name') }} provides links to you only as a convenience, and the inclusion of any link does not imply endorsement by {{ config('app.name') }} or the Website.
	</div>
</div>

<div class="my-5">
	<strong>Copyright</strong>
	<div>
		No part of this Website may be copied or imitated in whole or in part. No logo, graphic, sound or image from this Website may be copied or re-transmitted unless expressly permitted in writing by {{ config('app.name') }}. (These prohibitions are without limitation to the legal rights of {{ config('app.name') }}.)
		Any information provided to {{ config('app.name') }} in connection with its Website shall be provided by the submitter and received by {{ config('app.name') }} on a non-confidential basis. {{ config('app.name') }} shall be free to use such information on an unrestricted basis. In particular, we may transfer information between our subsidiary companies in different domains.
	</div>
</div>

<div class="my-5">
	<strong>Trademarks</strong>
	<div>
		The trademarks and logos (the Trademarks) used and displayed on this Site are registered and unregistered trademarks of {{ config('app.name') }} and others. They may not be used in any advertising or other publicity materials in relation to the distribution of any information or materials obtained from this Website without the prior written consent of the Trademark owner. (These prohibitions are without limitation to the legal rights of {{ config('app.name') }}.)
	</div>
</div>

@endsection

@push('scripts')
@endpush