@extends('layouts.app')

@section('title', 'Welcome')

@push('plugins')

<link href="{{ asset('css/swiper/swiper.min.css') }}" rel="stylesheet">

<style type="text/css">
	.fill-blank {
		display: block;
		width: 100%;
		height: calc(2.25rem + 2px);
		padding: 0px;
		font-weight: bolder;
		margin: 2px;
		font-size: 1.5rem;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border-bottom: 6px solid #ced4da;
		border-top: unset; 
		border-right: unset;
		border-left: unset;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
	.fill-blank:focus {
		color: #495057;
		background-color: #fff;
		border-color: #80bdff;
		outline: 0;
		box-shadow: unset;
	}
	.card-body {
		padding: 0px;
	}
	.wrightAnswer {
		color: #4CAF50!important;
	}
	.wrongAnswer {
		color: #f34423!important;
	}
	@media (max-width: 576px) {
		.card {
			border-right: 0px;
			border-left: 0px;
		}
		.container {
			padding-right: 0px;
			padding-left: 0px;
		}
	}
	.swiper-button-prev, .swiper-button-next {
	}
</style>

@endpush

@section('content')
	@foreach($response as $key => $values)
		<div class="card w-100{{ ($loop->last) ? '' : ' mb-5' }}">
			<div class="card-header bg-transparent">
				<h5 class="card-title" style="margin-bottom: 0px;">{{ $key }}</h5>
			</div>
			<div class="card-body mx-auto w-100 text-center">
				<div class="swiper-container" style="cursor: pointer;">
					<div class="swiper-wrapper">
						@foreach($values as $key => $value)
							<div class="swiper-slide my-auto" @click="clickOptions({{$value['id']}})">
								<img src="{{ $value['sketch'] }}" class="img-fluid" style="min-height: 295px;">
							</div>
						@endforeach
					</div>

					<div class="swiper-button-next">
					</div>
					<div class="swiper-button-prev">
					</div>
				</div>
			</div>
			<div class="card-footer bg-transparent">
				Footer
			</div>
		</div>
	@endforeach

	<div class="modal fade" id="optionsModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row m-2" v-html="queRaw">
						@{{ queRaw }}
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="text-center">
						<div class="row" v-if="visited == 'notVisited'">
							<div
								v-for="(value, key, index) in options"
								:key="index"
								@click="userAnswer(value, key, index)"
								:style="{'font-weight': 'bold', 'cursor': 'pointer', 'font-size': '25px'}"
								class="col-sm-6"
								:id="'op'+key"
							>
								<h5>@{{value}}</h5>
							</div>
						</div>
						<div class="row" v-if="visited == 'visited'">
							<div
								v-for="(value, key, index) in links"
								:key="index"
								class="col-sm-6 my-3"
							>
								<a :href="value.link" target="_blank">
									<img :src="value.icon">
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script src="{{ asset('js/swiper/swiper.min.js') }}"></script>

<script type="text/javascript">

	window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
	window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || {READ_WRITE: "readwrite"};
	window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

	var myHeading = document.querySelector('h1');

	$(window).on('load', function() {

		let app = new Vue({
			el: "#app",
			data: {
				response: @json($response),
				options: [],
				myDBInstance: null,
				sketchId: null,
				selectedItem: null,
				clientAnswer: null,
				question: '',
				questionArray: [],
				genQuestion: [],
				queRaw: "",
				sketchs: [],
				totalFields: 0,
				trueEvent: [],
				views: [],
				visited: "",
				links: [],
			},
			mounted() {
				this.setSwiper()
				if (!localStorage.getItem('views')) {
					localStorage.setItem('views', JSON.stringify([]))
				} else {
					this.views = JSON.parse(localStorage.getItem('views'))
				}
				this.storeToArray()
				// console.log(this.sketchs)
			},

			methods: {
				setSwiper() {
					new Swiper('.swiper-container', {
						// loop: true,
						navigation: {
							nextEl: '.swiper-button-next',
							prevEl: '.swiper-button-prev',
						},
						// direction: 'horizontal',
						mousewheel: true,
						speed: 1000,
						spaceBetween: 100,
					});
				},
				clickOptions(id) {

					let clickOP = []

					$(".col-sm-6").removeClass('text-danger')
					$(".col-sm-6").removeClass('text-success')

					clickOP = this.sketchs[id]

					this.genQuestion = []
					this.totalFields = 0
					this.trueEvent = []

					this.visited = 'notVisited'

					this.options = JSON.parse(clickOP.jumbel)
					this.sketchId = clickOP.id
					this.links = clickOP.links
					this.question = clickOP.question
					this.questionArray = this.question.split(" ")
					this.currectAns = JSON.parse(clickOP.correct_jumbel)

					// console.log(this.questionArray)
					// console.log(this.currectAns)

					this.questionArray.forEach( (value, key) => {

						let str = '';

						if(this.currectAns.includes(value)) {

							++this.totalFields

							str = "<input id='word"+key+"' disabled class='col-4 text-center text-success fill-blank' type='text'>"
							this.genQuestion.push(str);

						} else {
							str = "<span class='mx-1 my-3'>"+value+"</span>"
							this.genQuestion.push(str);

						}

					})

					if (this.views.includes(id)) {
						this.visited = 'visited',
						this.queRaw = this.question
					} else {
						this.queRaw = this.genQuestion.join(" ")
					}

					$('#optionsModel').modal('toggle')

				},
				userAnswer(value, key, index) {
					let clickOP = []
					let questionKey = this.questionArray.indexOf(value)

					if(this.questionArray.includes(value)) {
						$('#word'+questionKey).val( () => {
							return value;
						});

						if (!this.trueEvent.includes(value)) {
							this.trueEvent.push(value)
						}

						$("#op"+key).addClass("text-success")

						if (this.trueEvent.length == this.totalFields) {
							this.visited = 'visited',
							this.queRaw = this.question
							$(".col-sm-6").removeClass('text-danger')
							$(".col-sm-6").removeClass('text-success')

							this.views.push(this.sketchId)
							localStorage.setItem('views', JSON.stringify(this.views))
						}

					} else {
						$("#op"+key).addClass("text-danger")
					}

				},
				storeToArray() {
					for ( let value in this.response ) {
						this.response[value].forEach( (element) => {
							this.sketchs[element.id] = {
								id: element.id,
								question: element.question,
								jumbel: element.jumbel,
								links: element.links,
								correct_jumbel: element.correct_jumbel,
							}
						});
					}
				},
			}
		})
	})

</script>

@endpush