@component('mail::message')

{{ config('app.name') }} have new contact.
# Details

#Name: {{ $name }}

#Email: {{ $email }}

#Subject: {{ $subject }}

#Message:
{{ $message }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
