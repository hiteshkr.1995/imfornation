@verbatim
<!DOCTYPE html>
<!--[if IE 8]>
<html xmlns="http://www.w3.org/1999/xhtml" class="ie8 wp-toolbar"  lang="en-US">
<![endif]-->
<!--[if !(IE 8) ]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" class="wp-toolbar"  lang="en-US">
<!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Add New With Laravel</title>
	<script type="text/javascript">
		addLoadEvent = function(func){if(typeof jQuery!="undefined")jQuery(document).ready(func);else if(typeof wpOnload!='function'){wpOnload=func;}else{var oldonload=wpOnload;wpOnload=function(){oldonload();func();}}};
		var ajaxurl = '/wordpress/wp-admin/admin-ajax.php',
			pagenow = 'post',
			typenow = 'post',
			adminpage = 'post-new-php',
			thousandsSeparator = ',',
			decimalPoint = '.',
			isRtl = 0;
	</script>
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<link rel='dns-prefetch' href='//s.w.org' />
	<style type="text/css">
	img.wp-smiley,
	img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 .07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}
	</style>
	<link rel='stylesheet' href='http://127.0.0.1/wordpress/wp-admin/load-styles.php?c=0&amp;dir=ltr&amp;load%5B%5D=dashicons,admin-bar,buttons,media-views,editor-buttons,wp-components,wp-nux,wp-editor,wp-block-library,wp-block-library-theme,wp&amp;load%5B%5D=-edit-blocks,wp-edit-post,wp-format-library,common,forms,admin-menu,dashboard,list-tables,edit,revisions,media,themes,about,nav-&amp;load%5B%5D=menus,wp-pointer,widgets,site-icon,l10n,wp-auth-check&amp;ver=5.0.3' type='text/css' media='all' />
	<link rel='stylesheet' id='mediaelement-css'  href='http://127.0.0.1/wordpress/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.6-78496d1' type='text/css' media='all' />
	<link rel='stylesheet' id='wp-mediaelement-css'  href='http://127.0.0.1/wordpress/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=5.0.3' type='text/css' media='all' />
	<link rel='stylesheet' id='imgareaselect-css'  href='http://127.0.0.1/wordpress/wp-includes/js/imgareaselect/imgareaselect.css?ver=0.9.8' type='text/css' media='all' />
	<link rel='stylesheet' id='wp-editor-font-css'  href='https://fonts.googleapis.com/css?family=Noto+Serif%3A400%2C400i%2C700%2C700i&#038;ver=5.0.3' type='text/css' media='all' />
	<link rel='stylesheet' id='twentynineteen-editor-customizer-styles-css'  href='http://127.0.0.1/wordpress/wp-content/themes/twentynineteen/style-editor-customizer.css?ver=1.1' type='text/css' media='all' />
	<!--[if lte IE 7]>
	<link rel='stylesheet' id='ie-css'  href='http://127.0.0.1/wordpress/wp-admin/css/ie.min.css?ver=5.0.3' type='text/css' media='all' />
	<![endif]-->
		<script>
			if ( typeof performance !== 'undefined' && performance.navigation && performance.navigation.type === 2 ) {
				document.location.reload( true );
			}
		</script>
		
	<script type='text/javascript'>
	/* <![CDATA[ */
	var userSettings = {"url":"\/wordpress\/","uid":"1","time":"1551117324","secure":""};/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-admin/load-scripts.php?c=0&amp;load%5B%5D=jquery-core,jquery-migrate,utils,moxiejs,plupload&amp;ver=5.0.3'></script>
	<!--[if lt IE 8]>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/json2.min.js?ver=2015-05-03'></script>
	<![endif]-->
	<script type='text/javascript'>
	var mejsL10n = {"language":"en","strings":{"mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Turn off Fullscreen","mejs.fullscreen-on":"Go Fullscreen","mejs.download-video":"Download Video","mejs.fullscreen":"Fullscreen","mejs.time-jump-forward":["Jump forward 1 second","Jump forward %1 seconds"],"mejs.loop":"Toggle Loop","mejs.play":"Play","mejs.pause":"Pause","mejs.close":"Close","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.time-skip-back":["Skip back 1 second","Skip back %1 seconds"],"mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.mute-toggle":"Mute Toggle","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Skip ad","mejs.ad-skip-info":["Skip in 1 second","Skip in %1 seconds"],"mejs.source-chooser":"Source Chooser","mejs.stop":"Stop","mejs.speed-rate":"Speed Rate","mejs.live-broadcast":"Live Broadcast","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var _wpmejsSettings = {"pluginPath":"\/wordpress\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/tinymce/tinymce.min.js?ver=4800-20180716'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/tinymce/plugins/compat3x/plugin.min.js?ver=4800-20180716'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/tinymce/plugins/lists/plugin.min.js?ver=4800-20180716'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/wp-embed.min.js?ver=5.0.3'></script>
		<link id="wp-admin-canonical" rel="canonical" href="http://127.0.0.1/wordpress/wp-admin/post-new.php" />
		<script>
			/*if ( window.history.replaceState ) {
				window.history.replaceState( null, null, document.getElementById( 'wp-admin-canonical' ).href + window.location.hash );
			}*/
		</script>
	<script type="text/javascript">var _wpColorScheme = {"icons":{"base":"#82878c","focus":"#00a0d2","current":"#fff"}};</script>
	<style type="text/css" media="print">#wpadminbar { display:none; }</style>
</head>
<body class="wp-admin wp-core-ui no-js  post-new-php auto-fold admin-bar post-type-post branch-5 version-5-0-3 admin-color-fresh locale-en-us no-customize-support no-svg block-editor-page is-fullscreen-mode wp-embed-responsive">
	<script type="text/javascript">
		document.body.className = document.body.className.replace('no-js','js');
	</script>

		<!--[if lte IE 8]>
			<script type="text/javascript">
				document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
			</script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
			<script type="text/javascript">
				(function() {
					var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

							request = true;
			
					b[c] = b[c].replace( rcs, ' ' );
					// The customizer requires postMessage and CORS (if the site is cross domain)
					b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
				}());
			</script>
		<!--<![endif]-->
		
	<div id="wpwrap">

	<div id="wpcontent">
			
	<div id="wpbody" role="main">

	<div id="wpbody-content" aria-label="Main content" tabindex="0">
			<div id="screen-meta" class="metabox-prefs">

				<div id="contextual-help-wrap" class="hidden no-sidebar" tabindex="-1" aria-label="Contextual Help Tab">
					<div id="contextual-help-back"></div>
					<div id="contextual-help-columns">
						<div class="contextual-help-tabs">
							<ul>
													</ul>
						</div>

						
						<div class="contextual-help-tabs-wrap">
												</div>
					</div>
				</div>
			<div id="screen-options-wrap" class="hidden" tabindex="-1" aria-label="Screen Options Tab">
	<form id='adv-settings' method='post'>
			<fieldset class="metabox-prefs">
			<legend>Boxes</legend>
			<label for="categorydiv-hide"><input class="hide-postbox-tog" name="categorydiv-hide" type="checkbox" id="categorydiv-hide" value="categorydiv"  checked='checked' />Categories</label><label for="tagsdiv-post_tag-hide"><input class="hide-postbox-tog" name="tagsdiv-post_tag-hide" type="checkbox" id="tagsdiv-post_tag-hide" value="tagsdiv-post_tag"  checked='checked' />Tags</label><label for="postimagediv-hide"><input class="hide-postbox-tog" name="postimagediv-hide" type="checkbox" id="postimagediv-hide" value="postimagediv"  checked='checked' />Featured Image</label><label for="postexcerpt-hide"><input class="hide-postbox-tog" name="postexcerpt-hide" type="checkbox" id="postexcerpt-hide" value="postexcerpt"  />Excerpt</label><label for="trackbacksdiv-hide"><input class="hide-postbox-tog" name="trackbacksdiv-hide" type="checkbox" id="trackbacksdiv-hide" value="trackbacksdiv"  />Send Trackbacks</label><label for="postcustom-hide"><input class="hide-postbox-tog" name="postcustom-hide" type="checkbox" id="postcustom-hide" value="postcustom"  />Custom Fields</label><label for="commentstatusdiv-hide"><input class="hide-postbox-tog" name="commentstatusdiv-hide" type="checkbox" id="commentstatusdiv-hide" value="commentstatusdiv"  />Discussion</label><label for="slugdiv-hide"><input class="hide-postbox-tog" name="slugdiv-hide" type="checkbox" id="slugdiv-hide" value="slugdiv"  />Slug</label><label for="authordiv-hide"><input class="hide-postbox-tog" name="authordiv-hide" type="checkbox" id="authordiv-hide" value="authordiv"  />Author</label>		</fieldset>
			<fieldset class="editor-expand hidden"><legend>Additional settings</legend><label for="editor-expand-toggle"><input type="checkbox" id="editor-expand-toggle" checked='checked' />Enable full-height editor and distraction-free functionality.</label></fieldset>
	<input type="hidden" id="screenoptionnonce" name="screenoptionnonce" value="6c880aff76" />
	</form>
	</div>		</div>
					<div id="screen-meta-links">
						<div id="screen-options-link-wrap" class="hide-if-no-js screen-meta-toggle">
				<button type="button" id="show-settings-link" class="button show-settings" aria-controls="screen-options-wrap" aria-expanded="false">Screen Options</button>
				</div>
					</div>
			<div class='update-nag'><a href="https://codex.wordpress.org/Version_5.1">WordPress 5.1</a> is available! <a href="http://127.0.0.1/wordpress/wp-admin/update-core.php" aria-label="Please update WordPress now">Please update now</a>.</div>
	<div class="block-editor">
		<h1 class="screen-reader-text hide-if-no-js">Add New Post</h1>
		<div id="editor" class="block-editor__container hide-if-no-js"></div>
		<div id="metaboxes" class="hidden">
				<form class="metabox-base-form">
		<input type="hidden" id="_wpnonce" name="_wpnonce" value="368d226bd0" /><input type="hidden" name="_wp_http_referer" value="/wordpress/wp-admin/post-new.php" />	<input type="hidden" id="user-id" name="user_ID" value="1" />
		<input type="hidden" id="hiddenaction" name="action" value="editpost" />
		<input type="hidden" id="originalaction" name="originalaction" value="editpost" />
		<input type="hidden" id="post_type" name="post_type" value="post" />
		<input type="hidden" id="original_post_status" name="original_post_status" value="auto-draft" />
		<input type="hidden" id="referredby" name="referredby" value="http://127.0.0.1/wordpress/wp-admin/edit.php" />

		<input type="hidden" name="_wp_original_http_referer" value="http://127.0.0.1/wordpress/wp-admin/edit.php" /><input type='hidden' id='auto_draft' name='auto_draft' value='1' /><input type='hidden' id='post_ID' name='post_ID' value='17' /><input type="hidden" id="meta-box-order-nonce" name="meta-box-order-nonce" value="0280b3ae76" /><input type="hidden" id="closedpostboxesnonce" name="closedpostboxesnonce" value="03d3087781" /><input type="hidden" id="samplepermalinknonce" name="samplepermalinknonce" value="317e383499" />	</form>
		<form id="toggle-custom-fields-form" method="post" action="http://127.0.0.1/wordpress/wp-admin/post.php">
			<input type="hidden" id="_wpnonce" name="_wpnonce" value="a90c92f304" /><input type="hidden" name="_wp_http_referer" value="/wordpress/wp-admin/post-new.php" />		<input type="hidden" name="action" value="toggle-custom-fields" />
		</form>
				<form class="metabox-location-side" onsubmit="return false;">
				<div id="poststuff" class="sidebar-open">
					<div id="postbox-container-2" class="postbox-container">
						<div id="side-sortables" class="meta-box-sortables"></div>				</div>
				</div>
			</form>
				<form class="metabox-location-normal" onsubmit="return false;">
				<div id="poststuff" class="sidebar-open">
					<div id="postbox-container-2" class="postbox-container">
						<div id="normal-sortables" class="meta-box-sortables"></div>				</div>
				</div>
			</form>
				<form class="metabox-location-advanced" onsubmit="return false;">
				<div id="poststuff" class="sidebar-open">
					<div id="postbox-container-2" class="postbox-container">
						<div id="advanced-sortables" class="meta-box-sortables"></div>				</div>
				</div>
			</form>
				</div>

			<div class="wrap hide-if-js block-editor-no-js">
			<h1 class="wp-heading-inline">Add New Post</h1>
			<div class="notice notice-error notice-alt">
				<p>
					The block editor requires JavaScript. Please enable JavaScript in your browser settings, or try the <a href="https://wordpress.org/plugins/classic-editor/">Classic Editor plugin</a>.			</p>
			</div>
		</div>
	</div>

	<div class="clear"></div></div><!-- wpbody-content -->
	<div class="clear"></div></div><!-- wpbody -->
	<div class="clear"></div></div><!-- wpcontent -->

	<div id="wpfooter" role="contentinfo">
			<p id="footer-left" class="alignleft">
			<span id="footer-thankyou">Thank you for creating with <a href="https://wordpress.org/">WordPress</a>.</span>	</p>
		<p id="footer-upgrade" class="alignright">
			<strong><a href="http://127.0.0.1/wordpress/wp-admin/update-core.php">Get Version 5.1</a></strong>	</p>
		<div class="clear"></div>
	</div>
		<!--[if lte IE 8]>
		<style>
			.attachment:focus {
				outline: #1e8cbe solid;
			}
			.selected.attachment {
				outline: #1e8cbe solid;
			}
		</style>
		<![endif]-->
		<script type="text/html" id="tmpl-media-frame">
			<div class="media-frame-menu"></div>
			<div class="media-frame-title"></div>
			<div class="media-frame-router"></div>
			<div class="media-frame-content"></div>
			<div class="media-frame-toolbar"></div>
			<div class="media-frame-uploader"></div>
		</script>

		<script type="text/html" id="tmpl-media-modal">
			<div tabindex="0" class="media-modal wp-core-ui">
				<button type="button" class="media-modal-close"><span class="media-modal-icon"><span class="screen-reader-text">Close media panel</span></span></button>
				<div class="media-modal-content"></div>
			</div>
			<div class="media-modal-backdrop"></div>
		</script>

		<script type="text/html" id="tmpl-uploader-window">
			<div class="uploader-window-content">
				<h1>Drop files to upload</h1>
			</div>
		</script>

		<script type="text/html" id="tmpl-uploader-editor">
			<div class="uploader-editor-content">
				<div class="uploader-editor-title">Drop files to upload</div>
			</div>
		</script>

		<script type="text/html" id="tmpl-uploader-inline">
			<# var messageClass = data.message ? 'has-upload-message' : 'no-upload-message'; #>
			<# if ( data.canClose ) { #>
			<button class="close dashicons dashicons-no"><span class="screen-reader-text">Close uploader</span></button>
			<# } #>
			<div class="uploader-inline-content {{ messageClass }}">
			<# if ( data.message ) { #>
				<h2 class="upload-message">{{ data.message }}</h2>
			<# } #>
						<div class="upload-ui">
					<h2 class="upload-instructions drop-instructions">Drop files anywhere to upload</h2>
					<p class="upload-instructions drop-instructions">or</p>
					<button type="button" class="browser button button-hero">Select Files</button>
				</div>

				<div class="upload-inline-status"></div>

				<div class="post-upload-ui">
					
					<p class="max-upload-size">Maximum upload file size: 2 MB.</p>

					<# if ( data.suggestedWidth && data.suggestedHeight ) { #>
						<p class="suggested-dimensions">
							Suggested image dimensions: {{data.suggestedWidth}} by {{data.suggestedHeight}} pixels.					</p>
					<# } #>

								</div>
					</div>
		</script>

		<script type="text/html" id="tmpl-media-library-view-switcher">
			<a href="/wordpress/wp-admin/post-new.php?mode=list" class="view-list">
				<span class="screen-reader-text">List View</span>
			</a>
			<a href="/wordpress/wp-admin/post-new.php?mode=grid" class="view-grid current">
				<span class="screen-reader-text">Grid View</span>
			</a>
		</script>

		<script type="text/html" id="tmpl-uploader-status">
			<h2>Uploading</h2>
			<button type="button" class="button-link upload-dismiss-errors"><span class="screen-reader-text">Dismiss Errors</span></button>

			<div class="media-progress-bar"><div></div></div>
			<div class="upload-details">
				<span class="upload-count">
					<span class="upload-index"></span> / <span class="upload-total"></span>
				</span>
				<span class="upload-detail-separator">&ndash;</span>
				<span class="upload-filename"></span>
			</div>
			<div class="upload-errors"></div>
		</script>

		<script type="text/html" id="tmpl-uploader-status-error">
			<span class="upload-error-filename">{{{ data.filename }}}</span>
			<span class="upload-error-message">{{ data.message }}</span>
		</script>

		<script type="text/html" id="tmpl-edit-attachment-frame">
			<div class="edit-media-header">
				<button class="left dashicons <# if ( ! data.hasPrevious ) { #> disabled <# } #>"><span class="screen-reader-text">Edit previous media item</span></button>
				<button class="right dashicons <# if ( ! data.hasNext ) { #> disabled <# } #>"><span class="screen-reader-text">Edit next media item</span></button>
			</div>
			<div class="media-frame-title"></div>
			<div class="media-frame-content"></div>
		</script>

		<script type="text/html" id="tmpl-attachment-details-two-column">
			<div class="attachment-media-view {{ data.orientation }}">
				<div class="thumbnail thumbnail-{{ data.type }}">
					<# if ( data.uploading ) { #>
						<div class="media-progress-bar"><div></div></div>
					<# } else if ( data.sizes && data.sizes.large ) { #>
						<img class="details-image" src="{{ data.sizes.large.url }}" draggable="false" alt="" />
					<# } else if ( data.sizes && data.sizes.full ) { #>
						<img class="details-image" src="{{ data.sizes.full.url }}" draggable="false" alt="" />
					<# } else if ( -1 === jQuery.inArray( data.type, [ 'audio', 'video' ] ) ) { #>
						<img class="details-image icon" src="{{ data.icon }}" draggable="false" alt="" />
					<# } #>

					<# if ( 'audio' === data.type ) { #>
					<div class="wp-media-wrapper">
						<audio style="visibility: hidden" controls class="wp-audio-shortcode" width="100%" preload="none">
							<source type="{{ data.mime }}" src="{{ data.url }}"/>
						</audio>
					</div>
					<# } else if ( 'video' === data.type ) {
						var w_rule = '';
						if ( data.width ) {
							w_rule = 'width: ' + data.width + 'px;';
						} else if ( wp.media.view.settings.contentWidth ) {
							w_rule = 'width: ' + wp.media.view.settings.contentWidth + 'px;';
						}
					#>
					<div style="{{ w_rule }}" class="wp-media-wrapper wp-video">
						<video controls="controls" class="wp-video-shortcode" preload="metadata"
							<# if ( data.width ) { #>width="{{ data.width }}"<# } #>
							<# if ( data.height ) { #>height="{{ data.height }}"<# } #>
							<# if ( data.image && data.image.src !== data.icon ) { #>poster="{{ data.image.src }}"<# } #>>
							<source type="{{ data.mime }}" src="{{ data.url }}"/>
						</video>
					</div>
					<# } #>

					<div class="attachment-actions">
						<# if ( 'image' === data.type && ! data.uploading && data.sizes && data.can.save ) { #>
						<button type="button" class="button edit-attachment">Edit Image</button>
						<# } else if ( 'pdf' === data.subtype && data.sizes ) { #>
						Document Preview					<# } #>
					</div>
				</div>
			</div>
			<div class="attachment-info">
				<span class="settings-save-status">
					<span class="spinner"></span>
					<span class="saved">Saved.</span>
				</span>
				<div class="details">
					<div class="filename"><strong>File name:</strong> {{ data.filename }}</div>
					<div class="filename"><strong>File type:</strong> {{ data.mime }}</div>
					<div class="uploaded"><strong>Uploaded on:</strong> {{ data.dateFormatted }}</div>

					<div class="file-size"><strong>File size:</strong> {{ data.filesizeHumanReadable }}</div>
					<# if ( 'image' === data.type && ! data.uploading ) { #>
						<# if ( data.width && data.height ) { #>
							<div class="dimensions"><strong>Dimensions:</strong> {{ data.width }} &times; {{ data.height }}</div>
						<# } #>
					<# } #>

					<# if ( data.fileLength ) { #>
						<div class="file-length"><strong>Length:</strong> {{ data.fileLength }}</div>
					<# } #>

					<# if ( 'audio' === data.type && data.meta.bitrate ) { #>
						<div class="bitrate">
							<strong>Bitrate:</strong> {{ Math.round( data.meta.bitrate / 1000 ) }}kb/s
							<# if ( data.meta.bitrate_mode ) { #>
							{{ ' ' + data.meta.bitrate_mode.toUpperCase() }}
							<# } #>
						</div>
					<# } #>

					<div class="compat-meta">
						<# if ( data.compat && data.compat.meta ) { #>
							{{{ data.compat.meta }}}
						<# } #>
					</div>
				</div>

				<div class="settings">
					<label class="setting" data-setting="url">
						<span class="name">URL</span>
						<input type="text" value="{{ data.url }}" readonly />
					</label>
					<# var maybeReadOnly = data.can.save || data.allowLocalEdits ? '' : 'readonly'; #>
									<label class="setting" data-setting="title">
						<span class="name">Title</span>
						<input type="text" value="{{ data.title }}" {{ maybeReadOnly }} />
					</label>
									<# if ( 'audio' === data.type ) { #>
									<label class="setting" data-setting="artist">
						<span class="name">Artist</span>
						<input type="text" value="{{ data.artist || data.meta.artist || '' }}" />
					</label>
									<label class="setting" data-setting="album">
						<span class="name">Album</span>
						<input type="text" value="{{ data.album || data.meta.album || '' }}" />
					</label>
									<# } #>
					<label class="setting" data-setting="caption">
						<span class="name">Caption</span>
						<textarea {{ maybeReadOnly }}>{{ data.caption }}</textarea>
					</label>
					<# if ( 'image' === data.type ) { #>
						<label class="setting" data-setting="alt">
							<span class="name">Alt Text</span>
							<input type="text" value="{{ data.alt }}" {{ maybeReadOnly }} />
						</label>
					<# } #>
					<label class="setting" data-setting="description">
						<span class="name">Description</span>
						<textarea {{ maybeReadOnly }}>{{ data.description }}</textarea>
					</label>
					<div class="setting">
						<span class="name">Uploaded By</span>
						<span class="value">{{ data.authorName }}</span>
					</div>
					<# if ( data.uploadedToTitle ) { #>
						<div class="setting">
							<span class="name">Uploaded To</span>
							<# if ( data.uploadedToLink ) { #>
								<span class="value"><a href="{{ data.uploadedToLink }}">{{ data.uploadedToTitle }}</a></span>
							<# } else { #>
								<span class="value">{{ data.uploadedToTitle }}</span>
							<# } #>
						</div>
					<# } #>
					<div class="attachment-compat"></div>
				</div>

				<div class="actions">
					<a class="view-attachment" href="{{ data.link }}">View attachment page</a>
					<# if ( data.can.save ) { #> |
						<a href="{{ data.editLink }}">Edit more details</a>
					<# } #>
					<# if ( ! data.uploading && data.can.remove ) { #> |
												<button type="button" class="button-link delete-attachment">Delete Permanently</button>
										<# } #>
				</div>

			</div>
		</script>

		<script type="text/html" id="tmpl-attachment">
			<div class="attachment-preview js--select-attachment type-{{ data.type }} subtype-{{ data.subtype }} {{ data.orientation }}">
				<div class="thumbnail">
					<# if ( data.uploading ) { #>
						<div class="media-progress-bar"><div style="width: {{ data.percent }}%"></div></div>
					<# } else if ( 'image' === data.type && data.sizes ) { #>
						<div class="centered">
							<img src="{{ data.size.url }}" draggable="false" alt="" />
						</div>
					<# } else { #>
						<div class="centered">
							<# if ( data.image && data.image.src && data.image.src !== data.icon ) { #>
								<img src="{{ data.image.src }}" class="thumbnail" draggable="false" alt="" />
							<# } else if ( data.sizes && data.sizes.medium ) { #>
								<img src="{{ data.sizes.medium.url }}" class="thumbnail" draggable="false" alt="" />
							<# } else { #>
								<img src="{{ data.icon }}" class="icon" draggable="false" alt="" />
							<# } #>
						</div>
						<div class="filename">
							<div>{{ data.filename }}</div>
						</div>
					<# } #>
				</div>
				<# if ( data.buttons.close ) { #>
					<button type="button" class="button-link attachment-close media-modal-icon"><span class="screen-reader-text">Remove</span></button>
				<# } #>
			</div>
			<# if ( data.buttons.check ) { #>
				<button type="button" class="check" tabindex="-1"><span class="media-modal-icon"></span><span class="screen-reader-text">Deselect</span></button>
			<# } #>
			<#
			var maybeReadOnly = data.can.save || data.allowLocalEdits ? '' : 'readonly';
			if ( data.describe ) {
				if ( 'image' === data.type ) { #>
					<input type="text" value="{{ data.caption }}" class="describe" data-setting="caption"
						placeholder="Caption this image&hellip;" {{ maybeReadOnly }} />
				<# } else { #>
					<input type="text" value="{{ data.title }}" class="describe" data-setting="title"
						<# if ( 'video' === data.type ) { #>
							placeholder="Describe this video&hellip;"
						<# } else if ( 'audio' === data.type ) { #>
							placeholder="Describe this audio file&hellip;"
						<# } else { #>
							placeholder="Describe this media file&hellip;"
						<# } #> {{ maybeReadOnly }} />
				<# }
			} #>
		</script>

		<script type="text/html" id="tmpl-attachment-details">
			<h2>
				Attachment Details			<span class="settings-save-status">
					<span class="spinner"></span>
					<span class="saved">Saved.</span>
				</span>
			</h2>
			<div class="attachment-info">
				<div class="thumbnail thumbnail-{{ data.type }}">
					<# if ( data.uploading ) { #>
						<div class="media-progress-bar"><div></div></div>
					<# } else if ( 'image' === data.type && data.sizes ) { #>
						<img src="{{ data.size.url }}" draggable="false" alt="" />
					<# } else { #>
						<img src="{{ data.icon }}" class="icon" draggable="false" alt="" />
					<# } #>
				</div>
				<div class="details">
					<div class="filename">{{ data.filename }}</div>
					<div class="uploaded">{{ data.dateFormatted }}</div>

					<div class="file-size">{{ data.filesizeHumanReadable }}</div>
					<# if ( 'image' === data.type && ! data.uploading ) { #>
						<# if ( data.width && data.height ) { #>
							<div class="dimensions">{{ data.width }} &times; {{ data.height }}</div>
						<# } #>

						<# if ( data.can.save && data.sizes ) { #>
							<a class="edit-attachment" href="{{ data.editLink }}&amp;image-editor" target="_blank">Edit Image</a>
						<# } #>
					<# } #>

					<# if ( data.fileLength ) { #>
						<div class="file-length">Length: {{ data.fileLength }}</div>
					<# } #>

					<# if ( ! data.uploading && data.can.remove ) { #>
												<button type="button" class="button-link delete-attachment">Delete Permanently</button>
										<# } #>

					<div class="compat-meta">
						<# if ( data.compat && data.compat.meta ) { #>
							{{{ data.compat.meta }}}
						<# } #>
					</div>
				</div>
			</div>

			<label class="setting" data-setting="url">
				<span class="name">URL</span>
				<input type="text" value="{{ data.url }}" readonly />
			</label>
			<# var maybeReadOnly = data.can.save || data.allowLocalEdits ? '' : 'readonly'; #>
					<label class="setting" data-setting="title">
				<span class="name">Title</span>
				<input type="text" value="{{ data.title }}" {{ maybeReadOnly }} />
			</label>
					<# if ( 'audio' === data.type ) { #>
					<label class="setting" data-setting="artist">
				<span class="name">Artist</span>
				<input type="text" value="{{ data.artist || data.meta.artist || '' }}" />
			</label>
					<label class="setting" data-setting="album">
				<span class="name">Album</span>
				<input type="text" value="{{ data.album || data.meta.album || '' }}" />
			</label>
					<# } #>
			<label class="setting" data-setting="caption">
				<span class="name">Caption</span>
				<textarea {{ maybeReadOnly }}>{{ data.caption }}</textarea>
			</label>
			<# if ( 'image' === data.type ) { #>
				<label class="setting" data-setting="alt">
					<span class="name">Alt Text</span>
					<input type="text" value="{{ data.alt }}" {{ maybeReadOnly }} />
				</label>
			<# } #>
			<label class="setting" data-setting="description">
				<span class="name">Description</span>
				<textarea {{ maybeReadOnly }}>{{ data.description }}</textarea>
			</label>
		</script>

		<script type="text/html" id="tmpl-media-selection">
			<div class="selection-info">
				<span class="count"></span>
				<# if ( data.editable ) { #>
					<button type="button" class="button-link edit-selection">Edit Selection</button>
				<# } #>
				<# if ( data.clearable ) { #>
					<button type="button" class="button-link clear-selection">Clear</button>
				<# } #>
			</div>
			<div class="selection-view"></div>
		</script>

		<script type="text/html" id="tmpl-attachment-display-settings">
			<h2>Attachment Display Settings</h2>

			<# if ( 'image' === data.type ) { #>
				<label class="setting align">
					<span>Alignment</span>
					<select class="alignment"
						data-setting="align"
						<# if ( data.userSettings ) { #>
							data-user-setting="align"
						<# } #>>

						<option value="left">
							Left					</option>
						<option value="center">
							Center					</option>
						<option value="right">
							Right					</option>
						<option value="none" selected>
							None					</option>
					</select>
				</label>
			<# } #>

			<div class="setting">
				<label>
					<# if ( data.model.canEmbed ) { #>
						<span>Embed or Link</span>
					<# } else { #>
						<span>Link To</span>
					<# } #>

					<select class="link-to"
						data-setting="link"
						<# if ( data.userSettings && ! data.model.canEmbed ) { #>
							data-user-setting="urlbutton"
						<# } #>>

					<# if ( data.model.canEmbed ) { #>
						<option value="embed" selected>
							Embed Media Player					</option>
						<option value="file">
					<# } else { #>
						<option value="none" selected>
							None					</option>
						<option value="file">
					<# } #>
						<# if ( data.model.canEmbed ) { #>
							Link to Media File					<# } else { #>
							Media File					<# } #>
						</option>
						<option value="post">
						<# if ( data.model.canEmbed ) { #>
							Link to Attachment Page					<# } else { #>
							Attachment Page					<# } #>
						</option>
					<# if ( 'image' === data.type ) { #>
						<option value="custom">
							Custom URL					</option>
					<# } #>
					</select>
				</label>
				<input type="text" class="link-to-custom" data-setting="linkUrl" />
			</div>

			<# if ( 'undefined' !== typeof data.sizes ) { #>
				<label class="setting">
					<span>Size</span>
					<select class="size" name="size"
						data-setting="size"
						<# if ( data.userSettings ) { #>
							data-user-setting="imgsize"
						<# } #>>
												<#
							var size = data.sizes['thumbnail'];
							if ( size ) { #>
								<option value="thumbnail" >
									Thumbnail &ndash; {{ size.width }} &times; {{ size.height }}
								</option>
							<# } #>
												<#
							var size = data.sizes['medium'];
							if ( size ) { #>
								<option value="medium" >
									Medium &ndash; {{ size.width }} &times; {{ size.height }}
								</option>
							<# } #>
												<#
							var size = data.sizes['large'];
							if ( size ) { #>
								<option value="large" >
									Large &ndash; {{ size.width }} &times; {{ size.height }}
								</option>
							<# } #>
												<#
							var size = data.sizes['full'];
							if ( size ) { #>
								<option value="full"  selected='selected'>
									Full Size &ndash; {{ size.width }} &times; {{ size.height }}
								</option>
							<# } #>
										</select>
				</label>
			<# } #>
		</script>

		<script type="text/html" id="tmpl-gallery-settings">
			<h2>Gallery Settings</h2>

			<label class="setting">
				<span>Link To</span>
				<select class="link-to"
					data-setting="link"
					<# if ( data.userSettings ) { #>
						data-user-setting="urlbutton"
					<# } #>>

					<option value="post" <# if ( ! wp.media.galleryDefaults.link || 'post' == wp.media.galleryDefaults.link ) {
						#>selected="selected"<# }
					#>>
						Attachment Page				</option>
					<option value="file" <# if ( 'file' == wp.media.galleryDefaults.link ) { #>selected="selected"<# } #>>
						Media File				</option>
					<option value="none" <# if ( 'none' == wp.media.galleryDefaults.link ) { #>selected="selected"<# } #>>
						None				</option>
				</select>
			</label>

			<label class="setting">
				<span>Columns</span>
				<select class="columns" name="columns"
					data-setting="columns">
										<option value="1" <#
							if ( 1 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
						#>>
							1					</option>
										<option value="2" <#
							if ( 2 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
						#>>
							2					</option>
										<option value="3" <#
							if ( 3 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
						#>>
							3					</option>
										<option value="4" <#
							if ( 4 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
						#>>
							4					</option>
										<option value="5" <#
							if ( 5 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
						#>>
							5					</option>
										<option value="6" <#
							if ( 6 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
						#>>
							6					</option>
										<option value="7" <#
							if ( 7 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
						#>>
							7					</option>
										<option value="8" <#
							if ( 8 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
						#>>
							8					</option>
										<option value="9" <#
							if ( 9 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
						#>>
							9					</option>
								</select>
			</label>

			<label class="setting">
				<span>Random Order</span>
				<input type="checkbox" data-setting="_orderbyRandom" />
			</label>

			<label class="setting size">
				<span>Size</span>
				<select class="size" name="size"
					data-setting="size"
					<# if ( data.userSettings ) { #>
						data-user-setting="imgsize"
					<# } #>
					>
										<option value="thumbnail">
							Thumbnail					</option>
										<option value="medium">
							Medium					</option>
										<option value="large">
							Large					</option>
										<option value="full">
							Full Size					</option>
								</select>
			</label>
		</script>

		<script type="text/html" id="tmpl-playlist-settings">
			<h2>Playlist Settings</h2>

			<# var emptyModel = _.isEmpty( data.model ),
				isVideo = 'video' === data.controller.get('library').props.get('type'); #>

			<label class="setting">
				<input type="checkbox" data-setting="tracklist" <# if ( emptyModel ) { #>
					checked="checked"
				<# } #> />
				<# if ( isVideo ) { #>
				<span>Show Video List</span>
				<# } else { #>
				<span>Show Tracklist</span>
				<# } #>
			</label>

			<# if ( ! isVideo ) { #>
			<label class="setting">
				<input type="checkbox" data-setting="artists" <# if ( emptyModel ) { #>
					checked="checked"
				<# } #> />
				<span>Show Artist Name in Tracklist</span>
			</label>
			<# } #>

			<label class="setting">
				<input type="checkbox" data-setting="images" <# if ( emptyModel ) { #>
					checked="checked"
				<# } #> />
				<span>Show Images</span>
			</label>
		</script>

		<script type="text/html" id="tmpl-embed-link-settings">
			<label class="setting link-text">
				<span>Link Text</span>
				<input type="text" class="alignment" data-setting="linkText" />
			</label>
			<div class="embed-container" style="display: none;">
				<div class="embed-preview"></div>
			</div>
		</script>

		<script type="text/html" id="tmpl-embed-image-settings">
			<div class="thumbnail">
				<img src="{{ data.model.url }}" draggable="false" alt="" />
			</div>

						<label class="setting caption">
					<span>Caption</span>
					<textarea data-setting="caption" />
				</label>
			
			<label class="setting alt-text">
				<span>Alt Text</span>
				<input type="text" data-setting="alt" />
			</label>

			<div class="setting align">
				<span>Align</span>
				<div class="button-group button-large" data-setting="align">
					<button class="button" value="left">
						Left				</button>
					<button class="button" value="center">
						Center				</button>
					<button class="button" value="right">
						Right				</button>
					<button class="button active" value="none">
						None				</button>
				</div>
			</div>

			<div class="setting link-to">
				<span>Link To</span>
				<div class="button-group button-large" data-setting="link">
					<button class="button" value="file">
						Image URL				</button>
					<button class="button" value="custom">
						Custom URL				</button>
					<button class="button active" value="none">
						None				</button>
				</div>
				<input type="text" class="link-to-custom" data-setting="linkUrl" />
			</div>
		</script>

		<script type="text/html" id="tmpl-image-details">
			<div class="media-embed">
				<div class="embed-media-settings">
					<div class="column-image">
						<div class="image">
							<img src="{{ data.model.url }}" draggable="false" alt="" />

							<# if ( data.attachment && window.imageEdit ) { #>
								<div class="actions">
									<input type="button" class="edit-attachment button" value="Edit Original" />
									<input type="button" class="replace-attachment button" value="Replace" />
								</div>
							<# } #>
						</div>
					</div>
					<div class="column-settings">
												<label class="setting caption">
								<span>Caption</span>
								<textarea data-setting="caption">{{ data.model.caption }}</textarea>
							</label>
						
						<label class="setting alt-text">
							<span>Alternative Text</span>
							<input type="text" data-setting="alt" value="{{ data.model.alt }}" />
						</label>

						<h2>Display Settings</h2>
						<div class="setting align">
							<span>Align</span>
							<div class="button-group button-large" data-setting="align">
								<button class="button" value="left">
									Left							</button>
								<button class="button" value="center">
									Center							</button>
								<button class="button" value="right">
									Right							</button>
								<button class="button active" value="none">
									None							</button>
							</div>
						</div>

						<# if ( data.attachment ) { #>
							<# if ( 'undefined' !== typeof data.attachment.sizes ) { #>
								<label class="setting size">
									<span>Size</span>
									<select class="size" name="size"
										data-setting="size"
										<# if ( data.userSettings ) { #>
											data-user-setting="imgsize"
										<# } #>>
																				<#
											var size = data.sizes['thumbnail'];
											if ( size ) { #>
												<option value="thumbnail">
													Thumbnail &ndash; {{ size.width }} &times; {{ size.height }}
												</option>
											<# } #>
																				<#
											var size = data.sizes['medium'];
											if ( size ) { #>
												<option value="medium">
													Medium &ndash; {{ size.width }} &times; {{ size.height }}
												</option>
											<# } #>
																				<#
											var size = data.sizes['large'];
											if ( size ) { #>
												<option value="large">
													Large &ndash; {{ size.width }} &times; {{ size.height }}
												</option>
											<# } #>
																				<#
											var size = data.sizes['full'];
											if ( size ) { #>
												<option value="full">
													Full Size &ndash; {{ size.width }} &times; {{ size.height }}
												</option>
											<# } #>
																			<option value="custom">
											Custom Size									</option>
									</select>
								</label>
							<# } #>
								<div class="custom-size<# if ( data.model.size !== 'custom' ) { #> hidden<# } #>">
									<label><span>Width <small>(px)</small></span> <input data-setting="customWidth" type="number" step="1" value="{{ data.model.customWidth }}" /></label><span class="sep">&times;</span><label><span>Height <small>(px)</small></span><input data-setting="customHeight" type="number" step="1" value="{{ data.model.customHeight }}" /></label>
								</div>
						<# } #>

						<div class="setting link-to">
							<span>Link To</span>
							<select data-setting="link">
							<# if ( data.attachment ) { #>
								<option value="file">
									Media File							</option>
								<option value="post">
									Attachment Page							</option>
							<# } else { #>
								<option value="file">
									Image URL							</option>
							<# } #>
								<option value="custom">
									Custom URL							</option>
								<option value="none">
									None							</option>
							</select>
							<input type="text" class="link-to-custom" data-setting="linkUrl" />
						</div>
						<div class="advanced-section">
							<h2><button type="button" class="button-link advanced-toggle">Advanced Options</button></h2>
							<div class="advanced-settings hidden">
								<div class="advanced-image">
									<label class="setting title-text">
										<span>Image Title Attribute</span>
										<input type="text" data-setting="title" value="{{ data.model.title }}" />
									</label>
									<label class="setting extra-classes">
										<span>Image CSS Class</span>
										<input type="text" data-setting="extraClasses" value="{{ data.model.extraClasses }}" />
									</label>
								</div>
								<div class="advanced-link">
									<div class="setting link-target">
										<label><input type="checkbox" data-setting="linkTargetBlank" value="_blank" <# if ( data.model.linkTargetBlank ) { #>checked="checked"<# } #>>Open link in a new tab</label>
									</div>
									<label class="setting link-rel">
										<span>Link Rel</span>
										<input type="text" data-setting="linkRel" value="{{ data.model.linkRel }}" />
									</label>
									<label class="setting link-class-name">
										<span>Link CSS Class</span>
										<input type="text" data-setting="linkClassName" value="{{ data.model.linkClassName }}" />
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</script>

		<script type="text/html" id="tmpl-image-editor">
			<div id="media-head-{{ data.id }}"></div>
			<div id="image-editor-{{ data.id }}"></div>
		</script>

		<script type="text/html" id="tmpl-audio-details">
			<# var ext, html5types = {
				mp3: wp.media.view.settings.embedMimes.mp3,
				ogg: wp.media.view.settings.embedMimes.ogg
			}; #>

					<div class="media-embed media-embed-details">
				<div class="embed-media-settings embed-audio-settings">
					<audio style="visibility: hidden"
		controls
		class="wp-audio-shortcode"
		width="{{ _.isUndefined( data.model.width ) ? 400 : data.model.width }}"
		preload="{{ _.isUndefined( data.model.preload ) ? 'none' : data.model.preload }}"
		<#
		if ( ! _.isUndefined( data.model.autoplay ) && data.model.autoplay ) {
			#> autoplay<#
		}
		if ( ! _.isUndefined( data.model.loop ) && data.model.loop ) {
			#> loop<#
		}
		#>
	>
		<# if ( ! _.isEmpty( data.model.src ) ) { #>
		<source src="{{ data.model.src }}" type="{{ wp.media.view.settings.embedMimes[ data.model.src.split('.').pop() ] }}" />
		<# } #>

		<# if ( ! _.isEmpty( data.model.mp3 ) ) { #>
		<source src="{{ data.model.mp3 }}" type="{{ wp.media.view.settings.embedMimes[ 'mp3' ] }}" />
		<# } #>
		<# if ( ! _.isEmpty( data.model.ogg ) ) { #>
		<source src="{{ data.model.ogg }}" type="{{ wp.media.view.settings.embedMimes[ 'ogg' ] }}" />
		<# } #>
		<# if ( ! _.isEmpty( data.model.flac ) ) { #>
		<source src="{{ data.model.flac }}" type="{{ wp.media.view.settings.embedMimes[ 'flac' ] }}" />
		<# } #>
		<# if ( ! _.isEmpty( data.model.m4a ) ) { #>
		<source src="{{ data.model.m4a }}" type="{{ wp.media.view.settings.embedMimes[ 'm4a' ] }}" />
		<# } #>
		<# if ( ! _.isEmpty( data.model.wav ) ) { #>
		<source src="{{ data.model.wav }}" type="{{ wp.media.view.settings.embedMimes[ 'wav' ] }}" />
		<# } #>
		</audio>

					<# if ( ! _.isEmpty( data.model.src ) ) {
						ext = data.model.src.split('.').pop();
						if ( html5types[ ext ] ) {
							delete html5types[ ext ];
						}
					#>
					<div class="setting">
						<label for="audio-source">URL</label>
						<input type="text" id="audio-source" readonly data-setting="src" value="{{ data.model.src }}" />
						<button type="button" class="button-link remove-setting">Remove audio source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.mp3 ) ) {
						if ( ! _.isUndefined( html5types.mp3 ) ) {
							delete html5types.mp3;
						}
					#>
					<div class="setting">
						<label for="mp3-source">MP3</span>
						<input type="text" id="mp3-source" readonly data-setting="mp3" value="{{ data.model.mp3 }}" />
						<button type="button" class="button-link remove-setting">Remove audio source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.ogg ) ) {
						if ( ! _.isUndefined( html5types.ogg ) ) {
							delete html5types.ogg;
						}
					#>
					<div class="setting">
						<label for="ogg-source">OGG</span>
						<input type="text" id="ogg-source" readonly data-setting="ogg" value="{{ data.model.ogg }}" />
						<button type="button" class="button-link remove-setting">Remove audio source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.flac ) ) {
						if ( ! _.isUndefined( html5types.flac ) ) {
							delete html5types.flac;
						}
					#>
					<div class="setting">
						<label for="flac-source">FLAC</span>
						<input type="text" id="flac-source" readonly data-setting="flac" value="{{ data.model.flac }}" />
						<button type="button" class="button-link remove-setting">Remove audio source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.m4a ) ) {
						if ( ! _.isUndefined( html5types.m4a ) ) {
							delete html5types.m4a;
						}
					#>
					<div class="setting">
						<label for="m4a-source">M4A</span>
						<input type="text" id="m4a-source" readonly data-setting="m4a" value="{{ data.model.m4a }}" />
						<button type="button" class="button-link remove-setting">Remove audio source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.wav ) ) {
						if ( ! _.isUndefined( html5types.wav ) ) {
							delete html5types.wav;
						}
					#>
					<div class="setting">
						<label for="wav-source">WAV</span>
						<input type="text" id="wav-source" readonly data-setting="wav" value="{{ data.model.wav }}" />
						<button type="button" class="button-link remove-setting">Remove audio source</button>
					</div>
					<# } #>
					
					<# if ( ! _.isEmpty( html5types ) ) { #>
					<div class="setting">
						<span>Add alternate sources for maximum HTML5 playback:</span>
						<div class="button-large">
						<# _.each( html5types, function (mime, type) { #>
						<button class="button add-media-source" data-mime="{{ mime }}">{{ type }}</button>
						<# } ) #>
						</div>
					</div>
					<# } #>

					<div class="setting preload">
						<span>Preload</span>
						<div class="button-group button-large" data-setting="preload">
							<button class="button" value="auto">Auto</button>
							<button class="button" value="metadata">Metadata</button>
							<button class="button active" value="none">None</button>
						</div>
					</div>

					<label class="setting checkbox-setting autoplay">
						<input type="checkbox" data-setting="autoplay" />
						<span>Autoplay</span>
					</label>

					<label class="setting checkbox-setting">
						<input type="checkbox" data-setting="loop" />
						<span>Loop</span>
					</label>
				</div>
			</div>
		</script>

		<script type="text/html" id="tmpl-video-details">
			<# var ext, html5types = {
				mp4: wp.media.view.settings.embedMimes.mp4,
				ogv: wp.media.view.settings.embedMimes.ogv,
				webm: wp.media.view.settings.embedMimes.webm
			}; #>

					<div class="media-embed media-embed-details">
				<div class="embed-media-settings embed-video-settings">
					<div class="wp-video-holder">
					<#
					var w = ! data.model.width || data.model.width > 640 ? 640 : data.model.width,
						h = ! data.model.height ? 360 : data.model.height;

					if ( data.model.width && w !== data.model.width ) {
						h = Math.ceil( ( h * w ) / data.model.width );
					}
					#>

					<#  var w_rule = '', classes = [],
			w, h, settings = wp.media.view.settings,
			isYouTube = isVimeo = false;

		if ( ! _.isEmpty( data.model.src ) ) {
			isYouTube = data.model.src.match(/youtube|youtu\.be/);
			isVimeo = -1 !== data.model.src.indexOf('vimeo');
		}

		if ( settings.contentWidth && data.model.width >= settings.contentWidth ) {
			w = settings.contentWidth;
		} else {
			w = data.model.width;
		}

		if ( w !== data.model.width ) {
			h = Math.ceil( ( data.model.height * w ) / data.model.width );
		} else {
			h = data.model.height;
		}

		if ( w ) {
			w_rule = 'width: ' + w + 'px; ';
		}

		if ( isYouTube ) {
			classes.push( 'youtube-video' );
		}

		if ( isVimeo ) {
			classes.push( 'vimeo-video' );
		}

	#>
	<div style="{{ w_rule }}" class="wp-video">
	<video controls
		class="wp-video-shortcode {{ classes.join( ' ' ) }}"
		<# if ( w ) { #>width="{{ w }}"<# } #>
		<# if ( h ) { #>height="{{ h }}"<# } #>
		<#
			if ( ! _.isUndefined( data.model.poster ) && data.model.poster ) {
				#> poster="{{ data.model.poster }}"<#
			} #>
			preload="{{ _.isUndefined( data.model.preload ) ? 'metadata' : data.model.preload }}"<#
		 if ( ! _.isUndefined( data.model.autoplay ) && data.model.autoplay ) {
			#> autoplay<#
		}
		 if ( ! _.isUndefined( data.model.loop ) && data.model.loop ) {
			#> loop<#
		}
		#>
	>
		<# if ( ! _.isEmpty( data.model.src ) ) {
			if ( isYouTube ) { #>
			<source src="{{ data.model.src }}" type="video/youtube" />
			<# } else if ( isVimeo ) { #>
			<source src="{{ data.model.src }}" type="video/vimeo" />
			<# } else { #>
			<source src="{{ data.model.src }}" type="{{ settings.embedMimes[ data.model.src.split('.').pop() ] }}" />
			<# }
		} #>

		<# if ( data.model.mp4 ) { #>
		<source src="{{ data.model.mp4 }}" type="{{ settings.embedMimes[ 'mp4' ] }}" />
		<# } #>
		<# if ( data.model.m4v ) { #>
		<source src="{{ data.model.m4v }}" type="{{ settings.embedMimes[ 'm4v' ] }}" />
		<# } #>
		<# if ( data.model.webm ) { #>
		<source src="{{ data.model.webm }}" type="{{ settings.embedMimes[ 'webm' ] }}" />
		<# } #>
		<# if ( data.model.ogv ) { #>
		<source src="{{ data.model.ogv }}" type="{{ settings.embedMimes[ 'ogv' ] }}" />
		<# } #>
		<# if ( data.model.flv ) { #>
		<source src="{{ data.model.flv }}" type="{{ settings.embedMimes[ 'flv' ] }}" />
		<# } #>
			{{{ data.model.content }}}
	</video>
	</div>

					<# if ( ! _.isEmpty( data.model.src ) ) {
						ext = data.model.src.split('.').pop();
						if ( html5types[ ext ] ) {
							delete html5types[ ext ];
						}
					#>
					<div class="setting">
						<label for="video-source">URL</label>
						<input type="text" id="video-source" readonly data-setting="src" value="{{ data.model.src }}" />
						<button type="button" class="button-link remove-setting">Remove video source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.mp4 ) ) {
						if ( ! _.isUndefined( html5types.mp4 ) ) {
							delete html5types.mp4;
						}
					#>
					<div class="setting">
						<label for="mp4-source">MP4</label>
						<input type="text" id="mp4-source" readonly data-setting="mp4" value="{{ data.model.mp4 }}" />
						<button type="button" class="button-link remove-setting">Remove video source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.m4v ) ) {
						if ( ! _.isUndefined( html5types.m4v ) ) {
							delete html5types.m4v;
						}
					#>
					<div class="setting">
						<label for="m4v-source">M4V</label>
						<input type="text" id="m4v-source" readonly data-setting="m4v" value="{{ data.model.m4v }}" />
						<button type="button" class="button-link remove-setting">Remove video source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.webm ) ) {
						if ( ! _.isUndefined( html5types.webm ) ) {
							delete html5types.webm;
						}
					#>
					<div class="setting">
						<label for="webm-source">WEBM</label>
						<input type="text" id="webm-source" readonly data-setting="webm" value="{{ data.model.webm }}" />
						<button type="button" class="button-link remove-setting">Remove video source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.ogv ) ) {
						if ( ! _.isUndefined( html5types.ogv ) ) {
							delete html5types.ogv;
						}
					#>
					<div class="setting">
						<label for="ogv-source">OGV</label>
						<input type="text" id="ogv-source" readonly data-setting="ogv" value="{{ data.model.ogv }}" />
						<button type="button" class="button-link remove-setting">Remove video source</button>
					</div>
					<# } #>
					<# if ( ! _.isEmpty( data.model.flv ) ) {
						if ( ! _.isUndefined( html5types.flv ) ) {
							delete html5types.flv;
						}
					#>
					<div class="setting">
						<label for="flv-source">FLV</label>
						<input type="text" id="flv-source" readonly data-setting="flv" value="{{ data.model.flv }}" />
						<button type="button" class="button-link remove-setting">Remove video source</button>
					</div>
					<# } #>
									</div>

					<# if ( ! _.isEmpty( html5types ) ) { #>
					<div class="setting">
						<span>Add alternate sources for maximum HTML5 playback:</span>
						<div class="button-large">
						<# _.each( html5types, function (mime, type) { #>
						<button class="button add-media-source" data-mime="{{ mime }}">{{ type }}</button>
						<# } ) #>
						</div>
					</div>
					<# } #>

					<# if ( ! _.isEmpty( data.model.poster ) ) { #>
					<div class="setting">
						<label for="poster-image">Poster Image</label>
						<input type="text" id="poster-image" readonly data-setting="poster" value="{{ data.model.poster }}" />
						<button type="button" class="button-link remove-setting">Remove poster image</button>
					</div>
					<# } #>
					<div class="setting preload">
						<span>Preload</span>
						<div class="button-group button-large" data-setting="preload">
							<button class="button" value="auto">Auto</button>
							<button class="button" value="metadata">Metadata</button>
							<button class="button active" value="none">None</button>
						</div>
					</div>

					<label class="setting checkbox-setting autoplay">
						<input type="checkbox" data-setting="autoplay" />
						<span>Autoplay</span>
					</label>

					<label class="setting checkbox-setting">
						<input type="checkbox" data-setting="loop" />
						<span>Loop</span>
					</label>

					<div class="setting" data-setting="content">
						<#
						var content = '';
						if ( ! _.isEmpty( data.model.content ) ) {
							var tracks = jQuery( data.model.content ).filter( 'track' );
							_.each( tracks.toArray(), function (track) {
								content += track.outerHTML; #>
							<label for="video-track">Tracks (subtitles, captions, descriptions, chapters, or metadata)</span>
							<input class="content-track" type="text" id="video-track" readonly value="{{ track.outerHTML }}" />
							<button type="button" class="button-link remove-setting remove-track">Remove video track</button>
							<# } ); #>
						<# } else { #>
						<span>Tracks (subtitles, captions, descriptions, chapters, or metadata)</span>
						<em>There are no associated subtitles.</em>
						<# } #>
						<textarea class="hidden content-setting">{{ content }}</textarea>
					</div>
				</div>
			</div>
		</script>

		<script type="text/html" id="tmpl-editor-gallery">
			<# if ( data.attachments.length ) { #>
				<div class="gallery gallery-columns-{{ data.columns }}">
					<# _.each( data.attachments, function( attachment, index ) { #>
						<dl class="gallery-item">
							<dt class="gallery-icon">
								<# if ( attachment.thumbnail ) { #>
									<img src="{{ attachment.thumbnail.url }}" width="{{ attachment.thumbnail.width }}" height="{{ attachment.thumbnail.height }}" alt="" />
								<# } else { #>
									<img src="{{ attachment.url }}" alt="" />
								<# } #>
							</dt>
							<# if ( attachment.caption ) { #>
								<dd class="wp-caption-text gallery-caption">
									{{{ data.verifyHTML( attachment.caption ) }}}
								</dd>
							<# } #>
						</dl>
						<# if ( index % data.columns === data.columns - 1 ) { #>
							<br style="clear: both;">
						<# } #>
					<# } ); #>
				</div>
			<# } else { #>
				<div class="wpview-error">
					<div class="dashicons dashicons-format-gallery"></div><p>No items found.</p>
				</div>
			<# } #>
		</script>

		<script type="text/html" id="tmpl-crop-content">
			<img class="crop-image" src="{{ data.url }}" alt="Image crop area preview. Requires mouse interaction.">
			<div class="upload-errors"></div>
		</script>

		<script type="text/html" id="tmpl-site-icon-preview">
			<h2>Preview</h2>
			<strong aria-hidden="true">As a browser icon</strong>
			<div class="favicon-preview">
				<img src="http://127.0.0.1/wordpress/wp-admin/images/browser.png" class="browser-preview" width="182" height="" alt="" />

				<div class="favicon">
					<img id="preview-favicon" src="{{ data.url }}" alt="Preview as a browser icon"/>
				</div>
				<span class="browser-title" aria-hidden="true">Laravel</span>
			</div>

			<strong aria-hidden="true">As an app icon</strong>
			<div class="app-icon-preview">
				<img id="preview-app-icon" src="{{ data.url }}" alt="Preview as an app icon"/>
			</div>
		</script>

			<div id="wp-auth-check-wrap" class="hidden">
		<div id="wp-auth-check-bg"></div>
		<div id="wp-auth-check">
		<button type="button" class="wp-auth-check-close button-link"><span class="screen-reader-text">Close dialog</span></button>
				<div id="wp-auth-check-form" class="loading" data-src="http://127.0.0.1/wordpress/wp-login.php?interim-login=1&#038;wp_lang=en_US"></div>
				<div class="wp-auth-fallback">
			<p><b class="wp-auth-fallback-expired" tabindex="0">Session expired</b></p>
			<p><a href="http://127.0.0.1/wordpress/wp-login.php" target="_blank">Please log in again.</a>
			The login page will open in a new window. After logging in you can close it and return to this page.</p>
		</div>
		</div>
		</div>
		
	<script type='text/javascript'>
	/* <![CDATA[ */
	var commonL10n = {"warnDelete":"You are about to permanently delete these items from your site.\nThis action cannot be undone.\n 'Cancel' to stop, 'OK' to delete.","dismiss":"Dismiss this notice.","collapseMenu":"Collapse Main menu","expandMenu":"Expand Main menu"};/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-admin/load-scripts.php?c=0&amp;load%5B%5D=hoverIntent,common,admin-bar&amp;ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=7.0.0'></script>
	<script type='text/javascript'>
	( 'fetch' in window ) || document.write( '<script src="http://127.0.0.1/wordpress/wp-includes/js/dist/vendor/wp-polyfill-fetch.min.js?ver=3.0.0"></scr' + 'ipt>' );( document.contains ) || document.write( '<script src="http://127.0.0.1/wordpress/wp-includes/js/dist/vendor/wp-polyfill-node-contains.min.js?ver=3.26.0-0"></scr' + 'ipt>' );( window.FormData && window.FormData.prototype.keys ) || document.write( '<script src="http://127.0.0.1/wordpress/wp-includes/js/dist/vendor/wp-polyfill-formdata.min.js?ver=3.0.12"></scr' + 'ipt>' );( Element.prototype.matches && Element.prototype.closest ) || document.write( '<script src="http://127.0.0.1/wordpress/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min.js?ver=2.0.2"></scr' + 'ipt>' );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/hooks.min.js?ver=2.0.4'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var heartbeatSettings = {"nonce":"8aa2da63ad"};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/heartbeat.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/vendor/lodash.min.js?ver=4.17.11'></script>
	<script type='text/javascript'>
	window.lodash = _.noConflict();
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/jquery/ui/sortable.min.js?ver=1.11.4'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var postBoxL10n = {"postBoxEmptyString":"Drag boxes here"};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-admin/js/postbox.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/backbone.min.js?ver=1.2.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var _wpUtilSettings = {"ajax":{"url":"\/wordpress\/wp-admin\/admin-ajax.php"}};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/wp-util.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/wp-backbone.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var _wpMediaModelsL10n = {"settings":{"ajaxurl":"\/wordpress\/wp-admin\/admin-ajax.php","post":{"id":0}}};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/media-models.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var pluploadL10n = {"queue_limit_exceeded":"You have attempted to queue too many files.","file_exceeds_size_limit":"%s exceeds the maximum upload size for this site.","zero_byte_file":"This file is empty. Please try another.","invalid_filetype":"Sorry, this file type is not permitted for security reasons.","not_an_image":"This file is not an image. Please try another.","image_memory_exceeded":"Memory exceeded. Please try another smaller file.","image_dimensions_exceeded":"This is larger than the maximum size. Please try another.","default_error":"An error occurred in the upload. Please try again later.","missing_upload_url":"There was a configuration error. Please contact the server administrator.","upload_limit_exceeded":"You may only upload 1 file.","http_error":"HTTP error.","upload_failed":"Upload failed.","big_upload_failed":"Please try uploading this file with the %1$sbrowser uploader%2$s.","big_upload_queued":"%s exceeds the maximum upload size for the multi-file uploader when used in your browser.","io_error":"IO error.","security_error":"Security error.","file_cancelled":"File canceled.","upload_stopped":"Upload stopped.","dismiss":"Dismiss","crunching":"Crunching\u2026","deleted":"moved to the trash.","error_uploading":"\u201c%s\u201d has failed to upload."};
	var _wpPluploadSettings = {"defaults":{"file_data_name":"async-upload","url":"\/wordpress\/wp-admin\/async-upload.php","filters":{"max_file_size":"2097152b","mime_types":[{"extensions":"jpg,jpeg,jpe,gif,png,bmp,tiff,tif,ico,asf,asx,wmv,wmx,wm,avi,divx,flv,mov,qt,mpeg,mpg,mpe,mp4,m4v,ogv,webm,mkv,3gp,3gpp,3g2,3gp2,txt,asc,c,cc,h,srt,csv,tsv,ics,rtx,css,htm,html,vtt,dfxp,mp3,m4a,m4b,aac,ra,ram,wav,ogg,oga,flac,mid,midi,wma,wax,mka,rtf,js,pdf,class,tar,zip,gz,gzip,rar,7z,psd,xcf,doc,pot,pps,ppt,wri,xla,xls,xlt,xlw,mdb,mpp,docx,docm,dotx,dotm,xlsx,xlsm,xlsb,xltx,xltm,xlam,pptx,pptm,ppsx,ppsm,potx,potm,ppam,sldx,sldm,onetoc,onetoc2,onetmp,onepkg,oxps,xps,odt,odp,ods,odg,odc,odb,odf,wp,wpd,key,numbers,pages"}]},"multipart_params":{"action":"upload-attachment","_wpnonce":"9a540c131a"}},"browser":{"mobile":false,"supported":true},"limitExceeded":false};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/plupload/wp-plupload.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var wpApiSettings = {"root":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/","nonce":"f0fce3b788","versionString":"wp\/v2\/"};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/api-request.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var _wpMediaViewsL10n = {"url":"URL","addMedia":"Add Media","search":"Search","select":"Select","cancel":"Cancel","update":"Update","replace":"Replace","remove":"Remove","back":"Back","selected":"%d selected","dragInfo":"Drag and drop to reorder media files.","uploadFilesTitle":"Upload Files","uploadImagesTitle":"Upload Images","mediaLibraryTitle":"Media Library","insertMediaTitle":"Add Media","createNewGallery":"Create a new gallery","createNewPlaylist":"Create a new playlist","createNewVideoPlaylist":"Create a new video playlist","returnToLibrary":"\u2190 Return to library","allMediaItems":"All media items","allDates":"All dates","noItemsFound":"No items found.","insertIntoPost":"Insert into post","unattached":"Unattached","mine":"Mine","trash":"Trash","uploadedToThisPost":"Uploaded to this post","warnDelete":"You are about to permanently delete this item from your site.\nThis action cannot be undone.\n 'Cancel' to stop, 'OK' to delete.","warnBulkDelete":"You are about to permanently delete these items from your site.\nThis action cannot be undone.\n 'Cancel' to stop, 'OK' to delete.","warnBulkTrash":"You are about to trash these items.\n  'Cancel' to stop, 'OK' to delete.","bulkSelect":"Bulk Select","cancelSelection":"Cancel Selection","trashSelected":"Trash Selected","untrashSelected":"Untrash Selected","deleteSelected":"Delete Selected","deletePermanently":"Delete Permanently","apply":"Apply","filterByDate":"Filter by date","filterByType":"Filter by type","searchMediaLabel":"Search Media","searchMediaPlaceholder":"Search media items...","noMedia":"No media files found.","attachmentDetails":"Attachment Details","insertFromUrlTitle":"Insert from URL","setFeaturedImageTitle":"Featured Image","setFeaturedImage":"Set featured image","createGalleryTitle":"Create Gallery","editGalleryTitle":"Edit Gallery","cancelGalleryTitle":"\u2190 Cancel Gallery","insertGallery":"Insert gallery","updateGallery":"Update gallery","addToGallery":"Add to gallery","addToGalleryTitle":"Add to Gallery","reverseOrder":"Reverse order","imageDetailsTitle":"Image Details","imageReplaceTitle":"Replace Image","imageDetailsCancel":"Cancel Edit","editImage":"Edit Image","chooseImage":"Choose Image","selectAndCrop":"Select and Crop","skipCropping":"Skip Cropping","cropImage":"Crop Image","cropYourImage":"Crop your image","cropping":"Cropping\u2026","suggestedDimensions":"Suggested image dimensions: %1$s by %2$s pixels.","cropError":"There has been an error cropping your image.","audioDetailsTitle":"Audio Details","audioReplaceTitle":"Replace Audio","audioAddSourceTitle":"Add Audio Source","audioDetailsCancel":"Cancel Edit","videoDetailsTitle":"Video Details","videoReplaceTitle":"Replace Video","videoAddSourceTitle":"Add Video Source","videoDetailsCancel":"Cancel Edit","videoSelectPosterImageTitle":"Select Poster Image","videoAddTrackTitle":"Add Subtitles","playlistDragInfo":"Drag and drop to reorder tracks.","createPlaylistTitle":"Create Audio Playlist","editPlaylistTitle":"Edit Audio Playlist","cancelPlaylistTitle":"\u2190 Cancel Audio Playlist","insertPlaylist":"Insert audio playlist","updatePlaylist":"Update audio playlist","addToPlaylist":"Add to audio playlist","addToPlaylistTitle":"Add to Audio Playlist","videoPlaylistDragInfo":"Drag and drop to reorder videos.","createVideoPlaylistTitle":"Create Video Playlist","editVideoPlaylistTitle":"Edit Video Playlist","cancelVideoPlaylistTitle":"\u2190 Cancel Video Playlist","insertVideoPlaylist":"Insert video playlist","updateVideoPlaylist":"Update video playlist","addToVideoPlaylist":"Add to video playlist","addToVideoPlaylistTitle":"Add to Video Playlist","settings":{"tabs":[],"tabUrl":"http:\/\/127.0.0.1\/wordpress\/wp-admin\/media-upload.php?chromeless=1","mimeTypes":{"image":"Images","audio":"Audio","video":"Video"},"captions":true,"nonce":{"sendToEditor":"388d4cf90f"},"post":{"id":17,"nonce":"368d226bd0","featuredImageId":-1},"defaultProps":{"link":"none","align":"","size":""},"attachmentCounts":{"audio":1,"video":1},"oEmbedProxyUrl":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/oembed\/1.0\/proxy","embedExts":["mp3","ogg","flac","m4a","wav","mp4","m4v","webm","ogv","flv"],"embedMimes":{"mp3":"audio\/mpeg","ogg":"audio\/ogg","flac":"audio\/flac","m4a":"audio\/mpeg","wav":"audio\/wav","mp4":"video\/mp4","m4v":"video\/mp4","webm":"video\/webm","ogv":"video\/ogg","flv":"video\/x-flv"},"contentWidth":640,"months":[],"mediaTrash":0}};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/media-views.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/wp-a11y.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/i18n.min.js?ver=3.1.0'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/url.min.js?ver=2.3.3'></script>
	<script type='text/javascript'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "default", { "locale_data": { "messages": { "": {} } } } );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/api-fetch.min.js?ver=2.2.7'></script>
	<script type='text/javascript'>
	wp.apiFetch.use( wp.apiFetch.createNonceMiddleware( "f0fce3b788" ) );
	/*wp.apiFetch.use( wp.apiFetch.createRootURLMiddleware( "http://127.0.0.1/wordpress/index.php/wp-json/" ) );*/
	wp.apiFetch.use( wp.apiFetch.createPreloadingMiddleware( {"\/":{"body":{"name":"Laravel","description":"Just another WordPress site","url":"http:\/\/127.0.0.1\/wordpress","home":"http:\/\/127.0.0.1\/wordpress","gmt_offset":"0","timezone_string":"","namespaces":["oembed\/1.0","wp\/v2"],"authentication":[],"routes":{"\/":{"namespace":"","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/"}},"\/oembed\/1.0":{"namespace":"oembed\/1.0","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"namespace":{"required":false,"default":"oembed\/1.0"},"context":{"required":false,"default":"view"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/oembed\/1.0"}},"\/oembed\/1.0\/embed":{"namespace":"oembed\/1.0","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"url":{"required":true},"format":{"required":false,"default":"json"},"maxwidth":{"required":false,"default":600}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/oembed\/1.0\/embed"}},"\/oembed\/1.0\/proxy":{"namespace":"oembed\/1.0","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"url":{"required":true,"description":"The URL of the resource for which to fetch oEmbed data.","type":"string"},"format":{"required":false,"default":"json","enum":["json","xml"],"description":"The oEmbed format to use.","type":"string"},"maxwidth":{"required":false,"default":600,"description":"The maximum width of the embed frame in pixels.","type":"integer"},"maxheight":{"required":false,"description":"The maximum height of the embed frame in pixels.","type":"integer"},"discover":{"required":false,"default":true,"description":"Whether to perform an oEmbed discovery request for non-whitelisted providers.","type":"boolean"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/oembed\/1.0\/proxy"}},"\/wp\/v2":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"namespace":{"required":false,"default":"wp\/v2"},"context":{"required":false,"default":"view"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2"}},"\/wp\/v2\/posts":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"after":{"required":false,"description":"Limit response to posts published after a given ISO8601 compliant date.","type":"string"},"author":{"required":false,"default":[],"description":"Limit result set to posts assigned to specific authors.","type":"array","items":{"type":"integer"}},"author_exclude":{"required":false,"default":[],"description":"Ensure result set excludes posts assigned to specific authors.","type":"array","items":{"type":"integer"}},"before":{"required":false,"description":"Limit response to posts published before a given ISO8601 compliant date.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"desc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"date","enum":["author","date","id","include","modified","parent","relevance","slug","include_slugs","title"],"description":"Sort collection by object attribute.","type":"string"},"slug":{"required":false,"description":"Limit result set to posts with one or more specific slugs.","type":"array","items":{"type":"string"}},"status":{"required":false,"default":"publish","description":"Limit result set to posts assigned one or more statuses.","type":"array","items":{"enum":["publish","future","draft","pending","private","trash","auto-draft","inherit","request-pending","request-confirmed","request-failed","request-completed","any"],"type":"string"}},"categories":{"required":false,"default":[],"description":"Limit result set to all items that have the specified term assigned in the categories taxonomy.","type":"array","items":{"type":"integer"}},"categories_exclude":{"required":false,"default":[],"description":"Limit result set to all items except those that have the specified term assigned in the categories taxonomy.","type":"array","items":{"type":"integer"}},"tags":{"required":false,"default":[],"description":"Limit result set to all items that have the specified term assigned in the tags taxonomy.","type":"array","items":{"type":"integer"}},"tags_exclude":{"required":false,"default":[],"description":"Limit result set to all items except those that have the specified term assigned in the tags taxonomy.","type":"array","items":{"type":"integer"}},"sticky":{"required":false,"description":"Limit result set to items that are sticky.","type":"boolean"}}},{"methods":["POST"],"args":{"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"password":{"required":false,"description":"A password to protect access to the content and excerpt.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"content":{"required":false,"description":"The content for the object.","type":"object"},"author":{"required":false,"description":"The ID for the author of the object.","type":"integer"},"excerpt":{"required":false,"description":"The excerpt for the object.","type":"object"},"featured_media":{"required":false,"description":"The ID of the featured media for the object.","type":"integer"},"comment_status":{"required":false,"enum":["open","closed"],"description":"Whether or not comments are open on the object.","type":"string"},"ping_status":{"required":false,"enum":["open","closed"],"description":"Whether or not the object can be pinged.","type":"string"},"format":{"required":false,"enum":["standard","aside","chat","gallery","link","image","quote","status","video","audio"],"description":"The format for the object.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"},"sticky":{"required":false,"description":"Whether or not the object should be treated as sticky.","type":"boolean"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"},"categories":{"required":false,"description":"The terms assigned to the object in the category taxonomy.","type":"array","items":{"type":"integer"}},"tags":{"required":false,"description":"The terms assigned to the object in the post_tag taxonomy.","type":"array","items":{"type":"integer"}}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts"}},"\/wp\/v2\/posts\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH","DELETE"],"endpoints":[{"methods":["GET"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"password":{"required":false,"description":"The password for the post if it is password protected.","type":"string"}}},{"methods":["POST","PUT","PATCH"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"password":{"required":false,"description":"A password to protect access to the content and excerpt.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"content":{"required":false,"description":"The content for the object.","type":"object"},"author":{"required":false,"description":"The ID for the author of the object.","type":"integer"},"excerpt":{"required":false,"description":"The excerpt for the object.","type":"object"},"featured_media":{"required":false,"description":"The ID of the featured media for the object.","type":"integer"},"comment_status":{"required":false,"enum":["open","closed"],"description":"Whether or not comments are open on the object.","type":"string"},"ping_status":{"required":false,"enum":["open","closed"],"description":"Whether or not the object can be pinged.","type":"string"},"format":{"required":false,"enum":["standard","aside","chat","gallery","link","image","quote","status","video","audio"],"description":"The format for the object.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"},"sticky":{"required":false,"description":"Whether or not the object should be treated as sticky.","type":"boolean"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"},"categories":{"required":false,"description":"The terms assigned to the object in the category taxonomy.","type":"array","items":{"type":"integer"}},"tags":{"required":false,"description":"The terms assigned to the object in the post_tag taxonomy.","type":"array","items":{"type":"integer"}}}},{"methods":["DELETE"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"force":{"required":false,"default":false,"description":"Whether to bypass trash and force deletion.","type":"boolean"}}}]},"\/wp\/v2\/posts\/(?P<parent>[\\d]+)\/revisions":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"desc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"date","enum":["date","id","include","relevance","slug","include_slugs","title"],"description":"Sort collection by object attribute.","type":"string"}}}]},"\/wp\/v2\/posts\/(?P<parent>[\\d]+)\/revisions\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","DELETE"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["DELETE"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"force":{"required":false,"default":false,"description":"Required to be true, as revisions do not support trashing.","type":"boolean"}}}]},"\/wp\/v2\/posts\/(?P<id>[\\d]+)\/autosaves":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["POST"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"password":{"required":false,"description":"A password to protect access to the content and excerpt.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"content":{"required":false,"description":"The content for the object.","type":"object"},"author":{"required":false,"description":"The ID for the author of the object.","type":"integer"},"excerpt":{"required":false,"description":"The excerpt for the object.","type":"object"},"featured_media":{"required":false,"description":"The ID of the featured media for the object.","type":"integer"},"comment_status":{"required":false,"enum":["open","closed"],"description":"Whether or not comments are open on the object.","type":"string"},"ping_status":{"required":false,"enum":["open","closed"],"description":"Whether or not the object can be pinged.","type":"string"},"format":{"required":false,"enum":["standard","aside","chat","gallery","link","image","quote","status","video","audio"],"description":"The format for the object.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"},"sticky":{"required":false,"description":"Whether or not the object should be treated as sticky.","type":"boolean"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"},"categories":{"required":false,"description":"The terms assigned to the object in the category taxonomy.","type":"array","items":{"type":"integer"}},"tags":{"required":false,"description":"The terms assigned to the object in the post_tag taxonomy.","type":"array","items":{"type":"integer"}}}}]},"\/wp\/v2\/posts\/(?P<parent>[\\d]+)\/autosaves\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"id":{"required":false,"description":"The ID for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}}]},"\/wp\/v2\/pages":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"after":{"required":false,"description":"Limit response to posts published after a given ISO8601 compliant date.","type":"string"},"author":{"required":false,"default":[],"description":"Limit result set to posts assigned to specific authors.","type":"array","items":{"type":"integer"}},"author_exclude":{"required":false,"default":[],"description":"Ensure result set excludes posts assigned to specific authors.","type":"array","items":{"type":"integer"}},"before":{"required":false,"description":"Limit response to posts published before a given ISO8601 compliant date.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"menu_order":{"required":false,"description":"Limit result set to posts with a specific menu_order value.","type":"integer"},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"desc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"date","enum":["author","date","id","include","modified","parent","relevance","slug","include_slugs","title","menu_order"],"description":"Sort collection by object attribute.","type":"string"},"parent":{"required":false,"default":[],"description":"Limit result set to items with particular parent IDs.","type":"array","items":{"type":"integer"}},"parent_exclude":{"required":false,"default":[],"description":"Limit result set to all items except those of a particular parent ID.","type":"array","items":{"type":"integer"}},"slug":{"required":false,"description":"Limit result set to posts with one or more specific slugs.","type":"array","items":{"type":"string"}},"status":{"required":false,"default":"publish","description":"Limit result set to posts assigned one or more statuses.","type":"array","items":{"enum":["publish","future","draft","pending","private","trash","auto-draft","inherit","request-pending","request-confirmed","request-failed","request-completed","any"],"type":"string"}}}},{"methods":["POST"],"args":{"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"password":{"required":false,"description":"A password to protect access to the content and excerpt.","type":"string"},"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"title":{"required":false,"description":"The title for the object.","type":"object"},"content":{"required":false,"description":"The content for the object.","type":"object"},"author":{"required":false,"description":"The ID for the author of the object.","type":"integer"},"excerpt":{"required":false,"description":"The excerpt for the object.","type":"object"},"featured_media":{"required":false,"description":"The ID of the featured media for the object.","type":"integer"},"comment_status":{"required":false,"enum":["open","closed"],"description":"Whether or not comments are open on the object.","type":"string"},"ping_status":{"required":false,"enum":["open","closed"],"description":"Whether or not the object can be pinged.","type":"string"},"menu_order":{"required":false,"description":"The order of the object in relation to other object of its type.","type":"integer"},"meta":{"required":false,"description":"Meta fields.","type":"object"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/pages"}},"\/wp\/v2\/pages\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH","DELETE"],"endpoints":[{"methods":["GET"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"password":{"required":false,"description":"The password for the post if it is password protected.","type":"string"}}},{"methods":["POST","PUT","PATCH"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"password":{"required":false,"description":"A password to protect access to the content and excerpt.","type":"string"},"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"title":{"required":false,"description":"The title for the object.","type":"object"},"content":{"required":false,"description":"The content for the object.","type":"object"},"author":{"required":false,"description":"The ID for the author of the object.","type":"integer"},"excerpt":{"required":false,"description":"The excerpt for the object.","type":"object"},"featured_media":{"required":false,"description":"The ID of the featured media for the object.","type":"integer"},"comment_status":{"required":false,"enum":["open","closed"],"description":"Whether or not comments are open on the object.","type":"string"},"ping_status":{"required":false,"enum":["open","closed"],"description":"Whether or not the object can be pinged.","type":"string"},"menu_order":{"required":false,"description":"The order of the object in relation to other object of its type.","type":"integer"},"meta":{"required":false,"description":"Meta fields.","type":"object"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"}}},{"methods":["DELETE"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"force":{"required":false,"default":false,"description":"Whether to bypass trash and force deletion.","type":"boolean"}}}]},"\/wp\/v2\/pages\/(?P<parent>[\\d]+)\/revisions":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"desc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"date","enum":["date","id","include","relevance","slug","include_slugs","title"],"description":"Sort collection by object attribute.","type":"string"}}}]},"\/wp\/v2\/pages\/(?P<parent>[\\d]+)\/revisions\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","DELETE"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["DELETE"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"force":{"required":false,"default":false,"description":"Required to be true, as revisions do not support trashing.","type":"boolean"}}}]},"\/wp\/v2\/pages\/(?P<id>[\\d]+)\/autosaves":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["POST"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"password":{"required":false,"description":"A password to protect access to the content and excerpt.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"content":{"required":false,"description":"The content for the object.","type":"object"},"author":{"required":false,"description":"The ID for the author of the object.","type":"integer"},"excerpt":{"required":false,"description":"The excerpt for the object.","type":"object"},"featured_media":{"required":false,"description":"The ID of the featured media for the object.","type":"integer"},"comment_status":{"required":false,"enum":["open","closed"],"description":"Whether or not comments are open on the object.","type":"string"},"ping_status":{"required":false,"enum":["open","closed"],"description":"Whether or not the object can be pinged.","type":"string"},"menu_order":{"required":false,"description":"The order of the object in relation to other object of its type.","type":"integer"},"meta":{"required":false,"description":"Meta fields.","type":"object"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"}}}]},"\/wp\/v2\/pages\/(?P<parent>[\\d]+)\/autosaves\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"id":{"required":false,"description":"The ID for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}}]},"\/wp\/v2\/media":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"after":{"required":false,"description":"Limit response to posts published after a given ISO8601 compliant date.","type":"string"},"author":{"required":false,"default":[],"description":"Limit result set to posts assigned to specific authors.","type":"array","items":{"type":"integer"}},"author_exclude":{"required":false,"default":[],"description":"Ensure result set excludes posts assigned to specific authors.","type":"array","items":{"type":"integer"}},"before":{"required":false,"description":"Limit response to posts published before a given ISO8601 compliant date.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"desc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"date","enum":["author","date","id","include","modified","parent","relevance","slug","include_slugs","title"],"description":"Sort collection by object attribute.","type":"string"},"parent":{"required":false,"default":[],"description":"Limit result set to items with particular parent IDs.","type":"array","items":{"type":"integer"}},"parent_exclude":{"required":false,"default":[],"description":"Limit result set to all items except those of a particular parent ID.","type":"array","items":{"type":"integer"}},"slug":{"required":false,"description":"Limit result set to posts with one or more specific slugs.","type":"array","items":{"type":"string"}},"status":{"required":false,"default":"inherit","description":"Limit result set to posts assigned one or more statuses.","type":"array","items":{"enum":["inherit","private","trash"],"type":"string"}},"media_type":{"required":false,"enum":["image","video","text","application","audio"],"description":"Limit result set to attachments of a particular media type.","type":"string"},"mime_type":{"required":false,"description":"Limit result set to attachments of a particular MIME type.","type":"string"}}},{"methods":["POST"],"args":{"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"author":{"required":false,"description":"The ID for the author of the object.","type":"integer"},"comment_status":{"required":false,"enum":["open","closed"],"description":"Whether or not comments are open on the object.","type":"string"},"ping_status":{"required":false,"enum":["open","closed"],"description":"Whether or not the object can be pinged.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"},"alt_text":{"required":false,"description":"Alternative text to display when attachment is not displayed.","type":"string"},"caption":{"required":false,"description":"The attachment caption.","type":"object"},"description":{"required":false,"description":"The attachment description.","type":"object"},"post":{"required":false,"description":"The ID for the associated post of the attachment.","type":"integer"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/media"}},"\/wp\/v2\/media\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH","DELETE"],"endpoints":[{"methods":["GET"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["POST","PUT","PATCH"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"author":{"required":false,"description":"The ID for the author of the object.","type":"integer"},"comment_status":{"required":false,"enum":["open","closed"],"description":"Whether or not comments are open on the object.","type":"string"},"ping_status":{"required":false,"enum":["open","closed"],"description":"Whether or not the object can be pinged.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"},"alt_text":{"required":false,"description":"Alternative text to display when attachment is not displayed.","type":"string"},"caption":{"required":false,"description":"The attachment caption.","type":"object"},"description":{"required":false,"description":"The attachment description.","type":"object"},"post":{"required":false,"description":"The ID for the associated post of the attachment.","type":"integer"}}},{"methods":["DELETE"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"force":{"required":false,"default":false,"description":"Whether to bypass trash and force deletion.","type":"boolean"}}}]},"\/wp\/v2\/blocks":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"after":{"required":false,"description":"Limit response to posts published after a given ISO8601 compliant date.","type":"string"},"before":{"required":false,"description":"Limit response to posts published before a given ISO8601 compliant date.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"desc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"date","enum":["author","date","id","include","modified","parent","relevance","slug","include_slugs","title"],"description":"Sort collection by object attribute.","type":"string"},"slug":{"required":false,"description":"Limit result set to posts with one or more specific slugs.","type":"array","items":{"type":"string"}},"status":{"required":false,"default":"publish","description":"Limit result set to posts assigned one or more statuses.","type":"array","items":{"enum":["publish","future","draft","pending","private","trash","auto-draft","inherit","request-pending","request-confirmed","request-failed","request-completed","any"],"type":"string"}}}},{"methods":["POST"],"args":{"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"password":{"required":false,"description":"A password to protect access to the content and excerpt.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"content":{"required":false,"description":"The content for the object.","type":"object"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/blocks"}},"\/wp\/v2\/blocks\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH","DELETE"],"endpoints":[{"methods":["GET"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"password":{"required":false,"description":"The password for the post if it is password protected.","type":"string"}}},{"methods":["POST","PUT","PATCH"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"password":{"required":false,"description":"A password to protect access to the content and excerpt.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"content":{"required":false,"description":"The content for the object.","type":"object"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"}}},{"methods":["DELETE"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"force":{"required":false,"default":false,"description":"Whether to bypass trash and force deletion.","type":"boolean"}}}]},"\/wp\/v2\/blocks\/(?P<id>[\\d]+)\/autosaves":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["POST"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"password":{"required":false,"description":"A password to protect access to the content and excerpt.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"content":{"required":false,"description":"The content for the object.","type":"object"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"}}}]},"\/wp\/v2\/blocks\/(?P<parent>[\\d]+)\/autosaves\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"id":{"required":false,"description":"The ID for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}}]},"\/wp\/v2\/types":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/types"}},"\/wp\/v2\/types\/(?P<type>[\\w-]+)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"type":{"required":false,"description":"An alphanumeric identifier for the post type.","type":"string"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}}]},"\/wp\/v2\/statuses":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/statuses"}},"\/wp\/v2\/statuses\/(?P<status>[\\w-]+)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"status":{"required":false,"description":"An alphanumeric identifier for the status.","type":"string"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}}]},"\/wp\/v2\/taxonomies":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"type":{"required":false,"description":"Limit results to taxonomies associated with a specific post type.","type":"string"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/taxonomies"}},"\/wp\/v2\/taxonomies\/(?P<taxonomy>[\\w-]+)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"taxonomy":{"required":false,"description":"An alphanumeric identifier for the taxonomy.","type":"string"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}}]},"\/wp\/v2\/categories":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"order":{"required":false,"default":"asc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"name","enum":["id","include","name","slug","include_slugs","term_group","description","count"],"description":"Sort collection by term attribute.","type":"string"},"hide_empty":{"required":false,"default":false,"description":"Whether to hide terms not assigned to any posts.","type":"boolean"},"parent":{"required":false,"description":"Limit result set to terms assigned to a specific parent.","type":"integer"},"post":{"required":false,"description":"Limit result set to terms assigned to a specific post.","type":"integer"},"slug":{"required":false,"description":"Limit result set to terms with one or more specific slugs.","type":"array","items":{"type":"string"}}}},{"methods":["POST"],"args":{"description":{"required":false,"description":"HTML description of the term.","type":"string"},"name":{"required":true,"description":"HTML title for the term.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the term unique to its type.","type":"string"},"parent":{"required":false,"description":"The parent term ID.","type":"integer"},"meta":{"required":false,"description":"Meta fields.","type":"object"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/categories"}},"\/wp\/v2\/categories\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH","DELETE"],"endpoints":[{"methods":["GET"],"args":{"id":{"required":false,"description":"Unique identifier for the term.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["POST","PUT","PATCH"],"args":{"id":{"required":false,"description":"Unique identifier for the term.","type":"integer"},"description":{"required":false,"description":"HTML description of the term.","type":"string"},"name":{"required":false,"description":"HTML title for the term.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the term unique to its type.","type":"string"},"parent":{"required":false,"description":"The parent term ID.","type":"integer"},"meta":{"required":false,"description":"Meta fields.","type":"object"}}},{"methods":["DELETE"],"args":{"id":{"required":false,"description":"Unique identifier for the term.","type":"integer"},"force":{"required":false,"default":false,"description":"Required to be true, as terms do not support trashing.","type":"boolean"}}}]},"\/wp\/v2\/tags":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"asc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"name","enum":["id","include","name","slug","include_slugs","term_group","description","count"],"description":"Sort collection by term attribute.","type":"string"},"hide_empty":{"required":false,"default":false,"description":"Whether to hide terms not assigned to any posts.","type":"boolean"},"post":{"required":false,"description":"Limit result set to terms assigned to a specific post.","type":"integer"},"slug":{"required":false,"description":"Limit result set to terms with one or more specific slugs.","type":"array","items":{"type":"string"}}}},{"methods":["POST"],"args":{"description":{"required":false,"description":"HTML description of the term.","type":"string"},"name":{"required":true,"description":"HTML title for the term.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the term unique to its type.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/tags"}},"\/wp\/v2\/tags\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH","DELETE"],"endpoints":[{"methods":["GET"],"args":{"id":{"required":false,"description":"Unique identifier for the term.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["POST","PUT","PATCH"],"args":{"id":{"required":false,"description":"Unique identifier for the term.","type":"integer"},"description":{"required":false,"description":"HTML description of the term.","type":"string"},"name":{"required":false,"description":"HTML title for the term.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the term unique to its type.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"}}},{"methods":["DELETE"],"args":{"id":{"required":false,"description":"Unique identifier for the term.","type":"integer"},"force":{"required":false,"default":false,"description":"Required to be true, as terms do not support trashing.","type":"boolean"}}}]},"\/wp\/v2\/users":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"asc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"name","enum":["id","include","name","registered_date","slug","include_slugs","email","url"],"description":"Sort collection by object attribute.","type":"string"},"slug":{"required":false,"description":"Limit result set to users with one or more specific slugs.","type":"array","items":{"type":"string"}},"roles":{"required":false,"description":"Limit result set to users matching at least one specific role provided. Accepts csv list or single role.","type":"array","items":{"type":"string"}},"who":{"required":false,"enum":["authors"],"description":"Limit result set to users who are considered authors.","type":"string"}}},{"methods":["POST"],"args":{"username":{"required":true,"description":"Login name for the user.","type":"string"},"name":{"required":false,"description":"Display name for the user.","type":"string"},"first_name":{"required":false,"description":"First name for the user.","type":"string"},"last_name":{"required":false,"description":"Last name for the user.","type":"string"},"email":{"required":true,"description":"The email address for the user.","type":"string"},"url":{"required":false,"description":"URL of the user.","type":"string"},"description":{"required":false,"description":"Description of the user.","type":"string"},"locale":{"required":false,"enum":["","en_US"],"description":"Locale for the user.","type":"string"},"nickname":{"required":false,"description":"The nickname for the user.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the user.","type":"string"},"roles":{"required":false,"description":"Roles assigned to the user.","type":"array","items":{"type":"string"}},"password":{"required":true,"description":"Password for the user (never included).","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/users"}},"\/wp\/v2\/users\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH","DELETE"],"endpoints":[{"methods":["GET"],"args":{"id":{"required":false,"description":"Unique identifier for the user.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["POST","PUT","PATCH"],"args":{"id":{"required":false,"description":"Unique identifier for the user.","type":"integer"},"username":{"required":false,"description":"Login name for the user.","type":"string"},"name":{"required":false,"description":"Display name for the user.","type":"string"},"first_name":{"required":false,"description":"First name for the user.","type":"string"},"last_name":{"required":false,"description":"Last name for the user.","type":"string"},"email":{"required":false,"description":"The email address for the user.","type":"string"},"url":{"required":false,"description":"URL of the user.","type":"string"},"description":{"required":false,"description":"Description of the user.","type":"string"},"locale":{"required":false,"enum":["","en_US"],"description":"Locale for the user.","type":"string"},"nickname":{"required":false,"description":"The nickname for the user.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the user.","type":"string"},"roles":{"required":false,"description":"Roles assigned to the user.","type":"array","items":{"type":"string"}},"password":{"required":false,"description":"Password for the user (never included).","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"}}},{"methods":["DELETE"],"args":{"id":{"required":false,"description":"Unique identifier for the user.","type":"integer"},"force":{"required":false,"default":false,"description":"Required to be true, as users do not support trashing.","type":"boolean"},"reassign":{"required":true,"description":"Reassign the deleted user's posts and links to this user ID.","type":"integer"}}}]},"\/wp\/v2\/users\/me":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH","DELETE"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"}}},{"methods":["POST","PUT","PATCH"],"args":{"username":{"required":false,"description":"Login name for the user.","type":"string"},"name":{"required":false,"description":"Display name for the user.","type":"string"},"first_name":{"required":false,"description":"First name for the user.","type":"string"},"last_name":{"required":false,"description":"Last name for the user.","type":"string"},"email":{"required":false,"description":"The email address for the user.","type":"string"},"url":{"required":false,"description":"URL of the user.","type":"string"},"description":{"required":false,"description":"Description of the user.","type":"string"},"locale":{"required":false,"enum":["","en_US"],"description":"Locale for the user.","type":"string"},"nickname":{"required":false,"description":"The nickname for the user.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the user.","type":"string"},"roles":{"required":false,"description":"Roles assigned to the user.","type":"array","items":{"type":"string"}},"password":{"required":false,"description":"Password for the user (never included).","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"}}},{"methods":["DELETE"],"args":{"force":{"required":false,"default":false,"description":"Required to be true, as users do not support trashing.","type":"boolean"},"reassign":{"required":true,"description":"Reassign the deleted user's posts and links to this user ID.","type":"integer"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/users\/me"}},"\/wp\/v2\/comments":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"after":{"required":false,"description":"Limit response to comments published after a given ISO8601 compliant date.","type":"string"},"author":{"required":false,"description":"Limit result set to comments assigned to specific user IDs. Requires authorization.","type":"array","items":{"type":"integer"}},"author_exclude":{"required":false,"description":"Ensure result set excludes comments assigned to specific user IDs. Requires authorization.","type":"array","items":{"type":"integer"}},"author_email":{"required":false,"description":"Limit result set to that from a specific author email. Requires authorization.","type":"string"},"before":{"required":false,"description":"Limit response to comments published before a given ISO8601 compliant date.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"desc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"date_gmt","enum":["date","date_gmt","id","include","post","parent","type"],"description":"Sort collection by object attribute.","type":"string"},"parent":{"required":false,"default":[],"description":"Limit result set to comments of specific parent IDs.","type":"array","items":{"type":"integer"}},"parent_exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific parent IDs.","type":"array","items":{"type":"integer"}},"post":{"required":false,"default":[],"description":"Limit result set to comments assigned to specific post IDs.","type":"array","items":{"type":"integer"}},"status":{"required":false,"default":"approve","description":"Limit result set to comments assigned a specific status. Requires authorization.","type":"string"},"type":{"required":false,"default":"comment","description":"Limit result set to comments assigned a specific type. Requires authorization.","type":"string"},"password":{"required":false,"description":"The password for the post if it is password protected.","type":"string"}}},{"methods":["POST"],"args":{"author":{"required":false,"description":"The ID of the user object, if author was a user.","type":"integer"},"author_email":{"required":false,"description":"Email address for the object author.","type":"string"},"author_ip":{"required":false,"description":"IP address for the object author.","type":"string"},"author_name":{"required":false,"description":"Display name for the object author.","type":"string"},"author_url":{"required":false,"description":"URL for the object author.","type":"string"},"author_user_agent":{"required":false,"description":"User agent for the object author.","type":"string"},"content":{"required":false,"description":"The content for the object.","type":"object"},"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"parent":{"required":false,"default":0,"description":"The ID for the parent of the object.","type":"integer"},"post":{"required":false,"default":0,"description":"The ID of the associated post object.","type":"integer"},"status":{"required":false,"description":"State of the object.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/comments"}},"\/wp\/v2\/comments\/(?P<id>[\\d]+)":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH","DELETE"],"endpoints":[{"methods":["GET"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"password":{"required":false,"description":"The password for the parent post of the comment (if the post is password protected).","type":"string"}}},{"methods":["POST","PUT","PATCH"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"author":{"required":false,"description":"The ID of the user object, if author was a user.","type":"integer"},"author_email":{"required":false,"description":"Email address for the object author.","type":"string"},"author_ip":{"required":false,"description":"IP address for the object author.","type":"string"},"author_name":{"required":false,"description":"Display name for the object author.","type":"string"},"author_url":{"required":false,"description":"URL for the object author.","type":"string"},"author_user_agent":{"required":false,"description":"User agent for the object author.","type":"string"},"content":{"required":false,"description":"The content for the object.","type":"object"},"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"parent":{"required":false,"description":"The ID for the parent of the object.","type":"integer"},"post":{"required":false,"description":"The ID of the associated post object.","type":"integer"},"status":{"required":false,"description":"State of the object.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"}}},{"methods":["DELETE"],"args":{"id":{"required":false,"description":"Unique identifier for the object.","type":"integer"},"force":{"required":false,"default":false,"description":"Whether to bypass trash and force deletion.","type":"boolean"},"password":{"required":false,"description":"The password for the parent post of the comment (if the post is password protected).","type":"string"}}}]},"\/wp\/v2\/search":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"type":{"required":false,"default":"post","enum":["post"],"description":"Limit results to items of an object type.","type":"string"},"subtype":{"required":false,"default":"any","description":"Limit results to items of one or more object subtypes.","type":"array","items":{"enum":["post","page","any"],"type":"string"}}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/search"}},"\/wp\/v2\/block-renderer\/(?P<name>core\/block)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"name":{"required":false,"description":"Unique registered name for the block.","type":"string"},"context":{"required":false,"default":"view","enum":["edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"attributes":{"required":false,"default":[],"description":"Attributes for core\/block block","type":"object"},"post_id":{"required":false,"description":"ID of the post context.","type":"integer"}}}]},"\/wp\/v2\/block-renderer\/(?P<name>core\/latest-comments)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"name":{"required":false,"description":"Unique registered name for the block.","type":"string"},"context":{"required":false,"default":"view","enum":["edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"attributes":{"required":false,"default":[],"description":"Attributes for core\/latest-comments block","type":"object"},"post_id":{"required":false,"description":"ID of the post context.","type":"integer"}}}]},"\/wp\/v2\/block-renderer\/(?P<name>core\/archives)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"name":{"required":false,"description":"Unique registered name for the block.","type":"string"},"context":{"required":false,"default":"view","enum":["edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"attributes":{"required":false,"default":[],"description":"Attributes for core\/archives block","type":"object"},"post_id":{"required":false,"description":"ID of the post context.","type":"integer"}}}]},"\/wp\/v2\/block-renderer\/(?P<name>core\/categories)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"name":{"required":false,"description":"Unique registered name for the block.","type":"string"},"context":{"required":false,"default":"view","enum":["edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"attributes":{"required":false,"default":[],"description":"Attributes for core\/categories block","type":"object"},"post_id":{"required":false,"description":"ID of the post context.","type":"integer"}}}]},"\/wp\/v2\/block-renderer\/(?P<name>core\/latest-posts)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"name":{"required":false,"description":"Unique registered name for the block.","type":"string"},"context":{"required":false,"default":"view","enum":["edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"attributes":{"required":false,"default":[],"description":"Attributes for core\/latest-posts block","type":"object"},"post_id":{"required":false,"description":"ID of the post context.","type":"integer"}}}]},"\/wp\/v2\/block-renderer\/(?P<name>core\/shortcode)":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"name":{"required":false,"description":"Unique registered name for the block.","type":"string"},"context":{"required":false,"default":"view","enum":["edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"attributes":{"required":false,"default":[],"description":"Attributes for core\/shortcode block","type":"object"},"post_id":{"required":false,"description":"ID of the post context.","type":"integer"}}}]},"\/wp\/v2\/settings":{"namespace":"wp\/v2","methods":["GET","POST","PUT","PATCH"],"endpoints":[{"methods":["GET"],"args":[]},{"methods":["POST","PUT","PATCH"],"args":{"title":{"required":false,"description":"Site title.","type":"string"},"description":{"required":false,"description":"Site tagline.","type":"string"},"url":{"required":false,"description":"Site URL.","type":"string"},"email":{"required":false,"description":"This address is used for admin purposes, like new user notification.","type":"string"},"timezone":{"required":false,"description":"A city in the same timezone as you.","type":"string"},"date_format":{"required":false,"description":"A date format for all date strings.","type":"string"},"time_format":{"required":false,"description":"A time format for all time strings.","type":"string"},"start_of_week":{"required":false,"description":"A day number of the week that the week should start on.","type":"integer"},"language":{"required":false,"description":"WordPress locale code.","type":"string"},"use_smilies":{"required":false,"description":"Convert emoticons like :-) and :-P to graphics on display.","type":"boolean"},"default_category":{"required":false,"description":"Default post category.","type":"integer"},"default_post_format":{"required":false,"description":"Default post format.","type":"string"},"posts_per_page":{"required":false,"description":"Blog pages show at most.","type":"integer"},"default_ping_status":{"required":false,"enum":["open","closed"],"description":"Allow link notifications from other blogs (pingbacks and trackbacks) on new articles.","type":"string"},"default_comment_status":{"required":false,"enum":["open","closed"],"description":"Allow people to post comments on new articles.","type":"string"}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/settings"}},"\/wp\/v2\/themes":{"namespace":"wp\/v2","methods":["GET"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"status":{"required":true,"description":"Limit result set to themes assigned one or more statuses.","type":"array","items":{"enum":["active"],"type":"string"}}}}],"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/themes"}}},"_links":{"help":[{"href":"http:\/\/v2.wp-api.org\/"}]}},"headers":[]},"\/wp\/v2\/types?context=edit":{"body":{"post":{"capabilities":{"edit_post":"edit_post","read_post":"read_post","delete_post":"delete_post","edit_posts":"edit_posts","edit_others_posts":"edit_others_posts","publish_posts":"publish_posts","read_private_posts":"read_private_posts","read":"read","delete_posts":"delete_posts","delete_private_posts":"delete_private_posts","delete_published_posts":"delete_published_posts","delete_others_posts":"delete_others_posts","edit_private_posts":"edit_private_posts","edit_published_posts":"edit_published_posts","create_posts":"edit_posts"},"description":"","hierarchical":false,"viewable":true,"labels":{"name":"Posts","singular_name":"Post","add_new":"Add New","add_new_item":"Add New Post","edit_item":"Edit Post","new_item":"New Post","view_item":"View Post","view_items":"View Posts","search_items":"Search Posts","not_found":"No posts found.","not_found_in_trash":"No posts found in Trash.","parent_item_colon":null,"all_items":"All Posts","archives":"Post Archives","attributes":"Post Attributes","insert_into_item":"Insert into post","uploaded_to_this_item":"Uploaded to this post","featured_image":"Featured Image","set_featured_image":"Set featured image","remove_featured_image":"Remove featured image","use_featured_image":"Use as featured image","filter_items_list":"Filter posts list","items_list_navigation":"Posts list navigation","items_list":"Posts list","item_published":"Post published.","item_published_privately":"Post published privately.","item_reverted_to_draft":"Post reverted to draft.","item_scheduled":"Post scheduled.","item_updated":"Post updated.","menu_name":"Posts","name_admin_bar":"Post"},"name":"Posts","slug":"post","supports":{"title":true,"editor":true,"author":true,"thumbnail":true,"excerpt":true,"trackbacks":true,"custom-fields":true,"comments":true,"revisions":true,"post-formats":true},"taxonomies":["category","post_tag"],"rest_base":"posts","_links":{"collection":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/types"}],"wp:items":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts"}],"curies":[{"name":"wp","href":"https:\/\/api.w.org\/{rel}","templated":true}]}},"page":{"capabilities":{"edit_post":"edit_page","read_post":"read_page","delete_post":"delete_page","edit_posts":"edit_pages","edit_others_posts":"edit_others_pages","publish_posts":"publish_pages","read_private_posts":"read_private_pages","read":"read","delete_posts":"delete_pages","delete_private_posts":"delete_private_pages","delete_published_posts":"delete_published_pages","delete_others_posts":"delete_others_pages","edit_private_posts":"edit_private_pages","edit_published_posts":"edit_published_pages","create_posts":"edit_pages"},"description":"","hierarchical":true,"viewable":true,"labels":{"name":"Pages","singular_name":"Page","add_new":"Add New","add_new_item":"Add New Page","edit_item":"Edit Page","new_item":"New Page","view_item":"View Page","view_items":"View Pages","search_items":"Search Pages","not_found":"No pages found.","not_found_in_trash":"No pages found in Trash.","parent_item_colon":"Parent Page:","all_items":"All Pages","archives":"Page Archives","attributes":"Page Attributes","insert_into_item":"Insert into page","uploaded_to_this_item":"Uploaded to this page","featured_image":"Featured Image","set_featured_image":"Set featured image","remove_featured_image":"Remove featured image","use_featured_image":"Use as featured image","filter_items_list":"Filter pages list","items_list_navigation":"Pages list navigation","items_list":"Pages list","item_published":"Page published.","item_published_privately":"Page published privately.","item_reverted_to_draft":"Page reverted to draft.","item_scheduled":"Page scheduled.","item_updated":"Page updated.","menu_name":"Pages","name_admin_bar":"Page"},"name":"Pages","slug":"page","supports":{"title":true,"editor":true,"author":true,"thumbnail":true,"page-attributes":true,"custom-fields":true,"comments":true,"revisions":true},"taxonomies":[],"rest_base":"pages","_links":{"collection":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/types"}],"wp:items":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/pages"}],"curies":[{"name":"wp","href":"https:\/\/api.w.org\/{rel}","templated":true}]}},"attachment":{"capabilities":{"edit_post":"edit_post","read_post":"read_post","delete_post":"delete_post","edit_posts":"edit_posts","edit_others_posts":"edit_others_posts","publish_posts":"publish_posts","read_private_posts":"read_private_posts","read":"read","delete_posts":"delete_posts","delete_private_posts":"delete_private_posts","delete_published_posts":"delete_published_posts","delete_others_posts":"delete_others_posts","edit_private_posts":"edit_private_posts","edit_published_posts":"edit_published_posts","create_posts":"upload_files"},"description":"","hierarchical":false,"viewable":true,"labels":{"name":"Media","singular_name":"Media","add_new":"Add New","add_new_item":"Add New Post","edit_item":"Edit Media","new_item":"New Post","view_item":"View Attachment Page","view_items":"View Posts","search_items":"Search Posts","not_found":"No posts found.","not_found_in_trash":"No posts found in Trash.","parent_item_colon":null,"all_items":"Media","archives":"Media","attributes":"Attachment Attributes","insert_into_item":"Insert into post","uploaded_to_this_item":"Uploaded to this post","featured_image":"Featured Image","set_featured_image":"Set featured image","remove_featured_image":"Remove featured image","use_featured_image":"Use as featured image","filter_items_list":"Filter posts list","items_list_navigation":"Posts list navigation","items_list":"Posts list","item_published":"Post published.","item_published_privately":"Post published privately.","item_reverted_to_draft":"Post reverted to draft.","item_scheduled":"Post scheduled.","item_updated":"Post updated.","menu_name":"Media","name_admin_bar":"Media"},"name":"Media","slug":"attachment","supports":{"title":true,"author":true,"comments":true},"taxonomies":[],"rest_base":"media","_links":{"collection":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/types"}],"wp:items":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/media"}],"curies":[{"name":"wp","href":"https:\/\/api.w.org\/{rel}","templated":true}]}},"wp_block":{"capabilities":{"edit_post":"edit_block","read_post":"read_block","delete_post":"delete_block","edit_posts":"edit_blocks","edit_others_posts":"edit_others_posts","publish_posts":"publish_blocks","read_private_posts":"read_private_blocks","read":"edit_posts","delete_posts":"delete_blocks","delete_private_posts":"delete_private_blocks","delete_published_posts":"delete_published_posts","delete_others_posts":"delete_others_posts","edit_private_posts":"edit_private_blocks","edit_published_posts":"edit_published_posts","create_posts":"publish_posts"},"description":"","hierarchical":false,"viewable":false,"labels":{"name":"Blocks","singular_name":"Block","add_new":"Add New","add_new_item":"Add New Block","edit_item":"Edit Block","new_item":"New Block","view_item":"View Block","view_items":"View Posts","search_items":"Search Blocks","not_found":"No blocks found.","not_found_in_trash":"No blocks found in Trash.","parent_item_colon":null,"all_items":"All Blocks","archives":"All Blocks","attributes":"Post Attributes","insert_into_item":"Insert into post","uploaded_to_this_item":"Uploaded to this post","featured_image":"Featured Image","set_featured_image":"Set featured image","remove_featured_image":"Remove featured image","use_featured_image":"Use as featured image","filter_items_list":"Filter blocks list","items_list_navigation":"Blocks list navigation","items_list":"Blocks list","item_published":"Block published.","item_published_privately":"Block published privately.","item_reverted_to_draft":"Block reverted to draft.","item_scheduled":"Block scheduled.","item_updated":"Block updated.","menu_name":"Blocks","name_admin_bar":"Block"},"name":"Blocks","slug":"wp_block","supports":{"title":true,"editor":true},"taxonomies":[],"rest_base":"blocks","_links":{"collection":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/types"}],"wp:items":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/blocks"}],"curies":[{"name":"wp","href":"https:\/\/api.w.org\/{rel}","templated":true}]}}},"headers":[]},"\/wp\/v2\/taxonomies?per_page=-1&context=edit":{"body":{"category":{"name":"Categories","slug":"category","capabilities":{"manage_terms":"manage_categories","edit_terms":"edit_categories","delete_terms":"delete_categories","assign_terms":"assign_categories"},"description":"","labels":{"name":"Categories","singular_name":"Category","search_items":"Search Categories","popular_items":null,"all_items":"All Categories","parent_item":"Parent Category","parent_item_colon":"Parent Category:","edit_item":"Edit Category","view_item":"View Category","update_item":"Update Category","add_new_item":"Add New Category","new_item_name":"New Category Name","separate_items_with_commas":null,"add_or_remove_items":null,"choose_from_most_used":null,"not_found":"No categories found.","no_terms":"No categories","items_list_navigation":"Categories list navigation","items_list":"Categories list","most_used":"Most Used","back_to_items":"&larr; Back to Categories","menu_name":"Categories","name_admin_bar":"category"},"types":["post"],"show_cloud":true,"hierarchical":true,"rest_base":"categories","visibility":{"public":true,"publicly_queryable":true,"show_admin_column":true,"show_in_nav_menus":true,"show_in_quick_edit":true,"show_ui":true},"_links":{"collection":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/taxonomies"}],"wp:items":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/categories"}],"curies":[{"name":"wp","href":"https:\/\/api.w.org\/{rel}","templated":true}]}},"post_tag":{"name":"Tags","slug":"post_tag","capabilities":{"manage_terms":"manage_post_tags","edit_terms":"edit_post_tags","delete_terms":"delete_post_tags","assign_terms":"assign_post_tags"},"description":"","labels":{"name":"Tags","singular_name":"Tag","search_items":"Search Tags","popular_items":"Popular Tags","all_items":"All Tags","parent_item":null,"parent_item_colon":null,"edit_item":"Edit Tag","view_item":"View Tag","update_item":"Update Tag","add_new_item":"Add New Tag","new_item_name":"New Tag Name","separate_items_with_commas":"Separate tags with commas","add_or_remove_items":"Add or remove tags","choose_from_most_used":"Choose from the most used tags","not_found":"No tags found.","no_terms":"No tags","items_list_navigation":"Tags list navigation","items_list":"Tags list","most_used":"Most Used","back_to_items":"&larr; Back to Tags","menu_name":"Tags","name_admin_bar":"post_tag"},"types":["post"],"show_cloud":true,"hierarchical":false,"rest_base":"tags","visibility":{"public":true,"publicly_queryable":true,"show_admin_column":true,"show_in_nav_menus":true,"show_in_quick_edit":true,"show_ui":true},"_links":{"collection":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/taxonomies"}],"wp:items":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/tags"}],"curies":[{"name":"wp","href":"https:\/\/api.w.org\/{rel}","templated":true}]}}},"headers":[]},"\/wp\/v2\/themes?status=active":{"body":[{"theme_supports":{"formats":["standard"],"post-thumbnails":true,"responsive-embeds":true}}],"headers":{"X-WP-Total":1,"X-WP-TotalPages":1}},"\/wp\/v2\/posts\/17?context=edit":{"body":{"id":17,"date":"2019-02-25T17:55:24","date_gmt":"2019-02-25T17:55:24","guid":{"rendered":"http:\/\/127.0.0.1\/wordpress\/?p=17","raw":"http:\/\/127.0.0.1\/wordpress\/?p=17"},"modified":"2019-02-25T17:55:24","modified_gmt":"2019-02-25T17:55:24","password":"","slug":"","status":"auto-draft","type":"post","link":"http:\/\/127.0.0.1\/wordpress\/?p=17","title":{"raw":"Auto Draft","rendered":"Auto Draft"},"content":{"raw":"","rendered":"","protected":false,"block_version":0},"excerpt":{"raw":"","rendered":"","protected":false},"author":1,"featured_media":0,"comment_status":"open","ping_status":"open","sticky":false,"template":"","format":"standard","meta":[],"categories":[],"tags":[],"permalink_template":"http:\/\/127.0.0.1\/wordpress\/?p=17","generated_slug":"auto-draft","_links":{"self":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17"}],"collection":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts"}],"about":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/types\/post"}],"author":[{"embeddable":true,"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/users\/1"}],"replies":[{"embeddable":true,"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/comments?post=17"}],"version-history":[{"count":0,"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17\/revisions"}],"wp:attachment":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/media?parent=17"}],"wp:term":[{"taxonomy":"category","embeddable":true,"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/categories?post=17"},{"taxonomy":"post_tag","embeddable":true,"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/tags?post=17"}],"wp:action-publish":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17"}],"wp:action-unfiltered-html":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17"}],"wp:action-sticky":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17"}],"wp:action-assign-author":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17"}],"wp:action-create-categories":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17"}],"wp:action-assign-categories":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17"}],"wp:action-create-tags":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17"}],"wp:action-assign-tags":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts\/17"}],"curies":[{"name":"wp","href":"https:\/\/api.w.org\/{rel}","templated":true}]}},"headers":{"Link":"<http:\/\/127.0.0.1\/wordpress\/?p=17>; rel=\"alternate\"; type=text\/html"}},"\/wp\/v2\/types\/post?context=edit":{"body":{"capabilities":{"edit_post":"edit_post","read_post":"read_post","delete_post":"delete_post","edit_posts":"edit_posts","edit_others_posts":"edit_others_posts","publish_posts":"publish_posts","read_private_posts":"read_private_posts","read":"read","delete_posts":"delete_posts","delete_private_posts":"delete_private_posts","delete_published_posts":"delete_published_posts","delete_others_posts":"delete_others_posts","edit_private_posts":"edit_private_posts","edit_published_posts":"edit_published_posts","create_posts":"edit_posts"},"description":"","hierarchical":false,"viewable":true,"labels":{"name":"Posts","singular_name":"Post","add_new":"Add New","add_new_item":"Add New Post","edit_item":"Edit Post","new_item":"New Post","view_item":"View Post","view_items":"View Posts","search_items":"Search Posts","not_found":"No posts found.","not_found_in_trash":"No posts found in Trash.","parent_item_colon":null,"all_items":"All Posts","archives":"Post Archives","attributes":"Post Attributes","insert_into_item":"Insert into post","uploaded_to_this_item":"Uploaded to this post","featured_image":"Featured Image","set_featured_image":"Set featured image","remove_featured_image":"Remove featured image","use_featured_image":"Use as featured image","filter_items_list":"Filter posts list","items_list_navigation":"Posts list navigation","items_list":"Posts list","item_published":"Post published.","item_published_privately":"Post published privately.","item_reverted_to_draft":"Post reverted to draft.","item_scheduled":"Post scheduled.","item_updated":"Post updated.","menu_name":"Posts","name_admin_bar":"Post"},"name":"Posts","slug":"post","supports":{"title":true,"editor":true,"author":true,"thumbnail":true,"excerpt":true,"trackbacks":true,"custom-fields":true,"comments":true,"revisions":true,"post-formats":true},"taxonomies":["category","post_tag"],"rest_base":"posts","_links":{"collection":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/types"}],"wp:items":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/posts"}],"curies":[{"name":"wp","href":"https:\/\/api.w.org\/{rel}","templated":true}]}},"headers":[]},"\/wp\/v2\/users\/me?post_type=post&context=edit":{"body":{"id":1,"username":"laravel","name":"laravel","first_name":"","last_name":"","email":"hitesh@email.com","url":"","description":"","link":"http:\/\/127.0.0.1\/wordpress\/index.php\/author\/laravel\/","locale":"en_US","nickname":"laravel","slug":"laravel","roles":["administrator"],"registered_date":"2019-02-04T13:07:12+00:00","capabilities":{"switch_themes":true,"edit_themes":true,"activate_plugins":true,"edit_plugins":true,"edit_users":true,"edit_files":true,"manage_options":true,"moderate_comments":true,"manage_categories":true,"manage_links":true,"upload_files":true,"import":true,"unfiltered_html":true,"edit_posts":true,"edit_others_posts":true,"edit_published_posts":true,"publish_posts":true,"edit_pages":true,"read":true,"level_10":true,"level_9":true,"level_8":true,"level_7":true,"level_6":true,"level_5":true,"level_4":true,"level_3":true,"level_2":true,"level_1":true,"level_0":true,"edit_others_pages":true,"edit_published_pages":true,"publish_pages":true,"delete_pages":true,"delete_others_pages":true,"delete_published_pages":true,"delete_posts":true,"delete_others_posts":true,"delete_published_posts":true,"delete_private_posts":true,"edit_private_posts":true,"read_private_posts":true,"delete_private_pages":true,"edit_private_pages":true,"read_private_pages":true,"delete_users":true,"create_users":true,"unfiltered_upload":true,"edit_dashboard":true,"update_plugins":true,"delete_plugins":true,"install_plugins":true,"update_themes":true,"install_themes":true,"update_core":true,"list_users":true,"remove_users":true,"promote_users":true,"edit_theme_options":true,"delete_themes":true,"export":true,"administrator":true},"extra_capabilities":{"administrator":true},"avatar_urls":{"24":"http:\/\/0.gravatar.com\/avatar\/6aa11c3f4736d0c1f710646dee728e3e?s=24&d=mm&r=g","48":"http:\/\/0.gravatar.com\/avatar\/6aa11c3f4736d0c1f710646dee728e3e?s=48&d=mm&r=g","96":"http:\/\/0.gravatar.com\/avatar\/6aa11c3f4736d0c1f710646dee728e3e?s=96&d=mm&r=g"},"meta":[],"_links":{"self":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/users\/1"}],"collection":[{"href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/users"}]}},"headers":[]},"OPTIONS":{"\/wp\/v2\/media":{"body":{"namespace":"wp\/v2","methods":["GET","POST"],"endpoints":[{"methods":["GET"],"args":{"context":{"required":false,"default":"view","enum":["view","embed","edit"],"description":"Scope under which the request is made; determines fields present in response.","type":"string"},"page":{"required":false,"default":1,"description":"Current page of the collection.","type":"integer"},"per_page":{"required":false,"default":10,"description":"Maximum number of items to be returned in result set.","type":"integer"},"search":{"required":false,"description":"Limit results to those matching a string.","type":"string"},"after":{"required":false,"description":"Limit response to posts published after a given ISO8601 compliant date.","type":"string"},"author":{"required":false,"default":[],"description":"Limit result set to posts assigned to specific authors.","type":"array","items":{"type":"integer"}},"author_exclude":{"required":false,"default":[],"description":"Ensure result set excludes posts assigned to specific authors.","type":"array","items":{"type":"integer"}},"before":{"required":false,"description":"Limit response to posts published before a given ISO8601 compliant date.","type":"string"},"exclude":{"required":false,"default":[],"description":"Ensure result set excludes specific IDs.","type":"array","items":{"type":"integer"}},"include":{"required":false,"default":[],"description":"Limit result set to specific IDs.","type":"array","items":{"type":"integer"}},"offset":{"required":false,"description":"Offset the result set by a specific number of items.","type":"integer"},"order":{"required":false,"default":"desc","enum":["asc","desc"],"description":"Order sort attribute ascending or descending.","type":"string"},"orderby":{"required":false,"default":"date","enum":["author","date","id","include","modified","parent","relevance","slug","include_slugs","title"],"description":"Sort collection by object attribute.","type":"string"},"parent":{"required":false,"default":[],"description":"Limit result set to items with particular parent IDs.","type":"array","items":{"type":"integer"}},"parent_exclude":{"required":false,"default":[],"description":"Limit result set to all items except those of a particular parent ID.","type":"array","items":{"type":"integer"}},"slug":{"required":false,"description":"Limit result set to posts with one or more specific slugs.","type":"array","items":{"type":"string"}},"status":{"required":false,"default":"inherit","description":"Limit result set to posts assigned one or more statuses.","type":"array","items":{"enum":["inherit","private","trash"],"type":"string"}},"media_type":{"required":false,"enum":["image","video","text","application","audio"],"description":"Limit result set to attachments of a particular media type.","type":"string"},"mime_type":{"required":false,"description":"Limit result set to attachments of a particular MIME type.","type":"string"}}},{"methods":["POST"],"args":{"date":{"required":false,"description":"The date the object was published, in the site's timezone.","type":"string"},"date_gmt":{"required":false,"description":"The date the object was published, as GMT.","type":"string"},"slug":{"required":false,"description":"An alphanumeric identifier for the object unique to its type.","type":"string"},"status":{"required":false,"enum":["publish","future","draft","pending","private"],"description":"A named status for the object.","type":"string"},"title":{"required":false,"description":"The title for the object.","type":"object"},"author":{"required":false,"description":"The ID for the author of the object.","type":"integer"},"comment_status":{"required":false,"enum":["open","closed"],"description":"Whether or not comments are open on the object.","type":"string"},"ping_status":{"required":false,"enum":["open","closed"],"description":"Whether or not the object can be pinged.","type":"string"},"meta":{"required":false,"description":"Meta fields.","type":"object"},"template":{"required":false,"description":"The theme file to use to display the object.","type":"string"},"alt_text":{"required":false,"description":"Alternative text to display when attachment is not displayed.","type":"string"},"caption":{"required":false,"description":"The attachment caption.","type":"object"},"description":{"required":false,"description":"The attachment description.","type":"object"},"post":{"required":false,"description":"The ID for the associated post of the attachment.","type":"integer"}}}],"schema":{"$schema":"http:\/\/json-schema.org\/draft-04\/schema#","title":"attachment","type":"object","properties":{"date":{"description":"The date the object was published, in the site's timezone.","type":"string","format":"date-time","context":["view","edit","embed"]},"date_gmt":{"description":"The date the object was published, as GMT.","type":"string","format":"date-time","context":["view","edit"]},"guid":{"description":"The globally unique identifier for the object.","type":"object","context":["view","edit"],"readonly":true,"properties":{"raw":{"description":"GUID for the object, as it exists in the database.","type":"string","context":["edit"],"readonly":true},"rendered":{"description":"GUID for the object, transformed for display.","type":"string","context":["view","edit"],"readonly":true}}},"id":{"description":"Unique identifier for the object.","type":"integer","context":["view","edit","embed"],"readonly":true},"link":{"description":"URL to the object.","type":"string","format":"uri","context":["view","edit","embed"],"readonly":true},"modified":{"description":"The date the object was last modified, in the site's timezone.","type":"string","format":"date-time","context":["view","edit"],"readonly":true},"modified_gmt":{"description":"The date the object was last modified, as GMT.","type":"string","format":"date-time","context":["view","edit"],"readonly":true},"slug":{"description":"An alphanumeric identifier for the object unique to its type.","type":"string","context":["view","edit","embed"]},"status":{"description":"A named status for the object.","type":"string","enum":["publish","future","draft","pending","private"],"context":["view","edit"]},"type":{"description":"Type of Post for the object.","type":"string","context":["view","edit","embed"],"readonly":true},"permalink_template":{"description":"Permalink template for the object.","type":"string","context":["edit"],"readonly":true},"generated_slug":{"description":"Slug automatically generated from the object title.","type":"string","context":["edit"],"readonly":true},"title":{"description":"The title for the object.","type":"object","context":["view","edit","embed"],"properties":{"raw":{"description":"Title for the object, as it exists in the database.","type":"string","context":["edit"]},"rendered":{"description":"HTML title for the object, transformed for display.","type":"string","context":["view","edit","embed"],"readonly":true}}},"author":{"description":"The ID for the author of the object.","type":"integer","context":["view","edit","embed"]},"comment_status":{"description":"Whether or not comments are open on the object.","type":"string","enum":["open","closed"],"context":["view","edit"]},"ping_status":{"description":"Whether or not the object can be pinged.","type":"string","enum":["open","closed"],"context":["view","edit"]},"meta":{"description":"Meta fields.","type":"object","context":["view","edit"],"properties":[]},"template":{"description":"The theme file to use to display the object.","type":"string","context":["view","edit"]},"alt_text":{"description":"Alternative text to display when attachment is not displayed.","type":"string","context":["view","edit","embed"]},"caption":{"description":"The attachment caption.","type":"object","context":["view","edit","embed"],"properties":{"raw":{"description":"Caption for the attachment, as it exists in the database.","type":"string","context":["edit"]},"rendered":{"description":"HTML caption for the attachment, transformed for display.","type":"string","context":["view","edit","embed"],"readonly":true}}},"description":{"description":"The attachment description.","type":"object","context":["view","edit"],"properties":{"raw":{"description":"Description for the object, as it exists in the database.","type":"string","context":["edit"]},"rendered":{"description":"HTML description for the object, transformed for display.","type":"string","context":["view","edit"],"readonly":true}}},"media_type":{"description":"Attachment type.","type":"string","enum":["image","file"],"context":["view","edit","embed"],"readonly":true},"mime_type":{"description":"The attachment MIME type.","type":"string","context":["view","edit","embed"],"readonly":true},"media_details":{"description":"Details about the media file, specific to its type.","type":"object","context":["view","edit","embed"],"readonly":true},"post":{"description":"The ID for the associated post of the attachment.","type":"integer","context":["view","edit"]},"source_url":{"description":"URL to the original attachment file.","type":"string","format":"uri","context":["view","edit","embed"],"readonly":true}},"links":[{"rel":"https:\/\/api.w.org\/action-unfiltered-html","title":"The current user can post unfiltered HTML markup and JavaScript.","href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/media\/{id}","targetSchema":{"type":"object","properties":{"content":{"raw":{"type":"string"}}}}},{"rel":"https:\/\/api.w.org\/action-assign-author","title":"The current user can change the author on this post.","href":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/media\/{id}","targetSchema":{"type":"object","properties":{"author":{"type":"integer"}}}}]},"_links":{"self":"http:\/\/127.0.0.1\/wordpress\/index.php\/wp-json\/wp\/v2\/media"}},"headers":{"Allow":"GET, POST"}}}} ) );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-admin/js/editor.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	window.wp.oldEditor = window.wp.editor;
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/autop.min.js?ver=2.0.2'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/blob.min.js?ver=2.1.0'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/block-serialization-default-parser.min.js?ver=2.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/vendor/react.min.js?ver=16.6.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/vendor/react-dom.min.js?ver=16.6.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/escape-html.min.js?ver=1.0.1'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/element.min.js?ver=2.1.8'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/is-shallow-equal.min.js?ver=1.1.4'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/compose.min.js?ver=3.0.0'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/redux-routine.min.js?ver=3.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/data.min.js?ver=4.2.0'></script>
	<script type='text/javascript'>
	( function() {
		var userId = 1;
		var storageKey = "WP_DATA_USER_" + userId;
		wp.data
			.use( wp.data.plugins.persistence, { storageKey: storageKey } )
			.use( wp.data.plugins.controls );
	} )()
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/dom.min.js?ver=2.0.8'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/html-entities.min.js?ver=2.0.4'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/shortcode.min.js?ver=2.0.2'></script>
	<script type='text/javascript'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "default", { "locale_data": { "messages": { "": {} } } } );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/blocks.min.js?ver=6.0.5'></script>
	<script type='text/javascript'>
	wp.blocks.setCategories( [{"slug":"common","title":"Common Blocks","icon":null},{"slug":"formatting","title":"Formatting","icon":null},{"slug":"layout","title":"Layout Elements","icon":null},{"slug":"widgets","title":"Widgets","icon":null},{"slug":"embed","title":"Embeds","icon":null},{"slug":"reusable","title":"Reusable Blocks","icon":null}] );
	wp.blocks.unstable__bootstrapServerSideBlockDefinitions({"core\/block":{"attributes":{"ref":{"type":"number"}}},"core\/latest-comments":{"attributes":{"className":{"type":"string"},"commentsToShow":{"type":"number","default":5,"minimum":1,"maximum":100},"displayAvatar":{"type":"boolean","default":true},"displayDate":{"type":"boolean","default":true},"displayExcerpt":{"type":"boolean","default":true},"align":{"type":"string","enum":["center","left","right","wide","full",""]}}},"core\/archives":{"attributes":{"align":{"type":"string"},"className":{"type":"string"},"displayAsDropdown":{"type":"boolean","default":false},"showPostCounts":{"type":"boolean","default":false}}},"core\/latest-posts":{"attributes":{"categories":{"type":"string"},"className":{"type":"string"},"postsToShow":{"type":"number","default":5},"displayPostDate":{"type":"boolean","default":false},"postLayout":{"type":"string","default":"list"},"columns":{"type":"number","default":3},"align":{"type":"string"},"order":{"type":"string","default":"desc"},"orderBy":{"type":"string","default":"date"}}}});
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/vendor/moment.min.js?ver=2.22.2'></script>
	<script type='text/javascript'>
	moment.locale( 'en_US', {"months":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthsShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"weekdays":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"weekdaysShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"week":{"dow":1},"longDateFormat":{"LT":"g:i a","LTS":null,"L":null,"LL":"F j, Y","LLL":"F j, Y g:i a","LLLL":null}} );
	</script>
	<script type='text/javascript'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "default", { "locale_data": { "messages": { "": {} } } } );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/keycodes.min.js?ver=2.0.5'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/rich-text.min.js?ver=3.0.4'></script>
	<script type='text/javascript'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "default", { "locale_data": { "messages": { "": {} } } } );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/components.min.js?ver=7.0.5'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/core-data.min.js?ver=2.0.16'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/date.min.js?ver=3.0.1'></script>
	<script type='text/javascript'>
	wp.date.setSettings( {"l10n":{"locale":"en_US","months":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthsShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"weekdays":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"weekdaysShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"meridiem":{"am":"am","pm":"pm","AM":"AM","PM":"PM"},"relative":{"future":"%s from now","past":"%s ago"}},"formats":{"time":"g:i a","date":"F j, Y","datetime":"F j, Y g:i a","datetimeAbbreviated":"M j, Y g:i a"},"timezone":{"offset":"0","string":""}} );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/deprecated.min.js?ver=2.0.4'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/notices.min.js?ver=1.1.2'></script>
	<script type='text/javascript'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "default", { "locale_data": { "messages": { "": {} } } } );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/nux.min.js?ver=3.0.6'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/token-list.min.js?ver=1.1.0'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/viewport.min.js?ver=2.1.0'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/wordcount.min.js?ver=2.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var _wpMetaBoxUrl = "http:\/\/127.0.0.1\/wordpress\/wp-admin\/post.php?post=17&action=edit&meta-box-loader=1&_wpnonce=17df3364e4";
	/* ]]> */
	</script>
	<script type='text/javascript'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "default", { "locale_data": { "messages": { "": {} } } } );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/editor.min.js?ver=9.0.7'></script>
	<script type='text/javascript'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "default", { "locale_data": { "messages": { "": {} } } } );
	</script>
	<script type='text/javascript'>
	window.wpEditorL10n = {
			tinymce: {
				baseURL: "http:\/\/127.0.0.1\/wordpress\/wp-includes\/js\/tinymce",
				suffix: ".min",
				settings: {plugins:"charmap,colorpicker,hr,lists,media,paste,tabfocus,textcolor,fullscreen,wordpress,wpautoresize,wpeditimage,wpemoji,wpgallery,wplink,wpdialogs,wptextpattern,wpview",toolbar1:"formatselect,bold,italic,bullist,numlist,blockquote,alignleft,aligncenter,alignright,link,unlink,wp_more,spellchecker,wp_add_media,wp_adv",toolbar2:"strikethrough,hr,forecolor,pastetext,removeformat,charmap,outdent,indent,undo,redo,wp_help",toolbar3:"",toolbar4:"",external_plugins:[],classic_block_editor:true},
			}
		}
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/block-library.min.js?ver=2.2.12'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/dom-ready.min.js?ver=2.0.2'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/plugins.min.js?ver=2.0.10'></script>
	<script type='text/javascript'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "default", { "locale_data": { "messages": { "": {} } } } );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/edit-post.min.js?ver=3.1.7'></script>
	<script type='text/javascript'>
	( function() {
		window._wpLoadBlockEditor = new Promise( function( resolve ) {
			wp.domReady( function() {
				resolve( wp.editPost.initializeEditor( 'editor', "post", 17, {"alignWide":true,"availableTemplates":[],"allowedBlockTypes":true,"disableCustomColors":false,"disableCustomFontSizes":false,"disablePostFormats":true,"titlePlaceholder":"Add title","bodyPlaceholder":"Start writing or type \/ to choose a block","isRTL":false,"autosaveInterval":10,"maxUploadFileSize":2097152,"allowedMimeTypes":{"jpg|jpeg|jpe":"image\/jpeg","gif":"image\/gif","png":"image\/png","bmp":"image\/bmp","tiff|tif":"image\/tiff","ico":"image\/x-icon","asf|asx":"video\/x-ms-asf","wmv":"video\/x-ms-wmv","wmx":"video\/x-ms-wmx","wm":"video\/x-ms-wm","avi":"video\/avi","divx":"video\/divx","flv":"video\/x-flv","mov|qt":"video\/quicktime","mpeg|mpg|mpe":"video\/mpeg","mp4|m4v":"video\/mp4","ogv":"video\/ogg","webm":"video\/webm","mkv":"video\/x-matroska","3gp|3gpp":"video\/3gpp","3g2|3gp2":"video\/3gpp2","txt|asc|c|cc|h|srt":"text\/plain","csv":"text\/csv","tsv":"text\/tab-separated-values","ics":"text\/calendar","rtx":"text\/richtext","css":"text\/css","htm|html":"text\/html","vtt":"text\/vtt","dfxp":"application\/ttaf+xml","mp3|m4a|m4b":"audio\/mpeg","aac":"audio\/aac","ra|ram":"audio\/x-realaudio","wav":"audio\/wav","ogg|oga":"audio\/ogg","flac":"audio\/flac","mid|midi":"audio\/midi","wma":"audio\/x-ms-wma","wax":"audio\/x-ms-wax","mka":"audio\/x-matroska","rtf":"application\/rtf","js":"application\/javascript","pdf":"application\/pdf","class":"application\/java","tar":"application\/x-tar","zip":"application\/zip","gz|gzip":"application\/x-gzip","rar":"application\/rar","7z":"application\/x-7z-compressed","psd":"application\/octet-stream","xcf":"application\/octet-stream","doc":"application\/msword","pot|pps|ppt":"application\/vnd.ms-powerpoint","wri":"application\/vnd.ms-write","xla|xls|xlt|xlw":"application\/vnd.ms-excel","mdb":"application\/vnd.ms-access","mpp":"application\/vnd.ms-project","docx":"application\/vnd.openxmlformats-officedocument.wordprocessingml.document","docm":"application\/vnd.ms-word.document.macroEnabled.12","dotx":"application\/vnd.openxmlformats-officedocument.wordprocessingml.template","dotm":"application\/vnd.ms-word.template.macroEnabled.12","xlsx":"application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet","xlsm":"application\/vnd.ms-excel.sheet.macroEnabled.12","xlsb":"application\/vnd.ms-excel.sheet.binary.macroEnabled.12","xltx":"application\/vnd.openxmlformats-officedocument.spreadsheetml.template","xltm":"application\/vnd.ms-excel.template.macroEnabled.12","xlam":"application\/vnd.ms-excel.addin.macroEnabled.12","pptx":"application\/vnd.openxmlformats-officedocument.presentationml.presentation","pptm":"application\/vnd.ms-powerpoint.presentation.macroEnabled.12","ppsx":"application\/vnd.openxmlformats-officedocument.presentationml.slideshow","ppsm":"application\/vnd.ms-powerpoint.slideshow.macroEnabled.12","potx":"application\/vnd.openxmlformats-officedocument.presentationml.template","potm":"application\/vnd.ms-powerpoint.template.macroEnabled.12","ppam":"application\/vnd.ms-powerpoint.addin.macroEnabled.12","sldx":"application\/vnd.openxmlformats-officedocument.presentationml.slide","sldm":"application\/vnd.ms-powerpoint.slide.macroEnabled.12","onetoc|onetoc2|onetmp|onepkg":"application\/onenote","oxps":"application\/oxps","xps":"application\/vnd.ms-xpsdocument","odt":"application\/vnd.oasis.opendocument.text","odp":"application\/vnd.oasis.opendocument.presentation","ods":"application\/vnd.oasis.opendocument.spreadsheet","odg":"application\/vnd.oasis.opendocument.graphics","odc":"application\/vnd.oasis.opendocument.chart","odb":"application\/vnd.oasis.opendocument.database","odf":"application\/vnd.oasis.opendocument.formula","wp|wpd":"application\/wordperfect","key":"application\/vnd.apple.keynote","numbers":"application\/vnd.apple.numbers","pages":"application\/vnd.apple.pages"},"styles":[{"css":"\/**\n * Colors\n *\/\n\/**\n * Breakpoints & Media Queries\n *\/\n\/**\n * Often re-used variables\n *\/\n\/**\n * Breakpoint mixins\n *\/\n\/**\n * Long content fade mixin\n *\n * Creates a fading overlay to signify that the content is longer\n * than the space allows.\n *\/\n\/**\n * Button states and focus styles\n *\/\n\/**\n * Applies editor left position to the selector passed as argument\n *\/\n\/**\n * Applies editor right position to the selector passed as argument\n *\/\n\/**\n * Styles that are reused verbatim in a few places\n *\/\nbody {\n  font-family: \"Noto Serif\", serif;\n  font-size: 16px;\n  line-height: 1.8;\n  color: #191e23; }\n\np {\n  font-size: 16px;\n  line-height: 1.8; }\n\nul,\nol {\n  margin: 0;\n  padding: 0; }\n\nul {\n  list-style-type: disc; }\n\nol {\n  list-style-type: decimal; }\n\nul ul,\nol ul {\n  list-style-type: circle; }\n\n.mce-content-body {\n  line-height: 1.8; }\n"},{"css":"body { font-family: 'Noto Serif' }"},{"css":"@charset \"UTF-8\";\n\/*!\nTwenty Nineteen Editor Styles\n*\/\n\/** === Includes === *\/\n\/*\n * Chrome renders extra-wide &nbsp; characters for the Hoefler Text font.\n * This results in a jumping cursor when typing in both the Classic and block\n * editors. The following font-face override fixes the issue by manually inserting\n * a custom font that includes just a Hoefler Text space replacement for that\n * character instead.\n *\/\n@font-face {\n  font-family: 'NonBreakingSpaceOverride';\n  src: url(data:application\/font-woff2;charset=utf-8;base64,d09GMgABAAAAAAMoAA0AAAAACDQAAALTAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0ZGVE0cGh4GYACCahEICjx3CywAATYCJANUBCAFhiEHgWwbXQfILgpsY+rQRRARwyAs6uL7pxzYhxEE+32b3aeHmifR6tklkS9hiZA0ewkqGRJE+H7\/+6378ASViK\/PGeavqJyOzsceKi1s3BCiQsiOdn1r\/RBgIJYEgCUhbm\/8\/8\/h4saPssnTNkkiWUBrTRtjmQSajw3Ui3pZ3LYDPD+XG2C3JA\/yKAS8\/rU5eNfuGqRf4eNNgV4YAlIIgxglEkWe6FYpq10+wi3g+\/nUgvgPFczNrz\/RsTgVm\/zfbPuHZlsuQECxuyqBcQwKFBjFgKO8AqP4bAN9tFJtnM9xPcbNjeXS\/x1wY\/xU52f5W\/X1+9cnH4YwKIaoRRAkUkj\/YlAAeF\/624foiIDBgBmgQBeGAyhBljUPZUm\/l2dTvmpqcBDUOHdbPZWd8JsBAsGr4w8\/EDn82\/bUPx4eh0YNrQTBuHO2FjQEAGBwK0DeI37DpQVqdERS4gZBhpeUhWCfLFz7J99aEBgsJCHvUGAdAPp4IADDCAPCEFMGpMZ9AQpTfQtQGhLbGVBZFV8BaqNyP68oTZgHNj3M8kBPfXTTC9t90UuzYhy9ciH0grVlOcqyCytisvbsERsEYztiznR0WCrmTksJwbSNK6fd1Rvr25I9oLvctUoEbNOmXJbqgYgPXEHJ82IUsrCnpkxh23F1rfZ2zcRnJYoXtauB3VTFkFXQg3uoZYD5qE0kdjDtoDoF1h2bulGmev5HbYhbrjtohQSRI4aNOkffIcT+d3v6atpaYh3JvPoQsztCcqvaBkppDSPcQ3bw3KaCBo1f5CJWTZEgW3LjLofYg51MaVezrx8xZitYbQ9KYeoRaqQdVLwSEfrKXLK1otCWOKNdR\/YwYAfon5Yk8O2MJfSD10dPGA5PIJJQMkah0ugMJiv6x4Dm7LEa8xnrRGGGLAg4sAlbsA07sAt76DOsXKO3hIjtIlpnnFrt1qW4kh6NhS83P\/6HB\/fl1SMAAA==) format(\"woff2\"), url(data:application\/font-woff;charset=utf-8;base64,d09GRgABAAAAAAUQAA0AAAAACDQAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABGRlRNAAAE9AAAABwAAAAchf5yU0dERUYAAATYAAAAHAAAAB4AJwAbT1MvMgAAAaAAAABJAAAAYJAcgU5jbWFwAAACIAAAAF4AAAFqUUxBZ2dhc3AAAATQAAAACAAAAAgAAAAQZ2x5ZgAAApAAAAAyAAAAPL0n8y9oZWFkAAABMAAAADAAAAA2Fi93Z2hoZWEAAAFgAAAAHQAAACQOSgWaaG10eAAAAewAAAAzAAAAVC7TAQBsb2NhAAACgAAAABAAAAAsAOQBAm1heHAAAAGAAAAAHQAAACAAWQALbmFtZQAAAsQAAAF6AAADIYvD\/Adwb3N0AAAEQAAAAI4AAADsapk2o3jaY2BkYGAA4ov5mwzj+W2+MnCzXwCKMNzgCBSB0LfbQDQ7AxuI4mBgAlEAFKQIRHjaY2BkYGD3+NvCwMDBAALsDAyMDKhAFAA3+wH3AAAAeNpjYGRgYBBl4GBgYgABEMnIABJzAPMZAAVmAGUAAAB42mNgZlJhnMDAysDCKsKygYGBYRqEZtrDYMT4D8gHSmEHjgUFOQwODAqqf9g9\/rYwMLB7MNUAhRlBcsxBrMlASoGBEQAj8QtyAAAAeNrjYGBkAAGmWQwMjO8gmBnIZ2NA0ExAzNjAAFYJVn0ASBsD6VAIDZb7AtELAgANIgb9AHjaY2BgYGaAYBkGRgYQSAHyGMF8FgYPIM3HwMHAxMDGoMCwQIFLQV8hXvXP\/\/9AcRCfAcb\/\/\/h\/ygPW+w\/vb7olBjUHCTCyMcAFGZmABBO6AogThgZgIUsXAEDcEzcAAHjaY2BgECMCyoEgACZaAed42mNgYmRgYGBnYGNgYAZSDJqMgorCgoqCjECRXwwNrCAKSP5mAAFGBiRgyAAAi\/YFBQAAeNqtkc1OwkAUhU\/5M25cEhcsZick0AwlBJq6MWwgJkAgYV\/KAA2lJeUn+hY+gktXvpKv4dLTMqKycGHsTZNv7px7z50ZAFd4hYHjdw1Ls4EiHjVncIFnzVnc4F1zDkWjrzmPW+NNcwGlzIRKI3fJlUyrEjZQxb3mDH2fNGfRx4vmHKqG0JzHg6E0F9DOlFBGBxUI1GEzLNT4S0aLuTtsGAEUuYcQHkyg3KmIum1bNUvKlrjbbAIleqHHnS4iSudpQcySMYtdFiXlAxzSbAwfMxK6kZoHKhbjjespMTioOPZnzI+4ucCeTVyKMVKLfeAS6vSWaTinuZwzyy\/Dc7vaed+6KaV0kukdPUk6yOcctZPvvxxqksq2lEW8RvHjMEO2FCl\/zy6p3NEm0R9OFSafJdldc4QVeyaaObMBO0\/5cCaa6d9Ggyubxire+lEojscdjoWUR1xGOy8KD8mG2ZLO2l2paDc3A39qmU2z2W5YNv5+u79e6QfGJY\/hAAB42m3NywrCMBQE0DupWp\/1AYI7\/6DEaLQu66Mrd35BKUWKJSlFv1+rue4cGM7shgR981qSon+ZNwUJ8iDgoYU2OvDRRQ99DDDECAHGmGCKmf80hZSx\/Kik\/LliFbtmN6xmt+yOjdg9GztV4tROnRwX\/Bsaaw51nt4Lc7tWaZYHp\/MlzKx51LZs5htNri+2AAAAAQAB\/\/8AD3jaY2BkYGDgAWIxIGZiYARCESBmAfMYAAR6AEMAAAABAAAAANXtRbgAAAAA2AhRFAAAAADYCNuG) format(\"woff\");\n}\n\n\/* If we add the border using a regular CSS border, it won't look good on non-retina devices,\n * since its edges can look jagged due to lack of antialiasing. In this case, we are several\n * layers of box-shadow to add the border visually, which will render the border smoother. *\/\n\/* Fallback for non-latin fonts *\/\n\/* Calculates maximum width for post content *\/\n\/* Nested sub-menu padding: 10 levels deep *\/\n\/** === Editor Frame === *\/\nbody .wp-block[data-align=\"full\"] {\n  width: 100%;\n}\n\n@media only screen and (min-width: 600px) {\n  body .wp-block[data-align=\"full\"] {\n    width: calc( 100% + 90px);\n    max-width: calc( 100% + 90px);\n  }\n}\n\n@media only screen and (min-width: 768px) {\n  body .editor-writing-flow {\n    max-width: 80%;\n    margin: 0 10%;\n  }\n  body .editor-post-title__block,\n  body .editor-default-block-appender,\n  body .editor-block-list__block {\n    margin-left: 0;\n    margin-right: 0;\n  }\n  body .wp-block[data-align=\"wide\"] {\n    width: 100%;\n  }\n  body .wp-block[data-align=\"full\"] {\n    position: relative;\n    left: calc( -12.5% - 14px);\n    width: calc( 125% + 116px);\n    max-width: calc( 125% + 115px);\n  }\n  body .wp-block[data-align=\"right\"] {\n    max-width: 125%;\n  }\n}\n\n\/** === Content Width === *\/\n.wp-block {\n  width: calc(100vw - (2 * 1rem));\n  max-width: 100%;\n}\n\n@media only screen and (min-width: 768px) {\n  .wp-block {\n    width: calc(8 * (100vw \/ 12));\n  }\n}\n\n@media only screen and (min-width: 1168px) {\n  .wp-block {\n    width: calc(6 * (100vw \/ 12 ));\n  }\n}\n\n.wp-block .wp-block {\n  width: 100%;\n}\n\n\/** === Base Typography === *\/\nbody {\n  font-size: 22px;\n  font-family: \"NonBreakingSpaceOverride\", \"Hoefler Text\", \"Baskerville Old Face\", Garamond, \"Times New Roman\", serif;\n  line-height: 1.8;\n  color: #111;\n}\n\np {\n  font-size: 22px;\n}\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-weight: 700;\n}\n\nh1 {\n  font-size: 2.25em;\n}\n\nh1:before {\n  background: #767676;\n  content: \"\\020\";\n  display: block;\n  height: 2px;\n  margin: 1rem 0;\n  width: 1em;\n}\n\n@media only screen and (min-width: 768px) {\n  h1 {\n    font-size: 2.8125em;\n  }\n}\n\nh2 {\n  font-size: 1.6875em;\n}\n\nh2:before {\n  background: #767676;\n  content: \"\\020\";\n  display: block;\n  height: 2px;\n  margin: 1rem 0;\n  width: 1em;\n}\n\n@media only screen and (min-width: 768px) {\n  h2 {\n    font-size: 2.25em;\n  }\n}\n\nh3 {\n  font-size: 1.6875em;\n}\n\nh4 {\n  font-size: 1.125em;\n}\n\nh5 {\n  font-size: 0.88889em;\n}\n\nh6 {\n  font-size: 0.71111em;\n}\n\na {\n  transition: color 110ms ease-in-out;\n  color: #0073aa;\n}\n\na:hover, a:active {\n  color: #005177;\n  outline: 0;\n  text-decoration: none;\n}\n\na:focus {\n  outline: 0;\n  text-decoration: underline;\n}\n\n.has-primary-background-color,\n.has-secondary-background-color,\n.has-dark-gray-background-color,\n.has-light-gray-background-color {\n  color: #fff;\n}\n\n.has-primary-background-color p,\n.has-primary-background-color h1,\n.has-primary-background-color h2,\n.has-primary-background-color h3,\n.has-primary-background-color h4,\n.has-primary-background-color h5,\n.has-primary-background-color h6,\n.has-primary-background-color a,\n.has-secondary-background-color p,\n.has-secondary-background-color h1,\n.has-secondary-background-color h2,\n.has-secondary-background-color h3,\n.has-secondary-background-color h4,\n.has-secondary-background-color h5,\n.has-secondary-background-color h6,\n.has-secondary-background-color a,\n.has-dark-gray-background-color p,\n.has-dark-gray-background-color h1,\n.has-dark-gray-background-color h2,\n.has-dark-gray-background-color h3,\n.has-dark-gray-background-color h4,\n.has-dark-gray-background-color h5,\n.has-dark-gray-background-color h6,\n.has-dark-gray-background-color a,\n.has-light-gray-background-color p,\n.has-light-gray-background-color h1,\n.has-light-gray-background-color h2,\n.has-light-gray-background-color h3,\n.has-light-gray-background-color h4,\n.has-light-gray-background-color h5,\n.has-light-gray-background-color h6,\n.has-light-gray-background-color a {\n  color: #fff;\n}\n\n.has-white-background-color {\n  color: #111;\n}\n\n.has-white-background-color p,\n.has-white-background-color h1,\n.has-white-background-color h2,\n.has-white-background-color h3,\n.has-white-background-color h4,\n.has-white-background-color h5,\n.has-white-background-color h6,\n.has-white-background-color a {\n  color: #111;\n}\n\nfigcaption,\n.gallery-caption {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-size: 0.71111em;\n  line-height: 1.6;\n  color: #767676;\n}\n\n\/** === Post Title === *\/\n.editor-post-title__block:before {\n  background: #767676;\n  content: \"\\020\";\n  display: block;\n  height: 2px;\n  margin: 1rem 0;\n  width: 1em;\n}\n\n.editor-post-title__block:before {\n  width: 2.8125em;\n  margin-top: 0;\n  margin-bottom: 0;\n  margin-left: 1em;\n  position: relative;\n  top: 0.5em;\n}\n\n.editor-post-title__block .editor-post-title__input {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-size: 2.8125em;\n  font-weight: 700;\n}\n\n\/** === Default Appender === *\/\n.editor-default-block-appender .editor-default-block-appender__content {\n  font-family: \"NonBreakingSpaceOverride\", \"Hoefler Text\", \"Baskerville Old Face\", Garamond, \"Times New Roman\", serif;\n  font-size: 22px;\n}\n\n\/** === Heading === *\/\n.wp-block-heading strong {\n  font-weight: bolder;\n}\n\n\/** === Paragraph === *\/\n.wp-block-paragraph.has-drop-cap:not(:focus)::first-letter {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-size: 3.375em;\n  line-height: 1;\n  font-weight: bold;\n  margin: 0 0.25em 0 0;\n}\n\n\/** === Table === *\/\n.wp-block-table {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n}\n\n\/** === Cover === *\/\n.wp-block-cover h2,\n.wp-block-cover .wp-block-cover-text {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-size: 1.6875em;\n  font-weight: bold;\n  line-height: 1.4;\n  padding-left: 1rem;\n  padding-right: 1rem;\n}\n\n.wp-block-cover h2 strong,\n.wp-block-cover .wp-block-cover-text strong {\n  font-weight: bolder;\n}\n\n@media only screen and (min-width: 768px) {\n  .wp-block-cover h2,\n  .wp-block-cover .wp-block-cover-text {\n    margin-left: auto;\n    margin-right: auto;\n    padding: 0;\n  }\n}\n\n@media only screen and (min-width: 768px) {\n  .wp-block-cover {\n    padding-left: 10%;\n    padding-right: 10%;\n  }\n  .wp-block-cover h2,\n  .wp-block-cover .wp-block-cover-text {\n    font-size: 2.25em;\n  }\n}\n\n.wp-block[data-type=\"core\/cover\"][data-align=\"left\"] .editor-block-list__block-edit,\n.wp-block[data-type=\"core\/cover\"][data-align=\"right\"] .editor-block-list__block-edit {\n  width: calc(4 * (100vw \/ 12));\n}\n\n.wp-block[data-type=\"core\/cover\"][data-align=\"left\"] .wp-block-cover,\n.wp-block[data-type=\"core\/cover\"][data-align=\"right\"] .wp-block-cover {\n  width: 100%;\n  max-width: 100%;\n  padding: calc(1.375 * 1rem);\n}\n\n.wp-block[data-type=\"core\/cover\"][data-align=\"left\"] .wp-block-cover p,\n.wp-block[data-type=\"core\/cover\"][data-align=\"right\"] .wp-block-cover p {\n  padding-left: 0;\n  padding-right: 0;\n}\n\n@media only screen and (min-width: 768px) {\n  .wp-block[data-type=\"core\/cover\"][data-align=\"left\"] .wp-block-cover,\n  .wp-block[data-type=\"core\/cover\"][data-align=\"right\"] .wp-block-cover {\n    padding: calc(2.75 * 1rem) calc(2.75 * 1rem) calc(3.125 * 1rem);\n  }\n}\n\n@media only screen and (min-width: 768px) {\n  .wp-block[data-type=\"core\/cover\"][data-align=\"wide\"] h2,\n  .wp-block[data-type=\"core\/cover\"][data-align=\"wide\"] .wp-block-cover-text,\n  .wp-block[data-type=\"core\/cover\"][data-align=\"full\"] h2,\n  .wp-block[data-type=\"core\/cover\"][data-align=\"full\"] .wp-block-cover-text {\n    max-width: calc(8 * (100vw \/ 12));\n  }\n}\n\n@media only screen and (min-width: 1168px) {\n  .wp-block[data-type=\"core\/cover\"][data-align=\"wide\"] h2,\n  .wp-block[data-type=\"core\/cover\"][data-align=\"wide\"] .wp-block-cover-text,\n  .wp-block[data-type=\"core\/cover\"][data-align=\"full\"] h2,\n  .wp-block[data-type=\"core\/cover\"][data-align=\"full\"] .wp-block-cover-text {\n    max-width: calc(6 * (100vw \/ 12));\n  }\n}\n\n@media only screen and (min-width: 768px) {\n  .wp-block[data-type=\"core\/cover\"][data-align=\"full\"] .wp-block-cover {\n    padding-left: calc(10% + 64px);\n    padding-right: calc(10% + 64px);\n  }\n}\n\n\/** === Gallery === *\/\n.wp-block-gallery .blocks-gallery-image figcaption,\n.wp-block-gallery .blocks-gallery-item figcaption,\n.wp-block-gallery .gallery-item .gallery-caption {\n  font-size: 0.71111em;\n  line-height: 1.6;\n}\n\n\/** === Button === *\/\n.wp-block-button .wp-block-button__link {\n  line-height: 1.8;\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-size: 0.88889em;\n  font-weight: bold;\n}\n\n.wp-block-button:not(.is-style-outline) .wp-block-button__link {\n  background: #0073aa;\n}\n\n.wp-block-button:not(.is-style-squared) .wp-block-button__link {\n  border-radius: 5px;\n}\n\n.wp-block-button.is-style-outline, .wp-block-button.is-style-outline:hover, .wp-block-button.is-style-outline:focus, .wp-block-button.is-style-outline:active {\n  background: transparent;\n  color: #0073aa;\n}\n\n.wp-block-button.is-style-outline .wp-block-button__link, .wp-block-button.is-style-outline:hover .wp-block-button__link, .wp-block-button.is-style-outline:focus .wp-block-button__link, .wp-block-button.is-style-outline:active .wp-block-button__link {\n  background: transparent;\n}\n\n.wp-block-button.is-style-outline .wp-block-button__link:not(.has-text-color), .wp-block-button.is-style-outline:hover .wp-block-button__link:not(.has-text-color), .wp-block-button.is-style-outline:focus .wp-block-button__link:not(.has-text-color), .wp-block-button.is-style-outline:active .wp-block-button__link:not(.has-text-color) {\n  color: #0073aa;\n}\n\n\/** === Blockquote === *\/\n.wp-block-quote:not(.is-large):not(.is-style-large) {\n  border-left: 2px solid #0073aa;\n}\n\n.wp-block-quote.is-large, .wp-block-quote.is-style-large {\n  margin-top: 2.8125em;\n  margin-bottom: 2.8125em;\n}\n\n.wp-block-quote.is-large p,\n.wp-block-quote.is-style-large p {\n  font-size: 1.6875em;\n  line-height: 1.3;\n  margin-bottom: 0.5em;\n  margin-top: 0.5em;\n}\n\n.wp-block-quote cite,\n.wp-block-quote footer,\n.wp-block-quote .wp-block-quote__citation {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-size: 0.71111em;\n  line-height: 1.6;\n  color: #767676;\n}\n\n\/** === Pullquote === *\/\n.wp-block-pullquote {\n  border-color: transparent;\n  border-width: 2px;\n  color: #000;\n}\n\n.wp-block-pullquote blockquote {\n  margin-top: calc(3 * 1rem);\n  margin-bottom: calc(3.33 * 1rem);\n  hyphens: auto;\n  word-break: break-word;\n}\n\n.wp-block-pullquote:not(.is-style-solid-color) .wp-block-pullquote__citation {\n  color: #767676;\n}\n\n.wp-block-pullquote.is-style-solid-color blockquote {\n  width: calc(100% - (2 * 1rem));\n  max-width: calc( 100% - (2 * 1rem));\n}\n\n.wp-block-pullquote.is-style-solid-color blockquote a,\n.wp-block-pullquote.is-style-solid-color blockquote.has-text-color p,\n.wp-block-pullquote.is-style-solid-color blockquote.has-text-color a {\n  color: inherit;\n}\n\n.wp-block-pullquote.is-style-solid-color blockquote:not(.has-text-color) {\n  color: #fff;\n}\n\n@media only screen and (min-width: 768px) {\n  .wp-block-pullquote.is-style-solid-color blockquote {\n    max-width: 80%;\n  }\n}\n\n.wp-block-pullquote.is-style-solid-color:not(.has-background-color) {\n  background-color: #0073aa;\n}\n\n.wp-block[data-type=\"core\/pullquote\"] blockquote > .block-library-pullquote__content .editor-rich-text__tinymce[data-is-empty=\"true\"]::before,\n.wp-block[data-type=\"core\/pullquote\"] blockquote > .editor-rich-text p,\n.wp-block[data-type=\"core\/pullquote\"] p,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] blockquote > .block-library-pullquote__content .editor-rich-text__tinymce[data-is-empty=\"true\"]::before,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] blockquote > .editor-rich-text p,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] p,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] blockquote > .block-library-pullquote__content .editor-rich-text__tinymce[data-is-empty=\"true\"]::before,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] blockquote > .editor-rich-text p,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] p {\n  font-size: 1.6875em;\n  font-style: italic;\n  line-height: 1.3;\n  margin-bottom: 0.5em;\n  margin-top: 0.5em;\n}\n\n@media only screen and (min-width: 768px) {\n  .wp-block[data-type=\"core\/pullquote\"] blockquote > .block-library-pullquote__content .editor-rich-text__tinymce[data-is-empty=\"true\"]::before,\n  .wp-block[data-type=\"core\/pullquote\"] blockquote > .editor-rich-text p,\n  .wp-block[data-type=\"core\/pullquote\"] p,\n  .wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] blockquote > .block-library-pullquote__content .editor-rich-text__tinymce[data-is-empty=\"true\"]::before,\n  .wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] blockquote > .editor-rich-text p,\n  .wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] p,\n  .wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] blockquote > .block-library-pullquote__content .editor-rich-text__tinymce[data-is-empty=\"true\"]::before,\n  .wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] blockquote > .editor-rich-text p,\n  .wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] p {\n    font-size: 2.25em;\n  }\n}\n\n.wp-block[data-type=\"core\/pullquote\"] .wp-block-pullquote__citation,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] .wp-block-pullquote__citation,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] .wp-block-pullquote__citation {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-size: 0.71111em;\n  line-height: 1.6;\n  text-transform: none;\n}\n\n.wp-block[data-type=\"core\/pullquote\"] em,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] em,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] em {\n  font-style: normal;\n}\n\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] .editor-block-list__block-edit,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] .editor-block-list__block-edit {\n  width: calc(4 * (100vw \/ 12));\n  max-width: 50%;\n}\n\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] .editor-block-list__block-edit .wp-block-pullquote:not(.is-style-solid-color),\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] .editor-block-list__block-edit .wp-block-pullquote:not(.is-style-solid-color) {\n  padding: 0;\n}\n\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] .editor-block-list__block-edit .wp-block-pullquote.is-style-solid-color,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] .editor-block-list__block-edit .wp-block-pullquote.is-style-solid-color {\n  padding: 1em;\n}\n\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] blockquote > .block-library-pullquote__content .editor-rich-text__tinymce[data-is-empty=\"true\"]::before,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] blockquote > .editor-rich-text p,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] p,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"left\"] .wp-block-pullquote__citation,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] blockquote > .block-library-pullquote__content .editor-rich-text__tinymce[data-is-empty=\"true\"]::before,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] blockquote > .editor-rich-text p,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] p,\n.wp-block[data-type=\"core\/pullquote\"][data-align=\"right\"] .wp-block-pullquote__citation {\n  text-align: left;\n}\n\n@media only screen and (min-width: 768px) {\n  .wp-block[data-type=\"core\/pullquote\"][data-align=\"full\"] .wp-block-pullquote blockquote {\n    max-width: calc(80% - 128px);\n  }\n}\n\n\/** === File === *\/\n.wp-block-file {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n}\n\n.wp-block-file .wp-block-file__textlink {\n  text-decoration: underline;\n  color: #0073aa;\n}\n\n.wp-block-file .wp-block-file__textlink:hover {\n  color: #005177;\n  text-decoration: none;\n}\n\n.wp-block-file .wp-block-file__button {\n  display: table;\n  line-height: 1.8;\n  font-size: 0.88889em;\n  font-weight: bold;\n  background-color: #0073aa;\n  border-radius: 5px;\n}\n\n.wp-block-file .wp-block-file__button-richtext-wrapper {\n  display: block;\n  margin-top: calc(0.75 * 1rem);\n  margin-left: 0;\n}\n\n\/** === Verse === *\/\n.wp-block-verse,\n.wp-block-verse pre {\n  padding: 0;\n}\n\n\/** === Code === *\/\n.wp-block-code {\n  border-radius: 0;\n}\n\n\/** === Table === *\/\n.wp-block-table td, .wp-block-table th {\n  border-color: #767676;\n}\n\n\/** === Separator === *\/\n.wp-block-separator:not(.is-style-dots) {\n  border-bottom: 2px solid #767676;\n}\n\n.wp-block-separator:not(.is-style-wide):not(.is-style-dots) {\n  width: 2.25em;\n  margin-left: 0;\n}\n\n.wp-block-separator.is-style-dots:before {\n  color: #767676;\n  font-size: 1.6875em;\n  letter-spacing: calc(2 * 1rem);\n  padding-left: calc(2 * 1rem);\n}\n\n\/* Remove duplicate rule-line when a separator\n * is followed by an H1, or H2 *\/\n.wp-block[data-type=\"core\/separator\"] + .wp-block[data-type=\"core\/heading\"] h1:before,\n.wp-block[data-type=\"core\/separator\"] + .wp-block[data-type=\"core\/heading\"] h2:before {\n  display: none;\n}\n\n\/** === Latest Posts, Archives, Categories === *\/\nul.wp-block-archives,\n.wp-block-categories,\n.wp-block-latest-posts {\n  padding: 0;\n  list-style-type: none;\n}\n\nul.wp-block-archives ul,\n.wp-block-categories ul,\n.wp-block-latest-posts ul {\n  padding: 0;\n  list-style-type: none;\n}\n\nul.wp-block-archives li,\n.wp-block-categories li,\n.wp-block-latest-posts li {\n  color: #767676;\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-size: calc(22px * 1.125);\n  font-weight: bold;\n  line-height: 1.2;\n  padding-bottom: 0.75rem;\n}\n\nul.wp-block-archives li.menu-item-has-children, ul.wp-block-archives li:last-child,\n.wp-block-categories li.menu-item-has-children,\n.wp-block-categories li:last-child,\n.wp-block-latest-posts li.menu-item-has-children,\n.wp-block-latest-posts li:last-child {\n  padding-bottom: 0;\n}\n\nul.wp-block-archives li a,\n.wp-block-categories li a,\n.wp-block-latest-posts li a {\n  text-decoration: none;\n}\n\nul.wp-block-archives li ul,\n.wp-block-categories li ul,\n.wp-block-latest-posts li ul {\n  padding-left: 1rem;\n}\n\n.wp-block-categories ul {\n  padding-top: 0.75rem;\n}\n\n.wp-block-categories ul ul {\n  counter-reset: submenu;\n}\n\n.wp-block-categories ul ul > li > a::before {\n  font-family: \"NonBreakingSpaceOverride\", \"Hoefler Text\", \"Baskerville Old Face\", Garamond, \"Times New Roman\", serif;\n  font-weight: normal;\n  content: \"\u2013\u00a0\" counters(submenu, \"\u2013\u00a0\", none);\n  counter-increment: submenu;\n}\n\n.wp-block-categories li ul {\n  list-style: none;\n  padding-left: 0;\n  margin-bottom: -0.75rem;\n}\n\n\/** === Latest Posts grid view === *\/\n.wp-block-latest-posts.is-grid li {\n  border-top: 2px solid #ccc;\n  padding-top: 1rem;\n  margin-bottom: 2rem;\n}\n\n.wp-block-latest-posts.is-grid li a:after {\n  content: '';\n}\n\n.wp-block-latest-posts.is-grid li:last-child {\n  margin-bottom: auto;\n}\n\n.wp-block-latest-posts.is-grid li:last-child a:after {\n  content: '';\n}\n\n\/** === Latest Comments === *\/\n.wp-block-latest-comments .wp-block-latest-comments__comment-meta {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-weight: bold;\n}\n\n.wp-block-latest-comments .wp-block-latest-comments__comment-meta .wp-block-latest-comments__comment-date {\n  font-weight: normal;\n}\n\n.wp-block-latest-comments .wp-block-latest-comments__comment,\n.wp-block-latest-comments .wp-block-latest-comments__comment-date,\n.wp-block-latest-comments .wp-block-latest-comments__comment-excerpt p {\n  font-size: inherit;\n}\n\n.wp-block-latest-comments .wp-block-latest-comments__comment-date {\n  font-size: 0.71111em;\n}\n\n\/** === Classic Editor === *\/\n\/* Properly center-align captions in the classic-editor block *\/\n.wp-caption dd {\n  color: #767676;\n  font-size: 0.71111em;\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  line-height: 1.6;\n  margin: 0;\n  padding: 0.5rem;\n  text-align: left;\n  text-align: center;\n  -webkit-margin-start: 0px;\n  margin-inline-start: 0px;\n}\n\n.wp-block-freeform {\n  \/* Add style for galleries in classic-editor block *\/\n}\n\n.wp-block-freeform blockquote {\n  border-left: 2px solid #0073aa;\n}\n\n.wp-block-freeform blockquote cite {\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif;\n  font-size: 0.71111em;\n  font-style: normal;\n  line-height: 1.6;\n  color: #767676;\n}\n\n\/* Make sure our non-latin font overrides don't overwrite the iconfont used in the classic editor toolbar *\/\n.wp-block[data-type=\"core\/freeform\"] .mce-btn i {\n  font-family: dashicons !important;\n}\n","baseURL":"http:\/\/127.0.0.1\/wordpress\/wp-content\/themes\/twentynineteen\/style-editor.css"}],"imageSizes":[{"slug":"thumbnail","name":"Thumbnail"},{"slug":"medium","name":"Medium"},{"slug":"large","name":"Large"},{"slug":"full","name":"Full Size"}],"richEditingEnabled":true,"postLock":{"isLocked":false,"activePostLock":"1551117325:1"},"postLockUtils":{"nonce":"aadbdf604f","unlockNonce":"368d226bd0","ajaxUrl":"http:\/\/127.0.0.1\/wordpress\/wp-admin\/admin-ajax.php"},"enableCustomFields":false,"colors":[{"name":"Primary","slug":"primary","color":"#0073a8"},{"name":"Secondary","slug":"secondary","color":"#005075"},{"name":"Dark Gray","slug":"dark-gray","color":"#111"},{"name":"Light Gray","slug":"light-gray","color":"#767676"},{"name":"White","slug":"white","color":"#FFF"}],"fontSizes":[{"name":"Small","shortName":"S","size":19.5,"slug":"small"},{"name":"Normal","shortName":"M","size":22,"slug":"normal"},{"name":"Large","shortName":"L","size":36.5,"slug":"large"},{"name":"Huge","shortName":"XL","size":49.5,"slug":"huge"}]}, {"title":"","content":"","excerpt":""} ) );
			} );
		} );
	} )();
	window._wpLoadBlockEditor.then( function() {
			wp.data.dispatch( 'core/edit-post' ).setAvailableMetaBoxesPerLocation( {"side":[],"normal":[],"advanced":[]} );
		} );
	</script>
	<script type='text/javascript'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "default", { "locale_data": { "messages": { "": {} } } } );
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/dist/format-library.min.js?ver=1.2.10'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/shortcode.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/media-editor.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/media-audiovideo.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var mceViewL10n = {"shortcodes":["wp_caption","caption","gallery","playlist","audio","video","embed"]};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/mce-view.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/imgareaselect/jquery.imgareaselect.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var imageEditL10n = {"error":"Could not load the preview image. Please reload the page and try again."};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-admin/js/image-edit.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var quicktagsL10n = {"closeAllOpenTags":"Close all open tags","closeTags":"close tags","enterURL":"Enter the URL","enterImageURL":"Enter the URL of the image","enterImageDescription":"Enter a description of the image","textdirection":"text direction","toggleTextdirection":"Toggle Editor Text Direction","dfw":"Distraction-free writing mode","strong":"Bold","strongClose":"Close bold tag","em":"Italic","emClose":"Close italic tag","link":"Insert link","blockquote":"Blockquote","blockquoteClose":"Close blockquote tag","del":"Deleted text (strikethrough)","delClose":"Close deleted text tag","ins":"Inserted text","insClose":"Close inserted text tag","image":"Insert image","ul":"Bulleted list","ulClose":"Close bulleted list tag","ol":"Numbered list","olClose":"Close numbered list tag","li":"List item","liClose":"Close list item tag","code":"Code","codeClose":"Close code tag","more":"Insert Read More tag"};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/quicktags.min.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var wpLinkL10n = {"title":"Insert\/edit link","update":"Update","save":"Add Link","noTitle":"(no title)","noMatchesFound":"No results found.","linkSelected":"Link selected.","linkInserted":"Link inserted."};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/wplink.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/jquery/ui/menu.min.js?ver=1.11.4'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var uiAutocompleteL10n = {"noResults":"No results found.","oneResult":"1 result found. Use up and down arrow keys to navigate.","manyResults":"%d results found. Use up and down arrow keys to navigate.","itemSelected":"Item selected."};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/jquery/ui/autocomplete.min.js?ver=1.11.4'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var thickboxL10n = {"next":"Next >","prev":"< Prev","image":"Image","of":"of","close":"Close","noiframes":"This feature requires inline frames. You have iframes disabled or your browser does not support them.","loadingAnimation":"http:\/\/127.0.0.1\/wordpress\/wp-includes\/js\/thickbox\/loadingAnimation.gif"};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/thickbox/thickbox.js?ver=3.1-20121105'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-admin/js/media-upload.min.js?ver=5.0.3'></script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-admin/js/svg-painter.js?ver=5.0.3'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var authcheckL10n = {"beforeunload":"Your session has expired. You can log in again from this page or go to the login page.","interval":"180"};
	/* ]]> */
	</script>
	<script type='text/javascript' src='http://127.0.0.1/wordpress/wp-includes/js/wp-auth-check.min.js?ver=5.0.3'></script>
			<script type="text/javascript">
			window.wp = window.wp || {};
			window.wp.editor = window.wp.editor || {};
			window.wp.editor.getDefaultSettings = function() {
				return {
					tinymce: {theme:"modern",skin:"lightgray",language:"en",formats:{alignleft: [{selector: "p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li", styles: {textAlign:"left"}},{selector: "img,table,dl.wp-caption", classes: "alignleft"}],aligncenter: [{selector: "p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li", styles: {textAlign:"center"}},{selector: "img,table,dl.wp-caption", classes: "aligncenter"}],alignright: [{selector: "p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li", styles: {textAlign:"right"}},{selector: "img,table,dl.wp-caption", classes: "alignright"}],strikethrough: {inline: "del"}},relative_urls:false,remove_script_host:false,convert_urls:false,browser_spellcheck:true,fix_list_elements:true,entities:"38,amp,60,lt,62,gt",entity_encoding:"raw",keep_styles:false,cache_suffix:"wp-mce-4800-20180716",resize:"vertical",menubar:false,branding:false,preview_styles:"font-family font-size font-weight font-style text-decoration text-transform",end_container_on_empty_block:true,wpeditimage_html5_captions:true,wp_lang_attr:"en-US",wp_keep_scroll_position:false,wp_shortcut_labels:{"Heading 1":"access1","Heading 2":"access2","Heading 3":"access3","Heading 4":"access4","Heading 5":"access5","Heading 6":"access6","Paragraph":"access7","Blockquote":"accessQ","Underline":"metaU","Strikethrough":"accessD","Bold":"metaB","Italic":"metaI","Code":"accessX","Align center":"accessC","Align right":"accessR","Align left":"accessL","Justify":"accessJ","Cut":"metaX","Copy":"metaC","Paste":"metaV","Select all":"metaA","Undo":"metaZ","Redo":"metaY","Bullet list":"accessU","Numbered list":"accessO","Insert\/edit image":"accessM","Remove link":"accessS","Toolbar Toggle":"accessZ","Insert Read More tag":"accessT","Insert Page Break tag":"accessP","Distraction-free writing mode":"accessW","Add Media":"accessM","Keyboard Shortcuts":"accessH"},content_css:"http://127.0.0.1/wordpress/wp-includes/css/dashicons.min.css?ver=5.0.3,http://127.0.0.1/wordpress/wp-includes/js/tinymce/skins/wordpress/wp-content.css?ver=5.0.3",toolbar1:"bold,italic,bullist,numlist,link",wpautop:false,indent:true,elementpath:false,plugins:"charmap,colorpicker,hr,lists,paste,tabfocus,textcolor,fullscreen,wordpress,wpautoresize,wpeditimage,wpemoji,wpgallery,wplink,wptextpattern"},
					quicktags: {
						buttons: 'strong,em,link,ul,ol,li,code'
					}
				};
			};

						var tinyMCEPreInit = {
					baseURL: "http://127.0.0.1/wordpress/wp-includes/js/tinymce",
					suffix: ".min",
					mceInit: {},
					qtInit: {},
					load_ext: function(url,lang){var sl=tinymce.ScriptLoader;sl.markDone(url+'/langs/'+lang+'.js');sl.markDone(url+'/langs/'+lang+'_dlg.js');}
				};
						</script>
			<script type='text/javascript'>
	tinymce.addI18n( 'en', {"Ok":"OK","Bullet list":"Bulleted list","Insert\/Edit code sample":"Insert\/edit code sample","Url":"URL","Spellcheck":"Check Spelling","Row properties":"Table row properties","Cell properties":"Table cell properties","Paste row before":"Paste table row before","Paste row after":"Paste table row after","Cut row":"Cut table row","Copy row":"Copy table row","Merge cells":"Merge table cells","Split cell":"Split table cell","Paste is now in plain text mode. Contents will now be pasted as plain text until you toggle this option off.":"Paste is now in plain text mode. Contents will now be pasted as plain text until you toggle this option off.\n\nIf you\u2019re looking to paste rich content from Microsoft Word, try turning this option off. The editor will clean up text pasted from Word automatically.","Rich Text Area. Press ALT-F9 for menu. Press ALT-F10 for toolbar. Press ALT-0 for help":"Rich Text Area. Press Alt-Shift-H for help.","You have unsaved changes are you sure you want to navigate away?":"The changes you made will be lost if you navigate away from this page.","Your browser doesn't support direct access to the clipboard. Please use the Ctrl+X\/C\/V keyboard shortcuts instead.":"Your browser does not support direct access to the clipboard. Please use keyboard shortcuts or your browser\u2019s edit menu instead.","Edit|button":"Edit"});
	tinymce.ScriptLoader.markDone( 'http://127.0.0.1/wordpress/wp-includes/js/tinymce/langs/en.js' );
	</script>
			<div id="wp-link-backdrop" style="display: none"></div>
			<div id="wp-link-wrap" class="wp-core-ui" style="display: none" role="dialog" aria-labelledby="link-modal-title">
			<form id="wp-link" tabindex="-1">
			<input type="hidden" id="_ajax_linking_nonce" name="_ajax_linking_nonce" value="33fb6e27a5" />		<h1 id="link-modal-title">Insert/edit link</h1>
			<button type="button" id="wp-link-close"><span class="screen-reader-text">Close</span></button>
			<div id="link-selector">
				<div id="link-options">
					<p class="howto" id="wplink-enter-url">Enter the destination URL</p>
					<div>
						<label><span>URL</span>
						<input id="wp-link-url" type="text" aria-describedby="wplink-enter-url" /></label>
					</div>
					<div class="wp-link-text-field">
						<label><span>Link Text</span>
						<input id="wp-link-text" type="text" /></label>
					</div>
					<div class="link-target">
						<label><span></span>
						<input type="checkbox" id="wp-link-target" /> Open link in a new tab</label>
					</div>
				</div>
				<p class="howto" id="wplink-link-existing-content">Or link to existing content</p>
				<div id="search-panel">
					<div class="link-search-wrapper">
						<label>
							<span class="search-label">Search</span>
							<input type="search" id="wp-link-search" class="link-search-field" autocomplete="off" aria-describedby="wplink-link-existing-content" />
							<span class="spinner"></span>
						</label>
					</div>
					<div id="search-results" class="query-results" tabindex="0">
						<ul></ul>
						<div class="river-waiting">
							<span class="spinner"></span>
						</div>
					</div>
					<div id="most-recent-results" class="query-results" tabindex="0">
						<div class="query-notice" id="query-notice-message">
							<em class="query-notice-default">No search term specified. Showing recent items.</em>
							<em class="query-notice-hint screen-reader-text">Search or use up and down arrow keys to select an item.</em>
						</div>
						<ul></ul>
						<div class="river-waiting">
							<span class="spinner"></span>
						</div>
	 				</div>
	 			</div>
			</div>
			<div class="submitbox">
				<div id="wp-link-cancel">
					<button type="button" class="button">Cancel</button>
				</div>
				<div id="wp-link-update">
					<input type="submit" value="Add Link" class="button button-primary" id="wp-link-submit" name="wp-link-submit">
				</div>
			</div>
			</form>
			</div>
			
	<div class="clear"></div></div><!-- wpwrap -->
	<script type="text/javascript">if(typeof wpOnload=='function')wpOnload();</script>
</body>
</html>
@endverbatim