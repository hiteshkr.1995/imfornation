<?php
use App\Models\Category;

$categories = Category::all();
?>

<div class="nav-scroller mb-2">
	<nav class="nav d-flex justify-content-between">

		<a class="fas fa-home fas fa-2x p-1" style="background-color: var(--app-color);color: #FFF;" href="{{ route('welcome') }}"></a>

		<a class="p-2 {{ ( Request::is('posts') ? 'bg-secondary text-white' : 'text-muted' ) }}" href="{{ route('posts.index') }}">News Feed</a>

		@foreach($categories as $key => $value)
			<a class="p-2 {{ ( Request::is('cate/'.$value->slug) ? 'bg-secondary text-white' : 'text-muted' ) }}" href="{{ route('cate.page', $value->slug) }}">
				{{ $value->name }}
			</a>
		@endforeach
	</nav>
</div>