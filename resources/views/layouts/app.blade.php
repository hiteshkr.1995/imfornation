<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta property="fb:app_id"        content="426140541290719" />

	@section('meta')

	<meta name="keywords" content="Breaking News| India News | Headlines | Current Affairs | MCQ | in playful way | {{ config('app.name') }}">
	<meta name="description" content="Read latest news headline by filling the blanks. Read , politics, election news, geo politics, entertainment, business, sports, bollywood and technology news around the world.">

	<meta property="og:url"           content="{{ url('') }}" />
	<meta property="og:type"          content="News" />
	<meta property="og:title"         content="Breaking News| India News | Headlines | Current Affairs | MCQ | in playful way | {{ config('app.name') }}" />
	<meta property="og:description"   content="Read latest news headline by filling the blanks. Read , politics, election news, geo politics, entertainment, business, sports, bollywood and technology news around the world." />
	<meta property="og:image"         content="{{ app_logo('50', 'png') }}" />

	@show

	<meta name="author" content="{{ config('app.name') }} | Team">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title itemprop="name">@yield('title') | {{ config('app.name') }}</title>

	<!-- Fonts -->
	<link href="{{ asset('css/fonts.css') }}" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<!-- Styles -->
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css?v1') }}" rel="stylesheet">
	<style type="text/css">
		.navbar {
			background-color: var(--app-color);
			font-weight: bold;
			color: #FFF;
		}
		a {
			font-weight: bolder;
		}
		[v-cloak] {
			display: none;
		}
		/*#loading{
			text-align: center;
		}
		.spinner-border {
			border-radius: 50%!important;
		}*/
		.nav-link {
			color: #fff!important;
		}

		/*Google Translate customization css start*/
		.nav-item.active {
			background-color: #5f9e9f!important;
		}
		.goog-te-banner-frame.skiptranslate {
			display: none !important;
		}
		#goog-gt-tt, .goog-te-balloon-frame{
			display: none !important;
		}

		.goog-logo-link{
			display: none !important;
		}
		.goog-te-gadget{
			height: 28px !important;
			overflow: hidden;
		}
		.goog-text-highlight {
			background: none !important;
			box-shadow: none !important;
		}

		.goog-te-combo {
			margin-top: -7px!important;
			font-family: inherit!important;
			text-transform: none;
			word-wrap: normal;
			display: block;
			width: 100%;
			height: calc(1.5em + .75rem + 2px);
			padding: .375rem .75rem;
			font-size: 1rem!important;
			font-weight: 400!important;
			line-height: 1.5;
			color: #495057;
			background-color: #fff;
			background-clip: padding-box;
			border: 1px solid #ced4da;
			border-radius: .25rem;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
		}

		body {
			top: 0px !important;
		}
		/*Google Translate customization css end*/
	</style>
	@stack('plugins')

	@if ( App::environment('production') )

		<meta name="google-site-verification" content="uNhVxu2zplS4GPz_qc03qdVDll9hDDEScoQweEdh92s" />

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134949573-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-134949573-1');
		</script>

		@if (Route::is('posts.page.share', 'blogs.view'))
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({
					google_ad_client: "ca-pub-1162801230498805",
					enable_page_level_ads: true
				});
			</script>
		@endif

	@endif

</head>
<body>
	<!-- Navbar-->
	@include('layouts.nav')
	<div class="container">

		@if(Request::segment(1) == "blogs")
		@else
			@include('layouts.nav_category')

			@if( Route::currentRouteName() == "welcome" || Request::segment(1) == "cate" )
			<div class="text-center">
				<small style="font-weight: bolder;" class="text-app mx-1">Click on the image and solve the puzzle to view the news articles.</small>
			</div>
			@endif

		@endif

		@include('shared.messages')

		<main id="app" class="my-4">
			@yield('content')
		</main>

		<!-- <div id="loading" class="my-4">
			<div class="center">
				<div class="spinner-border text-primary" role="status">
					<span class="sr-only">Loading...</span>
				</div>
			</div>
		</div> -->
	</div>

	<!-- Scripts -->
	<script type="text/javascript">
		/*function googleTranslateElementInit() {
			new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
		}*/
		function googleTranslateElementInit() {
			new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
	</script>

	<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	<script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
	<script src="{{ asset('js/popper.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/vue/app.js') }}"></script>
	<script src="{{ asset('js/axios/axios.min.js') }}"></script>


	<script type="text/javascript">
		/*$(window).on('load', function() {
			$("#loading").attr("hidden", "hidden");
			$("#app").removeAttr("hidden");
		});*/


		/* Active navbar */
		$(document).ready(function () {
			var url = window.location;
			$('ul.navbar-nav a[href="'+ url +'"]').parent().addClass('active');
			$('ul.navbar-nav a').filter(function() {
				return this.href == url;
			}).parent().addClass('active');
		});

		/*let prevScrollpos = window.pageYOffset
		window.onscroll = function() {
			let currentScrollPos = window.pageYOffset
			if (prevScrollpos > currentScrollPos) {
				document.getElementById("navbar").style.top = "0"
			} else {
				document.getElementById("navbar").style.top = "-50px"
			}
			prevScrollpos = currentScrollPos
		}*/

	</script>

	@stack('scripts')


</body>
</html>
