<nav class="navbar navbar-expand-md navbar-dark">
	<div class="container">
		<a class="navbar-brand" href="{{ url('/') }}">
			<img src="{{ app_logo('tran_326', 'png') }}" class="img-fluid" style="width: 39px;">
			{{ config('app.name') }}
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!-- Left Side Of Navbar -->
			<ul class="navbar-nav mr-auto">

			</ul>

			<!-- Right Side Of Navbar -->
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<div id="google_translate_element" class="nav-link"></div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('blogs.index') }}">{{ __('Blogs') }}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('about') }}">{{ __('About') }}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('contact') }}">{{ __('Contact Us') }}</a>
				</li>
				{{--@guest
					<li class="nav-item">
						<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
					</li>
					@if (Route::has('register'))
						<li class="nav-item">
							<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
						</li>
					@endif
				@else
					<li class="nav-item">
						<a class="nav-link" href="{{ route('user.index') }}">{{ __('Profile') }}</a>
					</li>
					<li class="nav-item dropdown">
						<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
							{{ Auth::user()->name }} <span class="caret"></span>
						</a>

						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								{{ __('Logout') }}
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</div>
					</li>
				@endguest--}}
			</ul>
		</div>
	</div>
</nav>