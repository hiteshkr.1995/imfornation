@extends('layouts.app')

@section('title', 'Sketch Create')

@push('plugins')
<script src="{{ asset('js/vue-picture-input/vue-picture-input.js') }}"></script>
<style type="text/css">
	@media (max-width: 576px) {
		.container {
			padding-right: 0px;
			padding-left: 0px;
		}
		.col-12 {
			padding-right: 0px;
			padding-left: 0px;
		}
	}
</style>
@endpush

@section('content')
<div class="col-12 mx-auto preloader" v-cloak>
	<div class="card">
		<div class="card-body">
			<h1 class="text-center">Create Sketch</h1>
			<!-- <form
				id="user-create-sketch"
				action="{{ route('user.sketchs.store') }}"
				method="post"
				enctype="multipart/form-data"
			>
			@csrf -->
				<div class="form-group my-5">
					<input type="text" class="form-control form-control-lg" v-bind:class="{ 'is-invalid': errors.title }" name="title" v-model="title" placeholder="Enter Title">
					<div class="errors" v-if="errors.title">
						<small class="text-danger" :key="error" v-for="error in errors.title">@{{ error }}</small>
					</div>
				</div>
				<div class="form-group">
					<div class="errors" v-if="errors.sketch">
						<small class="text-danger" :key="error" v-for="error in errors.sketch">@{{ error }}</small>
					</div>
					<picture-input
						height="450"
						width="688"
						:crop="false"
						name="sketch"
						accept="image/jpeg,image/png"
						size="5"
						ref="pictureInput"
						button-class="btn btn-danger"
						@change="onChange"
					></picture-input>
				</div>
				<div class="form-group mt-5">
					<select class="form-control"  v-bind:class="{ 'is-invalid': errors.option }" name="option" v-model="option">
						<option disabled value="">Slelect Option</option>
						@foreach ($categories as $key => $value)
							<option value="{{ $value->id }}">{{ $value->name }}</option>
						@endforeach
					</select>
					<div class="errors" v-if="errors.option">
						<small class="text-danger" :key="error" v-for="error in errors.option">@{{ error }}</small>
					</div>
				</div>
				<div class="form-group my-5">
					<input type="text" class="form-control form-control-lg" v-bind:class="{ 'is-invalid': errors.question }" name="question" v-model="question" placeholder="Enter question">
					<div class="errors" v-if="errors.question || errors.words">
						<small class="text-danger" :key="error" v-for="error in errors.question">@{{ error }}</small>
						<!-- <small class="text-danger" :key="error" v-for="error in errors.words">@{{ error }}</small> -->
					</div>
				</div>
				@verbatim
					<small v-bind:class="[ errors.words ? 'text-danger' : 'text-info' ]" class="border-bottom" v-if="question">Double click on word to set fill in the blank:</small>
					<div class="my-3" @dblclick="selectWords($event)">
						<h3>{{ question }}</h3>
					</div>
					<ul class="mt-4">
						<li v-for="value in words">{{ value }}</li>
					</ul>
				@endverbatim
				<div class="form-group my-5">
					<textarea class="form-control" v-bind:class="{ 'is-invalid': errors.description }" rows="5" name="description" v-model="description" placeholder="Write Description"></textarea>
					<div class="errors" v-if="errors.description">
						<small class="text-danger" :key="error" v-for="error in errors.description">@{{ error }}</small>
					</div>
				</div>
				<small class="text-danger" :key="error" v-for="error in errors.links">@{{ error }}</small>
				<div
					class="form-group"
					v-for="(value,key) in links"
					:key="key"
				>
					<input type="text" class="form-control" v-model="links[key]" placeholder="Enter Link">
				</div>
				<!-- <div class="form-group mt-4">
					<textarea class="form-control" rows="5" placeholder="Enter you view" v-model="textarea"></textarea>
				</div> -->
				<div class="text-center">
					<button @click="upload" class="btn btn-lg btn-success">Create Sketch</button>
					<div class="btn-lg" style="display: none;">
						<div class="spinner-border text-primary" role="status">
							<span class="sr-only">Loading...</span>
						</div>
					</div>
				</div>
				<!-- <div class="form-actions text-center" v-else>
					<div class="spinner-border text-success" style="border-radius: 50% !important;height: 48px; width: 48px;" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div> -->
			<!-- </form> -->
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script>
	$(window).on('load', function() {
		let app = new Vue({
			el: "#app",
			data: {
				sketch: '',
				option: '',
				description: '',
				question: '',
				title: '',
				errors: [],
				words: [],
				links: [""],
			},
			components: {
				'picture-input': PictureInput
			},
			mounted() {
			},
			watch: {
				links: function (val) {
					let lastKey = (val.length-1)
					if (lastKey < 10) {
						if (this.links[lastKey] != "") {
							if (this.links[lastKey].length >= 1) {
								this.links.push("")
							}
						}
					}
				},
			},
			methods: {
				onChange(image) {
					this.sketch = image;
				},
				selectWords(event) {
					let word = window.getSelection().toString()
					if (word.length > 1 && this.words.length < 3 && !this.words.includes(word)) {
						this.words.push(word)
					}
				},
				upload: function (e) {
					$('.text-center button').hide()
					$('.text-center div').show()

					if (this.links.length > 1) {
						this.links = this.links.filter( (el) => {
							return el != ""
						})
					}

					if (this.links.length == 0) {
						this.links = [""]
					}

					axios.post("{{ route('user.sketchs.store') }}", {
						sketch: this.sketch,
						option: this.option,
						question: this.question,
						title: this.title,
						words: this.words,
						description: this.description,
						links: this.links,
					}).then((response) => {

						// window.location.href = response.data;

					}).catch(({ response }) => {

						$('.text-center button').show()
						$('.text-center div').hide()

						if (response.status == 422) {
							this.errors = response.data.errors
						}
					})
				},
			},
		})
	})
</script>
@endpush