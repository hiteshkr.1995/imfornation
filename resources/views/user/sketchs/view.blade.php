@extends('layouts.app')

@section('title', 'Sketch Create')

@push('plugins')
@endpush

@section('content')

<div>
	@foreach ($sketch as $key => $value)
		<div class="col-md-12 mx-auto{{ $loop->first ? '' : ' mt-5' }}">
			<div class="card">
				<div class="card-body row">

					<div class="col-md-4 text-center">
						<img class="img-fluid" src="{{ asset('storage/sketchs/'.$value->sketch) }}">
					</div>

					<div class="col-md-8" style="position: relative;">
						<h3>{{ $value->title }}</h3>
						<div>
							<div class="my-5">
								<h5>{{ $value->description }}</h5>
							</div>
							<div class="row" style="position: absolute;bottom: 0;width: 100%">
								<div class="col-md-6">
									<a href="{{ route('user.sketchs.byId', $value->id) }}" class="btn btn-sm btn-outline-primary">Read More</a>
								</div>
								<div class="col-md-6">
									<h6 class="float-right">{{ date('d-M-Y h:i A', strtotime($value->created_at)) }}</h6>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	@endforeach
</div>

@endsection

@push('scripts')
<script>
	let app = new Vue({
		el: "#app",
		data: {
		},
		components: {
		},
		mounted() {
		},
		methods: {
		},
	})
</script>
@endpush