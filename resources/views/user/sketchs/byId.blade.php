@extends('layouts.app')

@section('title', $sketch->title)

@push('plugins')
@endpush

@section('content')
<div class="card">
	<img src="{{ asset('storage/sketchs/'.$sketch->sketch) }}" class="card-img-top" alt="...">
	<div class="card-body">
		<h5 class="card-title">{{ $sketch->title }}</h5>
		<p class="mt-5" class="card-text">{{ $sketch->description }}</p>
	</div>
	<div class="card-footer text-muted">
		{{ date('d-M-Y h:i A', strtotime($sketch->created_at)) }}
	</div>
</div>
@endsection

@push('scripts')

<script type="text/javascript">

	let app = new Vue({
		el: "#app",
		data: {
		},
		mounted() {
		},

		methods: {
		}
	})

</script>

@endpush