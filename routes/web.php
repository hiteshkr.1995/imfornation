<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/test', function() {
// 	$cookie = Cookie::forever('imfo_views', 'SuperAdmin');
// 	return response('SuperAdmin SetCookie')->cookie($cookie);
// });

// Basic Routes
Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/about', 'BasicController@about')->name('about');
Route::get('/contact', 'BasicController@contact')->name('contact');
Route::post('/contact', 'BasicController@contactSend')->name('contact.send');
Route::get('/disclaimer', 'BasicController@disclaimer')->name('disclaimer');

Route::get('/img/{name}', 'BasicController@images')->name('images')->where('name', '.*');

// Blogs Routes
Route::get('/blogs', 'BlogsController@index')->name('blogs.index');
Route::get('/blogs/{slug}', 'BlogsController@view')->name('blogs.view');

// Posts Routes
Route::get('/posts', 'PostsController@index')->name('posts.index');
Route::get('/posts/{catSlug}', 'PostsController@catePage')->name('posts.cate.page');
Route::get('/posts/{catSlug}/{skeSlug}', 'PostsController@pageShare')->name('posts.page.share');

// Cate Routes
Route::get('/cate', 'CateController@index')->name('cate.index');
Route::get('/cate/{slug}', 'CateController@page')->name('cate.page');
Route::get('/cate/{slug}/{skeSlug}', 'CateController@pageShare')->name('cate.page.share');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Middleware
// Route::middleware(['auth:web'])->group(function () {
// 	Route::get('/user', 'UserController@index')->name('user.index');
// 	Route::get('/user/sketchs/create', 'UserController@sketchsCreate')->name('user.sketchs.create');
// 	Route::post('/user/sketchs/create', 'UserController@sketchsStore')->name('user.sketchs.store');
// 	Route::get('/user/sketchs/view', 'UserController@sketchsView')->name('user.sketchs.view');
// 	Route::get('/user/sketchs/{id}', 'UserController@sketchsById')->name('user.sketchs.byId');

// });

// oversight
Route::middleware(['auth:oversight'])->group(function () {
	Route::get('/about/edit', 'BasicController@aboutEdit')->name('about.edit');
	// Route::get('/disclaimer/edit', 'BasicController@disclaimerEdit')->name('disclaimer.edit');
	Route::post('/about/edit', 'BasicController@aboutUpdate')->name('about.update');
	// Route::post('/disclaimer/edit', 'BasicController@disclaimerUpdate')->name('disclaimer.update');
});

Route::prefix('oversight')->namespace('Oversight')->name('oversight.')->group(function () {

	Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('/login', 'Auth\LoginController@login')->name('login.post');
	Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

	// Middleware
	Route::middleware(['auth:oversight'])->group(function () {

		Route::get('/', 'DashboardController@index')->name('dashboard');

		Route::post('/upload', 'DashboardController@upload')->name('upload');
		Route::get('/storage', 'DashboardController@storage')->name('storage');

		Route::get('/websites', 'WebsiteIconController@websites')->name('websites');
		Route::post('/websites/store', 'WebsiteIconController@store_website_url')->name('store_website_url');
		Route::get('/website_icons', 'WebsiteIconController@website_icons')->name('website_icons');

		Route::post('/sketchs/datatable', 'SketchController@datatable')->name('sketchs.datatable');
		Route::post('/sketchs/is_active/{id}', 'SketchController@isActive')->name('sketchs.isActive');

		Route::post('/blogs/datatable', 'BlogController@datatable')->name('blogs.datatable');
		Route::post('/blogs/is_active/{id}', 'BlogController@isActive')->name('blogs.isActive');

		Route::resource('/blogs', 'BlogController')->parameters([
			'blogs' => 'id'
		]);
		Route::resource('/sketchs', 'SketchController')->parameters([
			'sketchs' => 'id'
		]);
		Route::resource('/categories', 'CategoryController')->parameters([
			'categories' => 'id',
		]);
		Route::resource('/website_icon', 'WebsiteIconController')->parameters([
			'website_icon' => 'id',
		]);

	});

});